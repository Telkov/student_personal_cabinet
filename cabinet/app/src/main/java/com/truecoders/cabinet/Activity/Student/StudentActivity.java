package com.truecoders.cabinet.Activity.Student;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.truecoders.cabinet.Fragment.General.NewsFragment;
import com.truecoders.cabinet.Fragment.Student.MenuFragment;
import com.truecoders.cabinet.Fragment.Student.RatingFragment;
import com.truecoders.cabinet.Fragment.Student.ScheduleFragment;
import com.truecoders.cabinet.Fragment.Student.TasksFragment;
import com.truecoders.cabinet.R;

/**
 * @author Andrew Telkov
 */

public class StudentActivity extends AppCompatActivity {
    private TextView titleActivity;
    private Boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student);

        getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorHeader));

        titleActivity = findViewById(R.id.header_title);

        BottomNavigationViewEx navigation = findViewById(R.id.activity_student_navigation);
        navigation.setSelectedItemId(R.id.navigation_schedule);

        navigation.enableAnimation(true);
        navigation.enableShiftingMode(false);
        navigation.enableItemShiftingMode(false);
        navigation.setTextVisibility(false);
        navigation.setIconSize(30, 30);

        displaySelectedScreen(2);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    private void displaySelectedScreen(int code) {
        Fragment fragment = null;
        switch (code) {
            case 0:
                fragment = new NewsFragment();
                break;
            case 1:
                fragment = new TasksFragment();
                break;
            case 2:
                fragment = new ScheduleFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("possibleGroups", getIntent().getSerializableExtra("possibleGroups"));
                fragment.setArguments(bundle);
                break;
            case 3:
                fragment = new RatingFragment();
                break;
            case 4:
                fragment = new MenuFragment();
                break;
        }
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
        if (!item.isChecked()) {
            switch (item.getItemId()) {
                case R.id.navigation_news:
                    titleActivity.setText(R.string.news_student_fragment);
                    displaySelectedScreen(0);
                    return true;
                case R.id.navigation_tasks:
                    titleActivity.setText(R.string.tasks_student_fragment);
                    displaySelectedScreen(1);
                    return true;
                case R.id.navigation_schedule:
                    titleActivity.setText(R.string.schedule_student_fragment);
                    displaySelectedScreen(2);
                    return true;
                case R.id.navigation_progress:
                    titleActivity.setText(R.string.rating_student_fragment);
                    displaySelectedScreen(3);
                    return true;
                case R.id.navigation_menu:
                    titleActivity.setText(R.string.menu_student_fragment);
                    displaySelectedScreen(4);
                    return true;
            }
        }
        return false;
    };

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() != 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            if (doubleBackToExitPressedOnce) {
                finishAffinity();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, R.string.exit_toast, Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 3000);
        }
    }
}

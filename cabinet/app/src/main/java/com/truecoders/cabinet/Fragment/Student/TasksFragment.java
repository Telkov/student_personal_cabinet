package com.truecoders.cabinet.Fragment.Student;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import com.truecoders.cabinet.Activity.Student.TaskCreateEditActivity;
import com.truecoders.cabinet.Adapter.ListViewAdapter.Student.TaskListAdapter;
import com.truecoders.cabinet.Model.Student.Profile;
import com.truecoders.cabinet.Model.Student.Task;
import com.truecoders.cabinet.R;
import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;
import io.realm.Realm;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Andrew Telkov
 */

public class TasksFragment extends Fragment implements View.OnClickListener, Serializable {
    private ListView tasksListView;
    private List<Task> taskList;
    private HorizontalCalendar calendar;
    private TextView tvEmptyList;
    private Date selectedDate = new Date();
    private Profile profile;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tasks, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view,  Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tasksListView = view.findViewById(R.id.list_view_tasks);
        tvEmptyList = view.findViewById(R.id.tv_empty_list);

        profile = Realm.getDefaultInstance().where(Profile.class).findFirst();

        calendarInit(view);

        view.findViewById(R.id.btn_create).setOnClickListener(this);
        view.findViewById(R.id.btn_calendar).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_create:
                startActivity(new Intent(getContext(), TaskCrea teEditActivity.class));
                break;
            case R.id.btn_calendar:
                if (calendar.getCalendarView().getVisibility() != View.GONE) {
                    calendar.getCalendarView().setVisibility(View.GONE);
                    showTasks(null);
                } else {
                    calendar.getCalendarView().setVisibility(View.VISIBLE);
                    showTasks(selectedDate);
                }
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        showTasks(null);
    }

    private void showTasks(Date date) {
        if (date != null) {
            taskList = Realm.getDefaultInstance().where(Task.class)
                    .equalTo("date", date)
                    .findAll()
                    .sort("date");
        } else {
            taskList = Realm.getDefaultInstance().where(Task.class)
                    .findAll()
                    .sort("date");
        }

        tvEmptyList.setVisibility(taskList.isEmpty() ? View.VISIBLE : View.GONE);

        TaskListAdapter adapter = new TaskListAdapter(getContext(), tasksListView, taskList);
        tasksListView.setAdapter(adapter);

        tasksListView.setOnItemClickListener((parent, view, position, id) -> {
            Task task = new Task();
            task.setId((taskList.get(position)).getId());
            task.setTitle((taskList.get(position)).getTitle());
            task.setDescription((taskList.get(position)).getDescription());
            task.setDate((taskList.get(position)).getDate());

            Intent intent = new Intent(getContext(), TaskCreateEditActivity.class);
            intent.putExtra("editMode", true);
            intent.putExtra("task", task);
            startActivity(intent);
        });
    }

    private void calendarInit(View view) {
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -1);

        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -4);

        calendar = new HorizontalCalendar.Builder(view, R.id.calendarView)
                .range(startDate, endDate)
                .datesNumberOnScreen(5)
                .configure()
                .showTopText(false)
                .end()
                .defaultSelectedDate(cal)
                .build();

        calendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar date, int position) {
                selectedDate = date.getTime();
                showTasks(selectedDate);
            }
        });
    }
}
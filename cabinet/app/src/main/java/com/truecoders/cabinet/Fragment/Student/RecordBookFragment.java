package com.truecoders.cabinet.Fragment.Student;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import com.james602152002.floatinglabelspinner.FloatingLabelSpinner;
import com.truecoders.cabinet.Adapter.SpinnerAdapter.SemesterSpinnerAdapter;
import com.truecoders.cabinet.Model.General.BaseClass;
import com.truecoders.cabinet.Model.Student.Profile;
import com.truecoders.cabinet.Model.Student.RecordBook;
import com.truecoders.cabinet.Model.Student.Semester;
import com.truecoders.cabinet.R;
import com.truecoders.cabinet.Utils.RetroClient;
import com.truecoders.expandingview.ExpandingList;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.truecoders.cabinet.Adapter.ListViewAdapter.Student.RatingListAdapter.renderRecordBookView;
import static com.truecoders.cabinet.DAO.Student.RatingDAO.addRecordBookToRealm;

public class RecordBookFragment extends Fragment {
    private List<String> spinnerList = new ArrayList<>();
    private FloatingLabelSpinner spinner;
    private ProgressBar progressBar;
    private ExpandingList expandingList;
    private RecordBook recordBook;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.subfragment_recordbook, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        spinner = view.findViewById(R.id.spinner);
        expandingList = view.findViewById(R.id.expanding_list_main);

        progressBar = view.findViewById(R.id.spin_kit);
        Profile profile = Realm.getDefaultInstance().where(Profile.class).findFirst();
        recordBook = Realm.getDefaultInstance().where(RecordBook.class).findFirst();

        spinner.setOnItemSelectedListener(spinnerOnItemSelected);
        spinner.setDropDownHintView(View.inflate(getContext(), R.layout.spinner_header, null));

        if (recordBook != null) {
            showSpinner(recordBook);
        } else if (profile != null) {
            RetroClient.getApiService().getRecordBook(
                    profile.getUsername(),
                    profile.getPassword()
            ).enqueue(new Callback<BaseClass>() {
                @Override
                public void onResponse(@NonNull Call<BaseClass> call, @NonNull Response<BaseClass> response) {
                    if (response.body() != null) {
                        recordBook = response.body().getRecordBook();
                        addRecordBookToRealm(recordBook);
                        showSpinner(recordBook);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<BaseClass> call, @NonNull Throwable t) {

                }
            });
        }
    }


    private void showSpinner(RecordBook recordBook) {
        for (Semester semester : recordBook.getSemesterList()) {
            spinnerList.add(semester.getSemesterName());
        }

        progressBar.setVisibility(View.GONE);
        spinner.setVisibility(View.VISIBLE);
        spinner.setAdapter(new SemesterSpinnerAdapter(getContext(), spinnerList));
    }

    private AdapterView.OnItemSelectedListener spinnerOnItemSelected = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position != 0) {
                expandingList.removeAllViews();
                renderRecordBookView(expandingList, Objects.requireNonNull(recordBook.getSemesterList().get(position - 1)).getSubjectList());

                AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
                anim.setDuration(500);
                anim.setRepeatCount(0);
                anim.setRepeatMode(Animation.REVERSE);
                progressBar.setVisibility(View.INVISIBLE);
                expandingList.setVisibility(View.VISIBLE);
                expandingList.startAnimation(anim);
                spinner.dismiss();
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };
}
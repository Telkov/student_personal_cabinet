package com.truecoders.cabinet.Adapter.PagerAdapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

/**
 * @author Maria Fabrichkina
 */

public class HostelCarouselAdapter extends PagerAdapter{

    private Context mContext;
    private int position;
    private String[] imagesList;

    public HostelCarouselAdapter (Context context, int position, String[] imagesList){
        this.mContext = context;
        this.position = position;
        this.imagesList = imagesList;
    }

    @Override
    public int getCount() {
        return imagesList.length;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        PhotoView photoView = new PhotoView(mContext);
        Picasso.get().load(imagesList[position]).into(photoView);
        photoView.setScaleType(ImageView.ScaleType.FIT_CENTER);

        container.addView(photoView);

        return photoView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((PhotoView) object);
    }

}

package com.truecoders.cabinet.DAO.Entrant;

import com.truecoders.cabinet.Model.Entrant.Marker;
import io.realm.Realm;

/**
 * @author Maria Fabrichkina
 */

public class MarkerDAO {

    public static void addMarkerToRealm(Marker marker) {

        Realm.getDefaultInstance().executeTransaction(realm -> {
            Marker realmObject = realm.createObject(Marker.class);

            realmObject.setLatitude(marker.getLatitude());
            realmObject.setLongitude(marker.getLongitude());
            realmObject.setTitle(marker.getTitle());
            realmObject.setSnippet(marker.getSnippet());
            realmObject.setIcon(marker.getIcon());
        });
    }
}

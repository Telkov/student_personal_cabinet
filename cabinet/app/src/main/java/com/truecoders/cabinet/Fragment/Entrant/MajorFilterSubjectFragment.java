package com.truecoders.cabinet.Fragment.Entrant;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.truecoders.cabinet.R;

import javax.annotation.Nullable;

/**
 * Created by Maria Fabrichkina on 06.03.19
 */


public class MajorFilterSubjectFragment extends Fragment {

    private int countOfSelected = 0;
    private LinearLayout llMath, llRussian, llPhysic, llHistory, llBiology, llInformatics, llChemistry, llLiterature,
            llInternal, llForeignLanguage, llSocialScience, llGeography;
    private TextView tvMath, tvRussian, tvPhysic, tvHistory, tvBiology, tvInformatics, tvChemistry, tvLiterature,
            tvInternal, tvForeignLanguage, tvSocialScience, tvGeography;
    private ImageView ivMath, ivRussian, ivPhysic, ivHistory, ivBiology, ivInformatics, ivChemistry, ivLiterature,
            ivInternal, ivForeignLanguage, ivSocialScience, ivGeography;
    private Boolean selectedMath = false, selectedRussian = false, selectedPhysic = false, selectedHistory = false,
            selectedBiology = false, selectedInformatics = false, selectedChemistry = false, selectedLiterature = false,
            selectedInternal = false, selectedForeignLanguage = false, selectedSocialScience = false, selectedGeography = false;
    private Button btnFurther;
    private Fragment fragment;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_major_filter_subject, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @android.support.annotation.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tvMath = view.findViewById(R.id.tvMath);
        llMath = view.findViewById(R.id.llMath);
        ivMath = view.findViewById(R.id.ivMath);
        llMath.setOnClickListener(v -> {
            selectedMath = click(ivMath, tvMath, selectedMath);
            countOfSelected();
        });

        tvRussian = view.findViewById(R.id.tvRussian);
        llRussian = view.findViewById(R.id.llRussian);
        ivRussian = view.findViewById(R.id.ivRussian);
        llRussian.setOnClickListener(v -> {
            selectedRussian = click(ivRussian, tvRussian, selectedRussian);
            countOfSelected();
        });

        tvPhysic = view.findViewById(R.id.tvPhysic);
        llPhysic = view.findViewById(R.id.llPhysic);
        ivPhysic = view.findViewById(R.id.ivPhysic);
        llPhysic.setOnClickListener(v -> {
            selectedPhysic = click(ivPhysic, tvPhysic, selectedPhysic);
            countOfSelected();
        });

        tvHistory = view.findViewById(R.id.tvHistory);
        llHistory = view.findViewById(R.id.llHistory);
        ivHistory = view.findViewById(R.id.ivHistory);
        llHistory.setOnClickListener(v -> {
            selectedHistory = click(ivHistory, tvHistory, selectedHistory);
            countOfSelected();
        });

        tvBiology = view.findViewById(R.id.tvBiology);
        llBiology = view.findViewById(R.id.llBiology);
        ivBiology = view.findViewById(R.id.ivBiology);
        llBiology.setOnClickListener(v -> {
            selectedBiology = click(ivBiology, tvBiology, selectedBiology);
            countOfSelected();
        });

        tvInformatics = view.findViewById(R.id.tvInformatics);
        llInformatics = view.findViewById(R.id.llInformatics);
        ivInformatics = view.findViewById(R.id.ivInformatics);
        llInformatics.setOnClickListener(v -> {
            selectedInformatics = click(ivInformatics, tvInformatics, selectedInformatics);
            countOfSelected();
        });

        tvChemistry = view.findViewById(R.id.tvChemistry);
        llChemistry = view.findViewById(R.id.llChemistry);
        ivChemistry = view.findViewById(R.id.ivChemistry);
        llChemistry.setOnClickListener(v -> {
            selectedChemistry = click(ivChemistry, tvChemistry, selectedChemistry);
            countOfSelected();
        });

        tvLiterature = view.findViewById(R.id.tvLiterature);
        llLiterature = view.findViewById(R.id.llLiterature);
        ivLiterature = view.findViewById(R.id.ivLiterature);
        llLiterature.setOnClickListener(v -> {
            selectedLiterature = click(ivLiterature, tvLiterature, selectedLiterature);
            countOfSelected();
        });

        tvInternal = view.findViewById(R.id.tvInternal);
        llInternal = view.findViewById(R.id.llInternal);
        ivInternal = view.findViewById(R.id.ivInternal);
        llInternal.setOnClickListener(v -> {
            selectedInternal = click(ivInternal, tvInternal, selectedInternal);
            countOfSelected();
        });

        tvForeignLanguage = view.findViewById(R.id.tvForeignLanguage);
        llForeignLanguage = view.findViewById(R.id.llForeignLanguage);
        ivForeignLanguage = view.findViewById(R.id.ivForeignLanguage);
        llForeignLanguage.setOnClickListener(v -> {
            selectedForeignLanguage = click(ivForeignLanguage, tvForeignLanguage, selectedForeignLanguage);
            countOfSelected();
        });

        tvSocialScience = view.findViewById(R.id.tvSocialScience);
        llSocialScience = view.findViewById(R.id.llSocialScience);
        ivSocialScience = view.findViewById(R.id.ivSocialScience);
        llSocialScience.setOnClickListener(v -> {
            selectedSocialScience = click(ivSocialScience, tvSocialScience, selectedSocialScience);
            countOfSelected();
        });

        tvGeography = view.findViewById(R.id.tvGeography);
        llGeography = view.findViewById(R.id.llGeography);
        ivGeography = view.findViewById(R.id.ivGeography);
        llGeography.setOnClickListener(v -> {
            selectedGeography = click(ivGeography, tvGeography, selectedGeography);
            countOfSelected();
        });

        btnFurther = view.findViewById(R.id.btnFurther);
        btnFurther.setOnClickListener(v -> {

                Bundle bundle = this.getArguments();

                fragment = new MajorListResultFragment();

                Bundle bundleResult = new Bundle();
                bundleResult.putBoolean("cbInternal", bundle.getBoolean("cbInternal"));
                bundleResult.putBoolean("cbCorrespondence", bundle.getBoolean("cbCorrespondence"));
                bundleResult.putBoolean("cbInternalCorrespondence", bundle.getBoolean("cbInternalCorrespondence"));
                bundleResult.putBoolean("cbBudget", bundle.getBoolean("cbBudget"));
                bundleResult.putBoolean("cbPay", bundle.getBoolean("cbPay"));

                if (bundle.getBoolean("cbPay") == true) {
                    bundleResult.putString("etCost", bundle.getString("etCost"));
                }

                bundleResult.putBoolean("selectedMath", selectedMath.booleanValue());
                bundleResult.putBoolean("selectedRussian", selectedRussian.booleanValue());
                bundleResult.putBoolean("selectedInformatics", selectedInformatics.booleanValue());
                bundleResult.putBoolean("selectedPhysic", selectedPhysic.booleanValue());
                bundleResult.putBoolean("selectedForeignLanguage", selectedForeignLanguage.booleanValue());
                bundleResult.putBoolean("selectedChemistry", selectedChemistry.booleanValue());
                bundleResult.putBoolean("selectedHistory", selectedHistory.booleanValue());
                bundleResult.putBoolean("selectedSocialScience", selectedSocialScience.booleanValue());
                bundleResult.putBoolean("selectedLiterature", selectedLiterature.booleanValue());
                bundleResult.putBoolean("selectedBiology", selectedBiology.booleanValue());
                bundleResult.putBoolean("selectedGeography", selectedGeography.booleanValue());
                bundleResult.putBoolean("selectedInternal", selectedInternal.booleanValue());

                fragment.setArguments(bundleResult);

                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.content_frame_inst, fragment);
                //ft.addToBackStack(null);
                ft.commit();

        });
    }

    public Boolean click(ImageView imageView, TextView textView, Boolean flag) {
        if (flag == false) {
            textView.setTextColor(getResources().getColor(R.color.colorOrange));
            imageView.setColorFilter(getResources().getColor(R.color.colorOrange));
            flag = true;
            countOfSelected += 1;
        } else {
            textView.setTextColor(getResources().getColor(R.color.colorWhite));
            imageView.setColorFilter(getResources().getColor(R.color.colorWhite));
            flag = false;
            countOfSelected -= 1;
        }
        return flag;
    }

    public void countOfSelected() {
        if (countOfSelected >= 3) {
            accessToItems(0);
        } else {
            accessToItems(1);
        }
    }

    public void accessToItems(int flag) {
        if (flag == 0) {
            if (selectedMath == false) {
                enabledItem(0, llMath, tvMath, ivMath);
            }
            if (selectedRussian == false) {
                enabledItem(0, llRussian, tvRussian, ivRussian);
            }
            if (selectedPhysic == false) {
                enabledItem(0, llPhysic, tvPhysic, ivPhysic);
            }
            if (selectedHistory == false) {
                enabledItem(0, llHistory, tvHistory, ivHistory);
            }
            if (selectedBiology == false) {
                enabledItem(0, llBiology, tvBiology, ivBiology);
            }
            if (selectedInformatics == false) {
                enabledItem(0, llInformatics, tvInformatics, ivInformatics);
            }
            if (selectedChemistry == false) {
                enabledItem(0, llChemistry, tvChemistry, ivChemistry);
            }
            if (selectedLiterature == false) {
                enabledItem(0, llLiterature, tvLiterature, ivLiterature);
            }
            if (selectedInternal == false) {
                enabledItem(0, llInternal, tvInternal, ivInternal);
            }
            if (selectedForeignLanguage == false) {
                enabledItem(0, llForeignLanguage, tvForeignLanguage, ivForeignLanguage);
            }
            if (selectedSocialScience == false) {
                enabledItem(0, llSocialScience, tvSocialScience, ivSocialScience);
            }
            if (selectedGeography == false) {
                enabledItem(0, llGeography, tvGeography, ivGeography);
            }

        } else if (flag == 1) {
            if (selectedMath == false) {
                enabledItem(1, llMath, tvMath, ivMath);
            }
            if (selectedRussian == false) {
                enabledItem(1, llRussian, tvRussian, ivRussian);
            }
            if (selectedPhysic == false) {
                enabledItem(1, llPhysic, tvPhysic, ivPhysic);
            }
            if (selectedHistory == false) {
                enabledItem(1, llHistory, tvHistory, ivHistory);
            }
            if (selectedBiology == false) {
                enabledItem(1, llBiology, tvBiology, ivBiology);
            }
            if (selectedInformatics == false) {
                enabledItem(1, llInformatics, tvInformatics, ivInformatics);
            }
            if (selectedChemistry == false) {
                enabledItem(1, llChemistry, tvChemistry, ivChemistry);
            }
            if (selectedLiterature == false) {
                enabledItem(1, llLiterature, tvLiterature, ivLiterature);
            }
            if (selectedInternal == false) {
                enabledItem(1, llInternal, tvInternal, ivInternal);
            }
            if (selectedForeignLanguage == false) {
                enabledItem(1, llForeignLanguage, tvForeignLanguage, ivForeignLanguage);
            }
            if (selectedSocialScience == false) {
                enabledItem(1, llSocialScience, tvSocialScience, ivSocialScience);
            }
            if (selectedGeography == false) {
                enabledItem(1, llGeography, tvGeography, ivGeography);
            }
        }
    }

    public void enabledItem(int flag, LinearLayout layout, TextView textView, ImageView imageView) {
        if (flag == 0) {
            layout.setEnabled(false);
            textView.setTextColor(getResources().getColor(R.color.colorLightGray));
            imageView.setColorFilter(getResources().getColor(R.color.colorLightGray));
        } else {
            layout.setEnabled(true);
            textView.setTextColor(getResources().getColor(R.color.colorWhite));
            imageView.setColorFilter(getResources().getColor(R.color.colorWhite));
        }
    }
}

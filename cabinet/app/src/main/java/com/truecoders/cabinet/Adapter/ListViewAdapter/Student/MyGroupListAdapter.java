package com.truecoders.cabinet.Adapter.ListViewAdapter.Student;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.truecoders.cabinet.Fragment.Student.ProfileFragment;
import com.truecoders.cabinet.Model.Student.Profile;
import com.truecoders.cabinet.R;

import java.util.List;

/**
 * @author Ilya Baykalov
 */

public class MyGroupListAdapter extends ArrayAdapter<Profile> {
    private LayoutInflater inflater;
    private List<Profile> profileList;

    public MyGroupListAdapter(Context context, List<Profile> profileList) {
        super(context, R.layout.adapter_my_group, profileList);
        this.profileList = profileList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull final ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.adapter_my_group, parent, false);
        }

        Profile profile = profileList.get(position);
        ((TextView) view.findViewById(R.id.full_name)).setText(String.format("%s", profile.getFullName()));

//        if (profile.isCaptain()) {
//            view.findViewById(R.id.captain).setVisibility(View.VISIBLE);
//        }

        view.setOnClickListener(v -> {
            if (((AppCompatActivity) getContext()).getSupportFragmentManager() != null) {
                Bundle args = new Bundle();
                args.putSerializable("profile", profile);
                String CURRENT_FRAGMENT = "CURRENT FRAGMENT";

                FragmentTransaction fragmentTransaction = ((AppCompatActivity) getContext()).getSupportFragmentManager().beginTransaction();

                Fragment fragment = new ProfileFragment();
                fragment.setArguments(args);
                fragmentTransaction.replace(R.id.content_frame, fragment, CURRENT_FRAGMENT);
                fragmentTransaction.addToBackStack(CURRENT_FRAGMENT);
                fragmentTransaction.commit();
            }
        });

        return view;
    }
}

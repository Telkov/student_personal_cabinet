package com.truecoders.cabinet.Model.Student;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import io.realm.RealmList;
import io.realm.RealmObject;

public class ECoursesList extends RealmObject {

    @SerializedName("eCourses")
    @Expose
    private RealmList<ECourse> eCourses = null;
    @SerializedName("eCourseName")
    @Expose
    private String eCourseName;

    public RealmList<ECourse> getECourses() {
        return eCourses;
    }

    public void setECourses(RealmList<ECourse> eCourses) {
        this.eCourses = eCourses;
    }

    public String getECourseName() {
        return eCourseName;
    }

    public void setECourseName(String eCourseName) {
        this.eCourseName = eCourseName;
    }

}
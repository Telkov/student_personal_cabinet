package com.truecoders.cabinet.Utils;

import com.truecoders.cabinet.DAO.ApiService;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.concurrent.TimeUnit;

/**
 * @author Ilya Baykalov
 */

public class RetroClient {

    private static String getHostAPI() {
        return "http://10.0.2.2:3000/api/";
        //return "http://89.223.92.36:3000/api/";
    }

    private static OkHttpClient getRequestHeader() {
        return new OkHttpClient.Builder()
                .readTimeout(600, TimeUnit.SECONDS)
                .build();
    }


    private static Retrofit getRetrofitInstance() {
        return new Retrofit.Builder()
                .baseUrl(getHostAPI())
                .client(getRequestHeader())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static ApiService getApiService() {
        return getRetrofitInstance().create(ApiService.class);
    }
}
package com.truecoders.cabinet.Fragment.Student;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.truecoders.cabinet.Adapter.ListViewAdapter.Student.MyGroupListAdapter;
import com.truecoders.cabinet.Model.General.BaseClass;
import com.truecoders.cabinet.Model.Student.Profile;
import com.truecoders.cabinet.R;
import com.truecoders.cabinet.Utils.RetroClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;

/**
 * @author Ilya Baykalov
 */

public class MyGroupFragment extends Fragment {
    private ListView myGroupListView;
    private ProgressBar progressBar;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_group, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        myGroupListView = view.findViewById(R.id.list_view_my_group);
        progressBar = view.findViewById(R.id.spin_kit);

        RetroClient.getApiService().getClassmates()
                .enqueue(new Callback<BaseClass>() {
                    @Override
                    public void onResponse(@NonNull Call<BaseClass> call, @NonNull Response<BaseClass> response) {
                        if (response.body() != null) {
                            List<Profile> classmates = response.body().getClassmates();
                            MyGroupListAdapter adapter = new MyGroupListAdapter(getContext(), classmates);
                            progressBar.setVisibility(View.GONE);
                            myGroupListView.setAdapter(adapter);
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<BaseClass> call, @NonNull Throwable t) {

                    }
                });
//        myGroupListView = view.findViewById(R.id.list_view_my_group);
//        progressBar = view.findViewById(R.id.spin_kit);
//        Profile profile = Realm.getDefaultInstance().where(Profile.class).findFirst();
//
//        if (Realm.getDefaultInstance().where(Profile.class).count() <= 1) {
//            if (profile != null) {
//                new RetroClient().POST("classmates/get",
//                        "{\"idGroup\":\"" + profile.getIdDicGroup() + "\"}",
//                        new Callback() {
//                            @Override
//                            public void onFailure(@NonNull Call call, @NonNull IOException e) {
//                            }
//
//                            @Override
//                            public void onResponse(@NonNull Call call, @NonNull Response response) {
//                                try {
//                                    if (response.body() != null) {
//                                        new MyGroupTask().execute(response.body().string());
//                                    }
//                                } catch (IOException e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                        });
//            }
//        } else {
//            showMyGroup();
//        }
    }

//    private void showMyGroup() {
//        List<Profile> profileList = Realm.getDefaultInstance().where(Profile.class).findAll();
//        MyGroupListAdapter adapter = new MyGroupListAdapter(getContext(), profileList.subList(1, profileList.size()));
//        progressBar.setVisibility(View.GONE);
//        myGroupListView.setAdapter(adapter);
//    }

//    @SuppressLint("StaticFieldLeak")
//    private class MyGroupTask extends AsyncTask<String, Void, String> {
//        @Override
//        protected String doInBackground(String... strings) {
//            return strings[0];
//        }
//
//        protected void onPostExecute(String result) {
//            List<Profile> profileList = getProfileArrayData(result);
//            for (Profile profile : profileList) {
//                addProfileToRealm(profile);
//            }
//            showMyGroup();
//        }
//    }
}
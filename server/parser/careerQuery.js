let Webdriver = require('selenium-webdriver');
let chromeDriver = require('selenium-webdriver/chrome');
const options = new chromeDriver.Options();
options.addArguments(
    'headless',
    'disable-gpu',
    'no-sandbox'
);
let fs = require("fs");

let Career = require('../parser/model/career');

module.exports = {
    getCareer: getCareer
};

async function getCareer(req, res) {

    let username = req.body.username;
    let password = req.body.password;
    // let username = "ATelkov-KI15";
    // let password = "Ubgthdfk123";

    console.log("open Google Chrome");

    let driver = new Webdriver.Builder()
        .forBrowser('chrome')
        .setChromeOptions(options)
        .build();

    console.log("open http://v.sfu-kras.ru/login");
    driver.get('http://v.sfu-kras.ru/login');

    let loginInput = driver.findElement(Webdriver.By.name("_username"));
    let passInput = driver.findElement(Webdriver.By.name("_password"));

    console.log("input login");
    await loginInput.sendKeys(username);
    console.log("input password");
    await passInput.sendKeys(password);

    console.log("click button");
    console.log("LOADING...");
    await driver.findElement(Webdriver.By.className("btn-success")).click();

    console.log("open events page");
    driver.get('http://v.sfu-kras.ru/events');

    let arragment = [];
    let counts = await driver.findElements(Webdriver.By.css("body > div > div:nth-child(3) > div"));
    for (let i = 0; i < counts.length; i++) {
        let event = new Career();
        event.setTitle = await (driver.findElement(Webdriver.By.css("body > div > div:nth-child(3) > div:nth-child(" + (i + 1) + ") > div > div.col-sm-8.px-3 > div > h4 > a"))).getText();
        event.setLink = await (driver.findElement(Webdriver.By.css("body > div > div:nth-child(3) > div:nth-child(" + (i + 1) + ") > div > div.col-sm-8.px-3 > div > h4 > a"))).getAttribute("href");
        event.setImage = await (driver.findElement(Webdriver.By.css("body > div > div:nth-child(3) > div:nth-child(" + (i + 1) + ") > div > div.col-sm-4 > img"))).getAttribute("src");
        event.setDate = await (driver.findElement(Webdriver.By.css("body > div > div:nth-child(3) > div:nth-child(" + (i + 1) + ") > div > div.col-sm-8.px-3 > div > p:nth-child(2)"))).getText();
        event.setStatus = "end";
        arragment.push(event);
    }
    let counts1 = await driver.findElements(Webdriver.By.css("body > div > div:nth-child(1) > div.col-sm-8 > div > div"));
    for (let j = 0; j < counts1.length; j++) {
        let event = new Career();
        event.setTitle = await (driver.findElement(Webdriver.By.css("body > div > div:nth-child(" + (j + 1) + ") > div.col-sm-8 > div > div > div.col-sm-8 > div > h4 > a"))).getText();
        event.setLink = await (driver.findElement(Webdriver.By.css("body > div > div:nth-child(" + (j + 1) + ") > div.col-sm-8 > div > div > div.col-sm-8 > div > h4 > a"))).getAttribute("href");
        event.setImage = await (driver.findElement(Webdriver.By.css("body > div > div:nth-child(" + (j + 1) + ") > div.col-sm-8 > div > div > div.col-sm-4 > img"))).getAttribute("src");
        event.setDate = await (driver.findElement(Webdriver.By.css("body > div > div:nth-child(" + (j + 1) + ") > div.col-sm-8 > div > div > div.col-sm-8 > div > p:nth-child(2)"))).getText();
        event.setPeople = await (driver.findElement(Webdriver.By.css("body > div > div:nth-child(" + (j + 1) + ") > div.col-sm-8 > div > div > div.col-sm-8 > div > p:nth-child(6)"))).getText();
        event.setStatus = "now";
        arragment.push(event);
    }
    res.status(200)
        .json({
            arragment
        });
}
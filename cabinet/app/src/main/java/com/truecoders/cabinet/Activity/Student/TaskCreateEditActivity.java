package com.truecoders.cabinet.Activity.Student;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.truecoders.cabinet.Model.Student.Schedule;
import com.truecoders.cabinet.Model.Student.Task;
import com.truecoders.cabinet.R;
import com.truecoders.cabinet.Utils.Notification.Alarm;
import com.truecoders.cabinet.Utils.Notification.NothingSelectedSpinnerAdapter;
import io.realm.Realm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.truecoders.cabinet.DAO.Student.TaskDAO.addTaskToRealm;
import static com.truecoders.cabinet.DAO.Student.TaskDAO.updateTaskIntoRealm;

/**
 * @author Ilya Baykalov
 */

public class TaskCreateEditActivity extends AppCompatActivity {
    private Boolean editMode;
    private Boolean timeChanged;
    private EditText editTextTitle;
    private EditText editTextDescription;
    private MaterialCalendarView materialCalendarView;
    private Task task;
    private Spinner sItems;
    private TimePicker timePicker;
    private CheckBox checkBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_create_edit);

        getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorMainBackgroundFinish));

        task = (Task) getIntent().getSerializableExtra("task");
        editMode = getIntent().getBooleanExtra("editMode", false);
        timeChanged = false;

        editTextTitle = findViewById(R.id.task_title);
        editTextDescription = findViewById(R.id.task_description);

        checkBox = findViewById(R.id.send_notification);

        sItems = findViewById(R.id.subject_spinner);
        timePicker = findViewById(R.id.time_picker);
        timePicker.setIs24HourView(true);

        materialCalendarView = findViewById(R.id.calendar_view_task);
        materialCalendarView.setLeftArrow(R.drawable.ic_iconmonstr_arrow_left_16dp);
        materialCalendarView.setRightArrow(R.drawable.ic_iconmonstr_arrow_right_16dp);
        materialCalendarView.setSelectedDate(materialCalendarView.getCurrentDate());

        materialCalendarView.setOnDateChangedListener((materialCalendarView, calendarDay, b) -> getSubjects());

        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                timeChanged = true;
                Log.i("Time", "Changed");
            }
        });

        getSubjects();

        if (editMode) {
            editTextTitle.setText(task.getTitle());
            editTextDescription.setText(task.getDescription());
        }

        findViewById(R.id.fab_save).setOnClickListener(saveTask);
    }

    private View.OnClickListener saveTask = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (editMode) {
                editTask();
            } else {
                try {
                    createTask();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String text;
                text = (String) sItems.getSelectedItem();
//                if (!text.equals("null")) {
//                    Log.i("Значение спиннера", text);
//                }
//                else
//                {
//                    Log.i("Значение спиннера", "!!!!!!!!!!!!!!!!!!!");
//                }
            }
        }
    };

    private void editTask() {
        if (!editTextTitle.getText().toString().isEmpty()) {
            try {
                CalendarDay day = materialCalendarView.getSelectedDate();
                Task updateTask = new Task(task.getId(),
                        editTextTitle.getText().toString(),
                        editTextDescription.getText().toString(),
                        new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
                                .parse(day.getDay() + "."
                                        + (day.getMonth() + 1) + "."
                                        + day.getYear()));

                updateTaskIntoRealm(updateTask);

            } catch (ParseException ignored) {
            }
            finish();
        } else {
            Toast.makeText(getApplicationContext(), R.string.task_error_creation, Toast.LENGTH_SHORT).show();
        }
    }

    private void createTask() throws ParseException {
        CalendarDay day = materialCalendarView.getSelectedDate();
        if (!editTextTitle.getText().toString().isEmpty()) {
            try {
                Date test = new SimpleDateFormat("dd.M.yyyy", Locale.getDefault())
                        .parse(day.getDay() + "."
                                + day.getMonth() + "."
                                + day.getYear());
                //Day day = collapsibleCalendar.getSelectedDay();
                Task task = new Task(editTextTitle.getText().toString(),
                        editTextDescription.getText().toString(),
                        new SimpleDateFormat("dd.M.yyyy", Locale.getDefault())
                                .parse(day.getDay() + "."
                                        + day.getMonth() + "."
                                        + day.getYear()));

                addTaskToRealm(task);
                Calendar calendar = new GregorianCalendar();
                if (checkBox.isChecked()) {
                    if (timeChanged.equals(true)) {
                        if (android.os.Build.VERSION.SDK_INT >= 23) {
                            calendar.set(day.getYear(),
                                    day.getMonth(),
                                    day.getDay(),
                                    timePicker.getHour(),
                                    timePicker.getMinute() - 1,
                                    0);
                            setAlarm(calendar.getTimeInMillis());
                        } else {
                            //TODO another SDK version
                        }
                    } else {
                        if (android.os.Build.VERSION.SDK_INT >= 23) {
                            calendar.set(day.getYear(),
                                    day.getMonth(),
                                    day.getDay(),
                                    0,
                                    0,
                                    0);
                            setAlarm(calendar.getTimeInMillis());
                        } else {
                            //TODO another SDK version
                        }
                    }
                }
            } catch (ParseException ignored) {
            }
            finish();
        } else {
            Toast.makeText(getApplicationContext(), R.string.task_error_creation, Toast.LENGTH_SHORT).show();
        }
    }

    private void getSubjects() {
        Integer day = Objects.requireNonNull(materialCalendarView.getSelectedDate()).getDate().getDayOfWeek().getValue();
        Set<String> set = new LinkedHashSet<>();
        //set.add("Выберите предмет...");
        for (Schedule schedule : Realm.getDefaultInstance().where(Schedule.class)
                .equalTo("day", day)
                .equalTo("week", getCurrentWeek())
                .findAll()) {
            String subjectName = schedule.getSubjectName();
            set.add(subjectName);
        }

        List<String> list = new ArrayList<>(set);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, list);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sItems.setAdapter(new NothingSelectedSpinnerAdapter(
                adapter,
                R.layout.contact_spinner_row_nothing_selected,
                // R.layout.contact_spinner_nothing_selected_dropdown, // Optional
                this));
    }

    private void setAlarm(long time) {
        //getting the alarm manager
        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        //creating a new intent specifying the broadcast receiver
        Intent i = new Intent(this, Alarm.class);

        //creating a pending intent using the intent
        PendingIntent pi = PendingIntent.getBroadcast(this, 0, i, 0);

        //setting the repeating alarm that will be fired every day
        am.setRepeating(AlarmManager.RTC, time, AlarmManager.INTERVAL_DAY, pi);
        Toast.makeText(this, "Alarm is set", Toast.LENGTH_SHORT).show();
    }

    private String loadText() {
        SharedPreferences sPref = getSharedPreferences("pref", Context.MODE_PRIVATE);
        return sPref.getString("current_week", "");
    }

    private Integer getCurrentWeek() {
        Calendar calendar = Calendar.getInstance();
        Date date = null;
        try {
            CalendarDay day = materialCalendarView.getSelectedDate();
            date = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
                    .parse(day.getDay() + "."
                            + (day.getMonth() + 1) + "."
                            + day.getYear());
            calendar.setTime(date);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Integer day = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        Integer currentWeekOfYear = Calendar.getInstance().get(Calendar.WEEK_OF_YEAR);
        Integer selectedWeek = calendar.get(Calendar.WEEK_OF_YEAR);

        int currentWeek = Integer.parseInt(loadText());

        return ((Math.abs(selectedWeek - currentWeekOfYear)) % 2) == 1 ? (currentWeek == 1 ? 2 : 1) : currentWeek;
    }
}

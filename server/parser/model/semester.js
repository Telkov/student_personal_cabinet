class Semester {

    constructor(){
        this.subjectList = [];
    }

    get getSubjectList() {
        return this.subjectList;
    }
}

module.exports = Semester;
package com.truecoders.cabinet.Fragment.Entrant;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.truecoders.cabinet.Adapter.ListViewAdapter.Entrant.InstituteListAdapter;
import com.truecoders.cabinet.Model.Entrant.Institute;
import com.truecoders.cabinet.Model.General.BaseClass;
import com.truecoders.cabinet.R;
import com.truecoders.cabinet.Utils.RetroClient;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;

import static com.truecoders.cabinet.DAO.Entrant.InstitutesDAO.addInstituteToRealm;

/**
 * @author Maria Fabrichkina
 */

public class InstitutesFragment extends android.support.v4.app.Fragment {

    private ListView listView;
    private ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fargment_list_of_institutions, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listView = view.findViewById(R.id.lvInstitutes);
        progressBar = view.findViewById(R.id.spin_kit);

        addInstitutes();
    }

    private void addInstitutes() {
        if (Realm.getDefaultInstance().where(Institute.class).count() == 0) {
            RetroClient.getApiService().getInstitutes()
                    .enqueue(new Callback<BaseClass>() {
                        @Override
                        public void onResponse(@NonNull Call<BaseClass> call, @NonNull Response<BaseClass> response) {
                            List<Institute> instituteList = response.body() != null ? response.body().getInstitutes() : null;

                            if (instituteList != null) {
                                for (Institute institute : instituteList) {
                                    addInstituteToRealm(institute);
                                }
                            }
                            showInstitutes();
                        }

                        @Override
                        public void onFailure(@NonNull Call<BaseClass> call, @NonNull Throwable t) {

                        }
                    });
        } else {
            showInstitutes();
        }
    }

    private void showInstitutes() {
        List<Institute> instituteList = Realm.getDefaultInstance().where(Institute.class).findAll();
        InstituteListAdapter adapter = new InstituteListAdapter(getContext(), instituteList);
        progressBar.setVisibility(View.GONE);
        listView.setAdapter(adapter);
    }
}



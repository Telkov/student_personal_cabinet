package com.truecoders.cabinet.Utils.Notification;

import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import com.truecoders.cabinet.Activity.Student.TaskCreateEditActivity;
import com.truecoders.cabinet.R;

import java.util.Date;
import java.util.Objects;

public class NotificationService extends Service {
    private NotificationManager mNM;

    // Unique Identification Number for the Notification.
    // We use it on Notification start, and to cancel it.
    private int NOTIFICATION = R.string.send_notification;

    /**
     * Class for clients to access.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with
     * IPC.
     */
    public class LocalBinder extends Binder {
        NotificationService getService() {
            return NotificationService.this;
        }
    }

    @Override
    public void onCreate() {
        Context context = getApplicationContext();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel notificationChannel = new NotificationChannel("ID", "Name", importance);
            Objects.requireNonNull(notificationManager).createNotificationChannel(notificationChannel);
            builder = new NotificationCompat.Builder(getApplicationContext(), notificationChannel.getId());
        } else {
            builder = new NotificationCompat.Builder(getApplicationContext());
        }

        builder = builder
                .setSmallIcon(R.drawable.ic_sfu_logo_24dp)
                .setColor(ContextCompat.getColor(context, R.color.colorGoldStart))
                .setContentTitle(context.getString(R.string.app_name))
                .setTicker(context.getString(R.string.app_name))
                .setContentText(new Date().toString())
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true);
        Objects.requireNonNull(notificationManager).notify(new Date().getMinutes(), builder.build());
//        mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
//
//        // Display a notification about us starting.  We put an icon in the status bar.
//        showNotification();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("LocalService", "Received start id " + startId + ": " + intent);
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        // Cancel the persistent notification.
        //mNM.cancel(NOTIFICATION);

        // Tell the user we stopped.
        //Toast.makeText(this, R.string.local_service_stopped, Toast.LENGTH_SHORT).show();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    // This is the object that receives interactions from clients.  See
    // RemoteService for a more complete example.
    private final IBinder mBinder = new LocalBinder();

    private void showNotification() {
        // In this sample, we'll use the same text for the ticker and the expanded notification
        CharSequence text = getText(R.string.send_notification);

        // The PendingIntent to launch our activity if the user selects this notification
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, TaskCreateEditActivity.class), 0);
        String channelId = "DefaultChannel";

        // Set the info for the views that show in the notification panel.
        NotificationCompat.Builder notification_compat_builder = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.ic_sfu_logo_24dp)
                .setContentTitle("Обратите внимание на ваши задачи!")
                .setContentText("Сдай МСО, падла!");

//        Notification notification = new Notification.Builder(this)
//                .setSmallIcon(R.drawable.ic_launcher_foreground)  // the status icon
//                .setTicker(text)  // the status text
//                .setWhen(System.currentTimeMillis())  // the time stamp
//                .setContentTitle(getText(R.string.local_service_label))  // the label of the entry
//                .setContentText(text)  // the contents of the entry
//                .setContentIntent(contentIntent)  // The intent to send when the entry is clicked
//                .build();

        // Send the notification.
        mNM.notify(NOTIFICATION, notification_compat_builder.build());
    }
}

package com.truecoders.cabinet.Activity.Student;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import com.truecoders.cabinet.Fragment.Student.OrderCreateFragment;
import com.truecoders.cabinet.Fragment.Student.OrderHistoryFragment;
import com.truecoders.cabinet.R;

/**
 * @author Ilya Baykalov
 */

public class DocumentOrderActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnCreateOrder;
    private Button btnGetHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document);

        getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorHeader));

        btnCreateOrder = (findViewById(R.id.btn_set_document));
        btnCreateOrder.setOnClickListener(this);
        btnGetHistory = (findViewById(R.id.btn_get_document));
        btnGetHistory.setOnClickListener(this);

        displaySelectedScreen(0);
    }


    private void displaySelectedScreen(int code) {
        Fragment fragment = null;
        switch (code) {
            case 0:
                fragment = new OrderCreateFragment();
                btnCreateOrder.setBackgroundResource(R.drawable.background_invisible);
                btnGetHistory.setBackgroundResource(R.color.background_buttons_document);
                break;
            case 1:
                fragment = new OrderHistoryFragment();
                btnGetHistory.setBackgroundResource(R.drawable.background_invisible);
                btnCreateOrder.setBackgroundResource(R.color.background_buttons_document);
                break;
        }
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_set_document:
                displaySelectedScreen(0);
                break;
            case R.id.btn_get_document:
                displaySelectedScreen(1);
                break;
        }
    }
}

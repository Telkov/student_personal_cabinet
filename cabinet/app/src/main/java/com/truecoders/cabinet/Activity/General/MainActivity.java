package com.truecoders.cabinet.Activity.General;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.TextView;
import com.truecoders.cabinet.Activity.Entrant.EntrantActivity;
import com.truecoders.cabinet.Activity.Student.StudentActivity;
import com.truecoders.cabinet.Model.General.BaseClass;
import com.truecoders.cabinet.Model.Student.Profile;
import com.truecoders.cabinet.R;
import com.truecoders.cabinet.Utils.RetroClient;
import github.ishaan.buttonprogressbar.ButtonProgressBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.Serializable;

import static com.truecoders.cabinet.DAO.Student.ProfileDAO.addProfileToRealm;

/**
 * @author Ilya Baykalov
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ConstraintLayout layoutAuth;
    private ConstraintLayout layoutButton;
    private EditText inputLogin, inputPass;
    private TextView error;
    private ButtonProgressBar btnEntrance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorMainBackgroundFinish));

        layoutAuth = findViewById(R.id.layout_auth);
        layoutButton = findViewById(R.id.layout_main_activity_button);

        inputLogin = findViewById(R.id.input_login);
        inputPass = findViewById(R.id.input_pass);
        error = findViewById(R.id.error);

        whoAreYou();

        findViewById(R.id.btn_student).setOnClickListener(this);
        findViewById(R.id.btn_university_entrant).setOnClickListener(this);
        inputLogin.setOnClickListener(this);
        inputPass.setOnClickListener(this);

        btnEntrance = findViewById(R.id.btn_entrance);
        btnEntrance.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorTransparent));
        btnEntrance.setProgressColor(ContextCompat.getColor(getApplicationContext(), R.color.colorTranslucent));
        btnEntrance.setOnClickListener(this);

//        inputLogin.setText("EUstinova-BB15");
//        inputPass.setText("89836120300");
//        inputLogin.setText("SProkopenko-EE15");
//        inputPass.setText("MERSERG");
//        inputLogin.setText("Mryazanova-bb15");
//        inputPass.setText("89659056099");
//        inputLogin.setText("ATelkov-KI15");
//        inputPass.setText("Ubgthdfk123");
        inputLogin.setText("IBaykalov-KI15");
        inputPass.setText("147369258");
//        inputLogin.setText("MFabrichkina-KI15");
//        inputPass.setText("12qazxsw21");
    }

    private void whoAreYou() {
        boolean isStudent = getIntent().getBooleanExtra("isStudent", false);

        if (isStudent) {
            layoutFadeInOut(layoutAuth, 0);
            layoutButton.setVisibility(View.INVISIBLE);
        } else {
            layoutAuth.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_student:
                layoutFadeInOut(layoutAuth, 0);
                layoutFadeInOut(layoutButton, 1);
                break;

            case R.id.btn_university_entrant:
                Intent intent = new Intent(this, EntrantActivity.class);
                startActivity(intent);
                break;

            case R.id.btn_entrance:
                btnEntrance.startLoader();
                error.setVisibility(View.GONE);
                RetroClient.getApiService().getProfile(
                        inputLogin.getText().toString(),
                        inputPass.getText().toString()
                ).enqueue(new Callback<BaseClass>() {
                    @Override
                    public void onResponse(@NonNull Call<BaseClass> call, @NonNull Response<BaseClass> response) {
                        btnEntrance.stopLoader();
                    if (response.body() != null) {
                            Profile profile = response.body().getProfile();

                            if (profile != null) {
                                addProfileToRealm(profile);
                            }
                            startActivity(new Intent(getApplicationContext(), StudentActivity.class)
                                    .putExtra("possibleGroups", (Serializable) response.body().getPossibleGroups()));
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<BaseClass> call, @NonNull Throwable t) {
                        error.setVisibility(View.VISIBLE);
                    }
                });
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (layoutAuth.getVisibility() == View.VISIBLE) {
            layoutFadeInOut(layoutAuth, 1);
            layoutFadeInOut(layoutButton, 0);
        }
    }

    private void layoutFadeInOut(ConstraintLayout layout, Integer mode) {
        AlphaAnimation anim = null;
        switch (mode) {
            case 0:
                anim = new AlphaAnimation(0.0f, 1.0f);
                layout.setVisibility(View.VISIBLE);
                anim.setDuration(800);
                break;
            case 1:
                anim = new AlphaAnimation(1.0f, 0.0f);
                layout.setVisibility(View.INVISIBLE);
                anim.setDuration(400);
                break;
        }
        if (anim != null) {
            anim.setRepeatCount(0);
            anim.setRepeatMode(Animation.REVERSE);
        }
        layout.startAnimation(anim);
    }
}

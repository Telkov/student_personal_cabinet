package com.truecoders.cabinet.DAO;

import com.truecoders.cabinet.Model.General.BaseClass;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiService {

    // Student
    @POST("student/get")
    @FormUrlEncoded
    Call<BaseClass> getProfile(
            @Field("username") String username,
            @Field("password") String password
    );

    @POST("schedule/get")
    @FormUrlEncoded
    Call<BaseClass> getSchedule(
            @Field("groupLink") String groupLink
    );

    @POST("rating/recordbook/get")
    @FormUrlEncoded
    Call<BaseClass> getRecordBook(
            @Field("username") String username,
            @Field("password") String password
    );

    @POST("rating/progress/get")
    @FormUrlEncoded
    Call<BaseClass> getProgress(
            @Field("username") String username,
            @Field("password") String password
    );

    @POST("news/get")
    @FormUrlEncoded
    Call<BaseClass> getNews(
            @Field("institute") String institute
    );

    @POST("career/get")
    @FormUrlEncoded
    Call<BaseClass> getCareer(
            @Field("username") String username,
            @Field("password") String password
    );

    @POST("classmates/get")
    Call<BaseClass> getClassmates();

    // Entrant
    @POST("entrant/contacts/get")
    Call<BaseClass> getContacts();

    @POST("entrant/majors/get")
    Call<BaseClass> getMajors();

    @POST("entrant/institutes/get")
    Call<BaseClass> getInstitutes();

    @POST("entrant/hostels/get")
    Call<BaseClass> getHostels();

    @POST("entrant/markers/get")
    Call<BaseClass> getMarkers();

}

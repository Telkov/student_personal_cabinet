package com.truecoders.cabinet.Fragment.Entrant;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.truecoders.cabinet.Adapter.ListViewAdapter.Entrant.HostelListAdapter;
import com.truecoders.cabinet.Model.Entrant.Hostel;
import com.truecoders.cabinet.Model.General.BaseClass;
import com.truecoders.cabinet.R;
import com.truecoders.cabinet.Utils.RetroClient;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;

import static com.truecoders.cabinet.DAO.Entrant.HostelDAO.addHostelToRealm;

/**
 * @author Maria Fabrichkina
 */

public class HostelsFragment extends Fragment {

    private ListView listView;
    private ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_hostels, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listView = view.findViewById(R.id.list_view_hostels);
        progressBar = view.findViewById(R.id.spin_kit);

        addHostels();
    }

    private void addHostels() {
        if ((Realm.getDefaultInstance().where(Hostel.class).count() == 0)) {
            RetroClient.getApiService().getHostels()
                    .enqueue(new Callback<BaseClass>() {
                        @Override
                        public void onResponse(@NonNull Call<BaseClass> call, @NonNull Response<BaseClass> response) {
                            List<Hostel> HostelList = response.body() != null ? response.body().getHostels() : null;

                            if (HostelList != null) {
                                for (Hostel hostel : HostelList) {
                                    addHostelToRealm(hostel);
                                }
                            }
                            showHostel();
                        }

                        @Override
                        public void onFailure(@NonNull Call<BaseClass> call, @NonNull Throwable t) {

                        }
                    });
        } else {
            showHostel();
        }
    }

    private void showHostel() {
        List<Hostel> hostelList = Realm.getDefaultInstance().where(Hostel.class).findAll();
        if (getContext() != null) {
            HostelListAdapter adapter = new HostelListAdapter(getContext(), hostelList);
            progressBar.setVisibility(View.GONE);
            listView.setAdapter(adapter);
        }
    }

}

package com.truecoders.cabinet.Fragment.General;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.truecoders.cabinet.Adapter.ListViewAdapter.General.NewsListAdapter;
import com.truecoders.cabinet.Model.General.BaseClass;
import com.truecoders.cabinet.Model.General.News;
import com.truecoders.cabinet.Model.Student.Profile;
import com.truecoders.cabinet.R;
import com.truecoders.cabinet.Utils.RetroClient;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;
import java.util.Objects;

/**
 * @author Andrew Telkov
 */

public class NewsFragment extends Fragment implements View.OnClickListener {

    private Button btnUniversity;
    private Button btnInstitute;
    private ListView newsListView;
    private ProgressBar progressBar;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_news, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        newsListView = view.findViewById(R.id.list_view_news);
        progressBar = view.findViewById(R.id.spin_kit);

        btnUniversity = view.findViewById(R.id.btn_university);
        btnUniversity.setOnClickListener(this);
        btnInstitute = view.findViewById(R.id.btn_institute);
        btnInstitute.setOnClickListener(this);
        btnUniversity.setBackgroundResource(R.drawable.background_invisible);
        btnInstitute.setBackgroundResource(R.color.background_buttons_document);

       //btnInstitute.setText(Objects.requireNonNull(Realm.getDefaultInstance().where(Profile.class).findFirst()).getInstitute());

        getNews("СФУ");

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_university:
                btnUniversity.setBackgroundResource(R.drawable.background_invisible);
                btnInstitute.setBackgroundResource(R.color.background_buttons_document);
                getNews("СФУ");
                break;
            case R.id.btn_institute:
                btnInstitute.setBackgroundResource(R.drawable.background_invisible);
                btnUniversity.setBackgroundResource(R.color.background_buttons_document);
                getNews(Objects.requireNonNull(Realm.getDefaultInstance().where(Profile.class).findFirst()).getInstitute());
                break;
        }
    }

    private void getNews(String institute) {
        RetroClient.getApiService().getNews(institute).enqueue(new Callback<BaseClass>() {
            @Override
            public void onResponse(@NonNull Call<BaseClass> call, @NonNull Response<BaseClass> response) {
                if (response.body() != null) {
                    List<News> newsList = response.body().getNews();
                    NewsListAdapter adapter = new NewsListAdapter(getContext(), newsList);
                    newsListView.setAdapter(adapter);
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(@NonNull Call<BaseClass> call, @NonNull Throwable t) {

            }
        });
    }
}
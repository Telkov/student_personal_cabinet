package com.truecoders.cabinet.Activity.General;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.truecoders.cabinet.Model.General.News;
import com.truecoders.cabinet.R;

/**
 * @author Andrew Telkov
 */

public class InfoNewsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_news);

        News news = (News) getIntent().getSerializableExtra("news");

        if (!news.getImageUrl().equals("null")) {
            Picasso.get()
                    .load(news.getImageUrl())
                    .placeholder(R.drawable.ic_sfu_logo_24dp)
                    .fit().centerCrop()
                    .into(((ImageView) findViewById(R.id.image_news)));
        }
        else {
            ((ImageView) findViewById(R.id.image_news)).setImageResource(R.drawable.ic_sfu_logo_24dp);
        }

        ((TextView) findViewById(R.id.text_title)).setText(String.format("%s", news.getTitle()));
        ((TextView) findViewById(R.id.text_description)).setText(String.format("%s", news.getDescription()));

        (findViewById(R.id.read_news)).setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(news.getLink()));
            startActivity(intent);
        });
    }
}

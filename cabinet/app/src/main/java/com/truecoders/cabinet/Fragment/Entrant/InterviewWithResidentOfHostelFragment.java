package com.truecoders.cabinet.Fragment.Entrant;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.truecoders.cabinet.R;

/**
 * Created by Maria Fabrichkina on 3.01.19
 */

public class InterviewWithResidentOfHostelFragment extends Fragment {

    private String interview;
    TextView tvTextInterview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_hostel_interview, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = this.getArguments();
        interview = bundle.getString("interview");

        tvTextInterview = view.findViewById(R.id.tvTextInterview);

        tvTextInterview.setText(interview);

    }
}

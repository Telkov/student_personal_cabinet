package com.truecoders.cabinet.DAO.Entrant;

import com.truecoders.cabinet.Model.Entrant.Institute;
import io.realm.Realm;

/**
 * @author Maria Fabrichkina
 */

public class InstitutesDAO {
    
    public static void addInstituteToRealm(Institute institute) {

        Realm.getDefaultInstance().executeTransaction(realm -> {
            Institute realmObject = realm.createObject(Institute.class);

            realmObject.setTitle(institute.getTitle());
            realmObject.setAbbreviation(institute.getAbbreviation());
            realmObject.setAddress(institute.getAddress());
            realmObject.setOperatingModeWeekday(institute.getOperatingModeWeekday());
            realmObject.setOperatingModeSat(institute.getOperatingModeSat());
            realmObject.setOperatingModeSun(institute.getOperatingModeSun());
            realmObject.setContactName(institute.getContactName());
            realmObject.setPhone(institute.getPhone());
            realmObject.setEmail(institute.getEmail());
            realmObject.setImage(institute.getImage());
        });
    }
}

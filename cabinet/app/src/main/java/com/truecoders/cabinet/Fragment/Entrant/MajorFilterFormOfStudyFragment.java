package com.truecoders.cabinet.Fragment.Entrant;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.truecoders.cabinet.R;

import javax.annotation.Nullable;

/**
 * Created by Maria Fabrichkina on 05.03.19
 */

public class MajorFilterFormOfStudyFragment extends Fragment implements SeekBar.OnSeekBarChangeListener {

    private CheckBox cbInternal, cbCorrespondence, cbInternalCorrespondence, cbBudget, cbPay;
    private SeekBar sbCost;
    private EditText etCost;
    private Button btnFurther;
    private ConstraintLayout clPay;
    private Fragment fragment;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_major_filter_form_of_study, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @android.support.annotation.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        cbInternal = view.findViewById(R.id.cbInternal);
        cbCorrespondence = view.findViewById(R.id.cbCorrespondence);
        cbInternalCorrespondence = view.findViewById(R.id.cbInternal_correspondence);

        cbBudget = view.findViewById(R.id.cbBudget);
        cbPay = view.findViewById(R.id.cbPay);

        sbCost = view.findViewById(R.id.sbCost);
        etCost = view.findViewById(R.id.etCost);
        btnFurther = view.findViewById(R.id.btnFurther);
        clPay = view.findViewById(R.id.clPay);

        etCost.setText(String.valueOf(sbCost.getProgress()));

        int color = ContextCompat.getColor(getContext(), R.color.colorOrange);
        sbCost.getProgressDrawable().setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        sbCost.getThumb().setColorFilter(color, PorterDuff.Mode.SRC_ATOP);

        sbCost.setOnSeekBarChangeListener(this);

        cbPay.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                clPay.setVisibility(View.VISIBLE);
            } else {
                clPay.setVisibility(View.INVISIBLE);
            }
        });

        btnFurther.setOnClickListener(v -> {

            if (cbInternal.isChecked() == false && cbCorrespondence.isChecked() == false && cbInternalCorrespondence.isChecked()==false
                    && cbBudget.isChecked() == false && cbPay.isChecked() == false) {

                Toast toast = Toast.makeText(getActivity(), "Ничего не выбрано!", Toast.LENGTH_SHORT);
                toast.show();

            } else {

                fragment = new MajorFilterSubjectFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean("cbInternal", cbInternal.isChecked());
                bundle.putBoolean("cbCorrespondence", cbCorrespondence.isChecked());
                bundle.putBoolean("cbInternalCorrespondence", cbInternalCorrespondence.isChecked());
                bundle.putBoolean("cbBudget", cbBudget.isChecked());
                bundle.putBoolean("cbPay", cbPay.isChecked());

                if (cbPay.isChecked()) {
                    bundle.putString("etCost", etCost.getText().toString());
                }

                fragment.setArguments(bundle);

                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.content_frame_inst, fragment);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        etCost.setText(String.valueOf(progress));
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        etCost.setText(String.valueOf(seekBar.getProgress()));
    }
}

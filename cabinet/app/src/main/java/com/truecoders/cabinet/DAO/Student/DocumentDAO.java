package com.truecoders.cabinet.DAO.Student;

import com.truecoders.cabinet.Model.Student.Document;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * @author Ilya Baykalov
 */

public class DocumentDAO {
    public static List<Document> getOrderHistory(String response) {
        List<Document> documentList = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONObject(response).getJSONArray("data");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject tempObject = jsonArray.getJSONObject(i);
                Integer idFS = tempObject.getInt("id_fact_sheet");
                Integer registerNumber = tempObject.getInt("register_number");
                Integer idTFS = tempObject.getInt("id_type_fact_sheet");
                Boolean officialSeal = tempObject.getBoolean("official_seal");
                Date createDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(tempObject.getString("date_create"));
                Boolean isDone = tempObject.getBoolean("is_receipt");
                Date doneDate = tempObject.getString("date_receipt").equals("null") ? null :
                        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(tempObject.getString("date_receipt"));
                Integer idFSS = tempObject.getInt("id_fact_sheet_status");

                documentList.add(new Document(idFS, idFSS, registerNumber, idTFS, officialSeal, createDate, isDone, doneDate));
            }
        } catch (JSONException | ParseException e) {
            e.printStackTrace();
        }
        return documentList;
    }

}

package com.truecoders.cabinet.DAO.General;

import com.truecoders.cabinet.Model.General.News;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ilya Baykalov
 */

public class NewsDAO {

//    public static List<News> getNewsData(String response) {
//        List<News> newsList = new ArrayList<>();
//        try {
//            JSONArray jsonArray = new JSONObject(response).getJSONArray("data");
//            for (int i = 0; i < jsonArray.length(); i++) {
//                JSONObject tempObject = jsonArray.getJSONObject(i);
//                Long id = tempObject.getLong("id");
//                String title = tempObject.getString("title");
//                String imageURL = tempObject.getString("imageURL");
//                String category = tempObject.getString("category");
//                String date = tempObject.getString("date");
//                String description = tempObject.getString("description");
//                String institute = tempObject.getString("institute");
//
//                newsList.add(new News(id, title, imageURL, category, date, description, institute));
//            }
//        } catch (Exception ignored) { }
//        return newsList;
//    }
//
//    // TODO: move method to server
//    public static String getNewsJSON(Integer page) {
//        StringBuilder jsonNews = new StringBuilder("{\"data\": [");
//        String s = "Loading error";
//        try {
//            Document document = Jsoup.connect("http://news.sfu-kras.ru/all?page=" + page).get();
//            Elements elements = document.select("article.news-item");
//
//            for (Element element : elements) {
//                StringBuilder json = new StringBuilder();
//
//                json.append("{\"id\":\"");
//                Elements id = element.select("a.news-item-link");
//                json.append(id.attr("href").substring(6)).append("\",");
//
//                json.append("\"title\":\"");
//                Elements title = element.select("h3.news-item-title");
//                String q = title.html();
//                json.append(q).append("\",");
//
//                json.append("\"imageURL\":\"");
//                Elements img = element.select("span.news-item-image img");
//                if (img.size() == 0)
//                    json.append("null\",");
//                else
//                    json.append("http://news.sfu-kras.ru").append(img.attr("src")).append("\",");
//
//                json.append("\"category\":\"");
//                Elements category = element.select("span.news-item-category");
//                json.append(category.html()).append("\",");
//
//                json.append("\"date\":\"");
//                Elements date1 = element.select("span.news-item-date-day");
//                Elements date2 = element.select("span.news-item-date-month");
//                json.append(date1.html()).append(" ").append(date2.html()).append("\",");
//
//                json.append("\"description\":\"");
//                Elements description = element.select("p.last");
//                String d = description.html();
//                json.append(d).append("\",");
//                json.append("\"src\":\"");
//                Elements src = element.select("a.news-item-link");
//                json.append("http://news.sfu-kras.ru").append(src.attr("href")).append("\"},");
//                jsonNews.append(json);
//            }
//            s = jsonNews.substring(0, jsonNews.length() - 1);
//            s += "]}";
//            s = s.replaceAll("<(\"[^\"]*\"|'[^']*'|[^'\">])*>", "").replaceAll("&nbsp;", " ");
//        } catch (IOException ignored) { }
//        return s;
//    }
}
package com.truecoders.cabinet.Utils.Notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

//TODO Debug

public class Alarm extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        intent = new Intent(context, NotificationService.class);
        intent.putExtra("ForeGround", false);
        try {
            context.startService(intent);
        }
        catch (IllegalStateException ex) {
            intent.putExtra("ForeGround", true);
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(intent);
            }
            else {
                context.startService(intent);
            }
        }
        //MyJobService(context);
        //context.startService(new Intent(context, MyService.class));
        Log.d("StudentCabinet", "Alarm just fired");
        Toast.makeText(context, "Time Alarm", Toast.LENGTH_SHORT).show();
    }
}

package com.truecoders.cabinet.Adapter.ListViewAdapter.Entrant;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintSet;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.truecoders.cabinet.Utils.URLSpanNoUnderline;
import com.truecoders.cabinet.Model.Entrant.Contact;
import com.truecoders.cabinet.R;

import java.util.List;

/**
 * @author Maria Fabrichkina
 */

public class ContactListAdapter extends ArrayAdapter<Contact> {

    private LayoutInflater inflater;
    private List<Contact> contacts;

    public ContactListAdapter(Context context, List<Contact> contacts) {
        super(context, R.layout.adapter_contacts, contacts);
        this.contacts = contacts;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.adapter_contacts, parent, false);
        }

        ((TextView) view.findViewById(R.id.tvTitleContacts)).setText(String.format("%s", contacts.get(position).getTitle()));
        ((TextView) view.findViewById(R.id.tvAddressInst)).setText(String.format("%s", contacts.get(position).getAddress()));

        if (contacts.get(position).getPhone().equals("null")) {

            ConstraintSet set = new ConstraintSet();

            set.clear(R.id.textView7, ConstraintSet.TOP);
            set.clear(R.id.tvAddressInst, ConstraintSet.TOP);

            set.connect(R.id.textView7, ConstraintSet.TOP, R.id.tvTitleContacts, ConstraintSet.BOTTOM, 10);
            set.connect(R.id.tvAddressInst, ConstraintSet.TOP, R.id.tvTitleContacts, ConstraintSet.BOTTOM, 10);

            (view.findViewById(R.id.tvPhoneNumber)).setVisibility(View.GONE);
            (view.findViewById(R.id.tvCount)).setVisibility(View.GONE);
        } else {
            (view.findViewById(R.id.tvCount)).setVisibility(View.VISIBLE);
            (view.findViewById(R.id.tvPhoneNumber)).setVisibility(View.VISIBLE);

            ((TextView) view.findViewById(R.id.tvPhoneNumber)).setText(String.format("%s", contacts.get(position).getPhone()));
        }

        if (contacts.get(position).getEmail().equals("null")) {

            ConstraintSet set = new ConstraintSet();

            set.clear(R.id.tvLink, ConstraintSet.TOP);
            set.connect(R.id.tvLink, ConstraintSet.TOP, R.id.tvAddressInst, ConstraintSet.BOTTOM, 6);

            (view.findViewById(R.id.textView9)).setVisibility(View.GONE);
            (view.findViewById(R.id.tvEmail)).setVisibility(View.GONE);
        } else {
            (view.findViewById(R.id.textView9)).setVisibility(View.VISIBLE);
            (view.findViewById(R.id.tvEmail)).setVisibility(View.VISIBLE);

            ((TextView) view.findViewById(R.id.tvEmail)).setText(String.format("%s", contacts.get(position).getEmail()));
        }

        String link = "<a style='text-decoration:underline' href='" + String.format("%s", contacts.get(position).getLink()) + "'>" + String.format("%s", contacts.get(position).getLinkTitle()) + "</a>";
        if (Build.VERSION.SDK_INT >= 24) {
            ((TextView) view.findViewById(R.id.tvLink)).setText(Html.fromHtml(link, Html.FROM_HTML_MODE_LEGACY), TextView.BufferType.SPANNABLE);

        } else {
            ((TextView) view.findViewById(R.id.tvLink)).setText(Html.fromHtml(link), TextView.BufferType.SPANNABLE);
        }

        ((TextView) view.findViewById(R.id.tvLink)).setMovementMethod(LinkMovementMethod.getInstance());

        URLSpanNoUnderline.removeUnderline(view.findViewById(R.id.tvLink));

        return view;
    }
}

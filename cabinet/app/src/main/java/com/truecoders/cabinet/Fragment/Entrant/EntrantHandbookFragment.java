package com.truecoders.cabinet.Fragment.Entrant;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.truecoders.cabinet.Activity.Entrant.EntrantHandbookActivity;
import com.truecoders.cabinet.R;

/**
 * @author Maria Fabrichkina
 */

public class EntrantHandbookFragment extends Fragment implements View.OnClickListener{

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_info_university_entrant, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        (view.findViewById(R.id.btn_directions)).setOnClickListener(this);
        (view.findViewById(R.id.btn_institutes)).setOnClickListener(this);
        (view.findViewById(R.id.btn_dormitories)).setOnClickListener(this);
        (view.findViewById(R.id.btn_contacts)).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getActivity(), EntrantHandbookActivity.class);

        switch (v.getId()) {
            case R.id.btn_directions:
                intent.putExtra("code", R.id.btn_directions);
                startActivity(intent);
                break;
            case R.id.btn_institutes:
                intent.putExtra("code", R.id.btn_institutes);
                startActivity(intent);
                break;
            case R.id.btn_dormitories:
                intent.putExtra("code", R.id.btn_dormitories);
                startActivity(intent);
                break;
            case R.id.btn_contacts:
                intent.putExtra("code", R.id.btn_contacts);
                startActivity(intent);
                break;
        }
    }
}
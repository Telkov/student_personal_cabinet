class Subject {

    constructor(subjectName, mark, markDate, hours, teacher) {
        this.setSubjectName = subjectName;
        this.setMark = mark;
        this.setMarkDate = markDate;
        this.setHours = hours;
        this.setTeacher = teacher;
    }

    set setSubjectName(newValue) {
        this.subjectName = newValue;
    }
    set setMark(newValue) {
        this.mark = newValue;
    }

    get getMark(){
        return this.mark;
    }

    set setMarkDate(newValue) {
        this.markDate = newValue;
    }

    set setHours(newValue) {
        this.hours = newValue;
    }

    set setTeacher(newValue) {
        this.teacher = newValue;
    }

    get getTeacher(){
        return this.teacher;
    }
}

module.exports = Subject;

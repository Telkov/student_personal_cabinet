package com.truecoders.cabinet.Fragment.Entrant;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;
import com.truecoders.cabinet.R;

public class InstituteImageZoomingFragment  extends Fragment {

    private PhotoView photoView;
    private String image;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_institute_image_zooming, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

       photoView= view.findViewById(R.id.pvImage);

        Bundle bundle = this.getArguments();
        image = bundle.getString("image");

        Picasso.get().load(image).into(photoView);
        photoView.setScaleType(ImageView.ScaleType.FIT_CENTER);
    }
}

package com.truecoders.cabinet.Model.Entrant;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import io.realm.RealmObject;

import java.io.Serializable;

/**
 * @author Maria Fabrichkina
 */

public class Institute extends RealmObject implements Serializable {
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("abbreviation")
    @Expose
    private String abbreviation;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("operatingModeWeekday")
    @Expose
    private String operatingModeWeekday;
    @SerializedName("operatingModeSat")
    @Expose
    private String operatingModeSat;
    @SerializedName("operatingModeSun")
    @Expose
    private String operatingModeSun;
    @SerializedName("contactName")
    @Expose
    private String contactName;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("image")
    @Expose
    private String image;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOperatingModeWeekday() {
        return operatingModeWeekday;
    }

    public void setOperatingModeWeekday(String operatingModeWeekday) {
        this.operatingModeWeekday = operatingModeWeekday;
    }

    public String getOperatingModeSat() {
        return operatingModeSat;
    }

    public void setOperatingModeSat(String operatingModeSat) {
        this.operatingModeSat = operatingModeSat;
    }

    public String getOperatingModeSun() {
        return operatingModeSun;
    }

    public void setOperatingModeSun(String operatingModeSun) {
        this.operatingModeSun = operatingModeSun;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }
}
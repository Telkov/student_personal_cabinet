let Webdriver = require('selenium-webdriver');
let chromeDriver = require('selenium-webdriver/chrome');
const options = new chromeDriver.Options();
options.addArguments(
    'headless',
    'disable-gpu',
    'no-sandbox'
);
let RecordBook = require('../parser/model/recordBook');
let Semester = require('../parser/model/semester');
let Subject = require('../parser/model/subject');
let Progress = require('../parser/model/progress');
let ECourseTask = require('./model/eCourseTask');

// Запросы
module.exports = {
    getRecordBook: getRecordBookBeta,
    getProgress: getProgressBeta
};

// Получение зачетной книжки
async function getRecordBook(req, res) {
    let username = req.body.username;
    let password = req.body.password;

    console.log("open Google Chrome");

    let driver = new Webdriver.Builder()
        .forBrowser('chrome')
        .setChromeOptions(options)
        .build();

    console.log("open recordbook");
    await driver.get('https://i.sfu-kras.ru/auth/index.php?backurl=%2Fesfu%2F%3Faction%3Dsgradebook');

    console.log("login like " + username);
    let loginInput = driver.findElement(Webdriver.By.name("USER_LOGIN"));
    let passInput = driver.findElement(Webdriver.By.name("USER_PASSWORD"));

    console.log("input login");
    await loginInput.sendKeys(username);
    console.log("input password");
    await passInput.sendKeys(password);

    console.log("click button");
    console.log("LOADING...");
    await driver.findElement(Webdriver.By.className("login-btn")).click();

    let recordBook = new RecordBook();

    let semesterCount = 0;
    let currSemester = 1;

    for (let i = 1; true; i += 2) {
        try {
            await driver.findElement(Webdriver.By.css("#workarea-content > div > div.user-sgradebook > div:nth-child(" + i + ")")).getText();
            semesterCount++;
        } catch (e) {
            break;
        }
    }
    semesterCount = semesterCount * 2 - 1;

    for (let i = semesterCount; i > 0; i -= 2) {
        let semester = new Semester();
        let semesterTitle = (await driver.findElement(Webdriver.By.css("#workarea-content > div > div.user-sgradebook > div:nth-child(" + i + ")")).getText()).split(', ');

        semester.semesterName = semesterTitle[0];
        semester.period = semesterTitle[1];

        for (let j = 2; true; j++) {
            let subject = new Subject();
            try {
                let element = await driver.findElement(Webdriver.By.css("#workarea-content > div > div.user-sgradebook > table:nth-child(" + (i + 1) + ") > tbody > tr:nth-child(" + j + ")")).getText();
                if (element !== "Экзамен" && element !== "Практика" && element !== "Зачет" && element !== "Курсовой проект" && element !== "") {
                    subject.setSubjectName = await driver.findElement(Webdriver.By.css("#workarea-content > div > div.user-sgradebook > table:nth-child(" + (i + 1) + ") > tbody > tr:nth-child(" + j + ") > td:nth-child(1)")).getText();
                    subject.setMark = (await driver.findElement(Webdriver.By.css("#workarea-content > div > div.user-sgradebook > table:nth-child(" + (i + 1) + ") > tbody > tr:nth-child(" + j + ") > td:nth-child(2)")).getText()).replace(/(О)\S*|(Х)\S*|(У)\S*|(Неу)\S*/g, "$1$2$3$4");
                    subject.setMarkDate = await driver.findElement(Webdriver.By.css("#workarea-content > div > div.user-sgradebook > table:nth-child(" + (i + 1) + ") > tbody > tr:nth-child(" + j + ") > td:nth-child(3)")).getText();

                    let hours = (await driver.findElement(Webdriver.By.css("#workarea-content > div > div.user-sgradebook > table:nth-child(" + (i + 1) + ") > tbody > tr:nth-child(" + j + ") > td:nth-child(4)")).getText()).replace(/\s/g, '');
                    subject.setHours = hours !== "" ? hours : null;

                    let teachers = (await driver.findElement(Webdriver.By.css("#workarea-content > div > div.user-sgradebook > table:nth-child(" + (i + 1) + ") > tbody > tr:nth-child(" + j + ") > td:nth-child(5)")).getText()).replace(/([А-я]+)\s*([А-я]+)\s*([А-Я][а-я])/, "$1 $2 $3").split(', ');
                    subject.setTeacher = teachers[0] !== "" ? teachers : null;

                    switch (subject.getMark) {
                        case "О":
                            subject.setMark = "Отлично";
                            break;
                        case "Х":
                            subject.setMark = "Хорошо";
                            break;
                        case "У":
                            subject.setMark = "Удовлетворительно";
                            break;
                        case "Н":
                            subject.setMark = "Неудовлетворительно";
                            break;
                    }
                    semester.getSubjectList.push(subject);
                }
            } catch (e) {
                currSemester++;
                break;
            }
        }
        recordBook.getSemesterList.push(semester);
    }

    // ..................................................................................................................
    // номер семестра -> #workarea-content > div > div.user-sgradebook > div:nth-child(1)
    // шапка семестра -> #workarea-content > div > div.user-sgradebook > table:nth-child(2) > tbody > tr:nth-child(1)
    // зачет ->          #workarea-content > div > div.user-sgradebook > table:nth-child(2) > tbody > tr:nth-child(2)
    // предмет ->        #workarea-content > div > div.user-sgradebook > table:nth-child(2) > tbody > tr:nth-child(3)
    // разделитель ->    #workarea-content > div > div.user-sgradebook > table:nth-child(2) > tbody > tr:nth-child(4)
    // практика ->       #workarea-content > div > div.user-sgradebook > table:nth-child(2) > tbody > tr:nth-child(5)
    // предмет ->        #workarea-content > div > div.user-sgradebook > table:nth-child(2) > tbody > tr:nth-child(6)
    // разделитель ->    #workarea-content > div > div.user-sgradebook > table:nth-child(2) > tbody > tr:nth-child(7)
    // экзамен ->        #workarea-content > div > div.user-sgradebook > table:nth-child(2) > tbody > tr:nth-child(8)
    // предмет ->        #workarea-content > div > div.user-sgradebook > table:nth-child(2) > tbody > tr:nth-child(9)
    // предмет ->        #workarea-content > div > div.user-sgradebook > table:nth-child(2) > tbody > tr:nth-child(10)
    // предмет ->        #workarea-content > div > div.user-sgradebook > table:nth-child(2) > tbody > tr:nth-child(11)
    // разделитель ->    #workarea-content > div > div.user-sgradebook > table:nth-child(2) > tbody > tr:nth-child(12)
    //
    // номер семестра -> #workarea-content > div > div.user-sgradebook > div:nth-child(3)
    // шапка семестра -> #workarea-content > div > div.user-sgradebook > table:nth-child(4) > tbody > tr:nth-child(1)
    // зачет ->          #workarea-content > div > div.user-sgradebook > table:nth-child(4) > tbody > tr:nth-child(2)
    // предмет ->        #workarea-content > div > div.user-sgradebook > table:nth-child(4) > tbody > tr:nth-child(3)
    // предмет ->        #workarea-content > div > div.user-sgradebook > table:nth-child(4) > tbody > tr:nth-child(4)
    // предмет ->        #workarea-content > div > div.user-sgradebook > table:nth-child(4) > tbody > tr:nth-child(5)
    // предмет ->        #workarea-content > div > div.user-sgradebook > table:nth-child(4) > tbody > tr:nth-child(6)
    // предмет ->        #workarea-content > div > div.user-sgradebook > table:nth-child(4) > tbody > tr:nth-child(7)
    // разделитель ->    #workarea-content > div > div.user-sgradebook > table:nth-child(4) > tbody > tr:nth-child(8)
    // курсово проект -> #workarea-content > div > div.user-sgradebook > table:nth-child(4) > tbody > tr:nth-child(9)
    // предмет ->        #workarea-content > div > div.user-sgradebook > table:nth-child(4) > tbody > tr:nth-child(10)
    // разделитель ->    #workarea-content > div > div.user-sgradebook > table:nth-child(4) > tbody > tr:nth-child(11)
    // экзамен ->        #workarea-content > div > div.user-sgradebook > table:nth-child(4) > tbody > tr:nth-child(12)
    // предмет ->        #workarea-content > div > div.user-sgradebook > table:nth-child(4) > tbody > tr:nth-child(13)
    // предмет ->        #workarea-content > div > div.user-sgradebook > table:nth-child(4) > tbody > tr:nth-child(14)
    // предмет ->        #workarea-content > div > div.user-sgradebook > table:nth-child(4) > tbody > tr:nth-child(15)
    // предмет ->        #workarea-content > div > div.user-sgradebook > table:nth-child(4) > tbody > tr:nth-child(16)
    // разделитель ->    #workarea-content > div > div.user-sgradebook > table:nth-child(4) > tbody > tr:nth-child(17)
    //
    // номер семестра -> #workarea-content > div > div.user-sgradebook > div:nth-child(5)
    // шапка семестра -> #workarea-content > div > div.user-sgradebook > table:nth-child(6) > tbody > tr:nth-child(1)
    // ..................................................................................................................

    res.status(200)
        .json({
            status: "success",
            recordBook
        });

    await driver.quit();
}

function getRecordBookBeta(req, res) {
    res.status(200)
        .json(
            {
                "status": "success",
                "recordBook": {
                    "semesterList": [
                        {
                            "subjectList": [
                                {
                                    "subjectName": "Прикладная физическая культура и спорт",
                                    "mark": "Зачет",
                                    "markDate": "24.12.15",
                                    "hours": "50/1.39",
                                    "teacher": [
                                        "Болотская О.Ю.",
                                        "Юдаков Владимир Николаевич"
                                    ]
                                },
                                {
                                    "subjectName": "Основы программирования",
                                    "mark": "Зачет",
                                    "markDate": "29.12.15",
                                    "hours": "180/5",
                                    "teacher": [
                                        "Прокопенко Александр Владимирович"
                                    ]
                                },
                                {
                                    "subjectName": "Иностранный язык",
                                    "mark": "Зачет",
                                    "markDate": "25.12.15",
                                    "hours": "72/2",
                                    "teacher": [
                                        "Лабушева Татьяна Михайловна",
                                        "Юрьева Елена Владимировна"
                                    ]
                                },
                                {
                                    "subjectName": "Введение в инженерную деятельность",
                                    "mark": "Зачет",
                                    "markDate": "30.12.15",
                                    "hours": "108/3",
                                    "teacher": [
                                        "Кулягин Виталий Александрович"
                                    ]
                                },
                                {
                                    "subjectName": "Математический анализ",
                                    "mark": "Зачет",
                                    "markDate": "29.12.15",
                                    "hours": "180/5",
                                    "teacher": [
                                        "Белько Елена Станиславовна"
                                    ]
                                },
                                {
                                    "subjectName": "Экология",
                                    "mark": "Зачет",
                                    "markDate": "30.12.15",
                                    "hours": "108/3",
                                    "teacher": [
                                        "Стравинскене Екатерина Сергеевна"
                                    ]
                                },
                                {
                                    "subjectName": "Физическая культура и спорт",
                                    "mark": "Зачет",
                                    "markDate": "31.12.15",
                                    "hours": "18/0.5",
                                    "teacher": [
                                        "Пазенко Вячеслав Иванович"
                                    ]
                                },
                                {
                                    "subjectName": "История",
                                    "mark": "Хорошо",
                                    "markDate": "23.01.16",
                                    "hours": "144/4",
                                    "teacher": [
                                        "Ермакова Елена Евгеньевна"
                                    ]
                                },
                                {
                                    "subjectName": "Алгебра и геометрия",
                                    "mark": "Хорошо",
                                    "markDate": "14.01.16",
                                    "hours": "180/5",
                                    "teacher": [
                                        "Сафонов Константин Владимирович"
                                    ]
                                }
                            ],
                            "semesterName": "Семестр 1",
                            "period": "осень 2015-2016 уч года"
                        },
                        {
                            "subjectList": [
                                {
                                    "subjectName": "Теория и практика эффективного речевого общения",
                                    "mark": "Зачет",
                                    "markDate": "07.06.16",
                                    "hours": "108/3",
                                    "teacher": [
                                        "Колобаев Павел Алексеевич"
                                    ]
                                },
                                {
                                    "subjectName": "Прикладная физическая культура и спорт",
                                    "mark": "Зачет",
                                    "markDate": "09.06.16",
                                    "hours": "50/1.39",
                                    "teacher": [
                                        "Болотская О.Ю.",
                                        "Юдаков Владимир Николаевич"
                                    ]
                                },
                                {
                                    "subjectName": "Физика",
                                    "mark": "Зачет",
                                    "markDate": "08.06.16",
                                    "hours": "144/4",
                                    "teacher": [
                                        "Ким Татьяна Алексеевна"
                                    ]
                                },
                                {
                                    "subjectName": "Физическая культура и спорт",
                                    "mark": "Зачет",
                                    "markDate": "06.06.16",
                                    "hours": "18/0.5",
                                    "teacher": [
                                        "Николаев Евгений Анатольевич"
                                    ]
                                },
                                {
                                    "subjectName": "Информатика",
                                    "mark": "Зачет",
                                    "markDate": "11.06.16",
                                    "hours": "108/3",
                                    "teacher": [
                                        "Тушко Тамара Алексеевна"
                                    ]
                                },
                                {
                                    "subjectName": "Иностранный язык",
                                    "mark": "Зачет",
                                    "markDate": "09.06.16",
                                    "hours": "72/2",
                                    "teacher": [
                                        "Даниленко А.С.",
                                        "Лабушева Татьяна Михайловна"
                                    ]
                                },
                                {
                                    "subjectName": "Введение в инженерную деятельность",
                                    "mark": "Отлично",
                                    "markDate": "16.06.16",
                                    "hours": "108/3",
                                    "teacher": [
                                        "Евдокимов И.В."
                                    ]
                                },
                                {
                                    "subjectName": "Дискретная математика",
                                    "mark": "Хорошо",
                                    "markDate": "21.06.16",
                                    "hours": "180/5",
                                    "teacher": [
                                        "Гульнова Белла Владимировна"
                                    ]
                                },
                                {
                                    "subjectName": "Математический анализ",
                                    "mark": "Хорошо",
                                    "markDate": "29.09.16",
                                    "hours": "180/5",
                                    "teacher": [
                                        "Белько Елена Станиславовна"
                                    ]
                                },
                                {
                                    "subjectName": "Основы программирования",
                                    "mark": "Отлично",
                                    "markDate": "25.06.16",
                                    "hours": "144/4",
                                    "teacher": [
                                        "Прокопенко Александр Владимирович"
                                    ]
                                }
                            ],
                            "semesterName": "Семестр 2",
                            "period": "весна 2015-2016 уч года"
                        },
                        {
                            "subjectList": [
                                {
                                    "subjectName": "Прикладная физическая культура и спорт",
                                    "mark": "Зачет",
                                    "markDate": "28.12.16",
                                    "hours": null,
                                    "teacher": [
                                        "Вовденко И.В.",
                                        "Воронцов Сергей Васильевич"
                                    ]
                                },
                                {
                                    "subjectName": "Экономика программной инженерии",
                                    "mark": "Зачет",
                                    "markDate": "29.12.16",
                                    "hours": "72/2",
                                    "teacher": [
                                        "Евдокимов И.В."
                                    ]
                                },
                                {
                                    "subjectName": "Иностранный язык",
                                    "mark": "Зачет",
                                    "markDate": "26.12.16",
                                    "hours": "72/2",
                                    "teacher": [
                                        "Лабушева Т.М.",
                                        "Юрьева Елена Владимировна"
                                    ]
                                },
                                {
                                    "subjectName": "Безопасность жизнедеятельности",
                                    "mark": "Зачет",
                                    "markDate": "30.12.16",
                                    "hours": "108/3",
                                    "teacher": [
                                        "Лапкаев Алексей Григорьевич"
                                    ]
                                },
                                {
                                    "subjectName": "Математическая логика и теория алгоритмов",
                                    "mark": "Зачет",
                                    "markDate": "30.12.16",
                                    "hours": "108/3",
                                    "teacher": [
                                        "Вайнштейн Юлия Владимировна"
                                    ]
                                },
                                {
                                    "subjectName": "Командный курсовой проект",
                                    "mark": "Зачет",
                                    "markDate": "27.12.16",
                                    "hours": "72/2",
                                    "teacher": [
                                        "Евдокимов И.В."
                                    ]
                                },
                                {
                                    "subjectName": "Парадигмы программирования",
                                    "mark": "Зачет",
                                    "markDate": "24.12.16",
                                    "hours": "108/3",
                                    "teacher": [
                                        "Никитина Мария Ивановна"
                                    ]
                                },
                                {
                                    "subjectName": "Практика по получению первичных профессиональных умений и навыков, в том числе первичных умений и навыков научно-исследовательской деятельности",
                                    "mark": "Отлично",
                                    "markDate": "01.10.16",
                                    "hours": "108/3",
                                    "teacher": null
                                },
                                {
                                    "subjectName": "Объектно-ориентированное программирование",
                                    "mark": "Отлично",
                                    "markDate": "12.01.17",
                                    "hours": "180/5",
                                    "teacher": [
                                        "Якунин Юрий Юрьевич"
                                    ]
                                },
                                {
                                    "subjectName": "Физика",
                                    "mark": "Отлично",
                                    "markDate": "21.01.17",
                                    "hours": "144/4",
                                    "teacher": [
                                        "Ли Оксана Анатольевна"
                                    ]
                                },
                                {
                                    "subjectName": "Системное программирование",
                                    "mark": "Отлично",
                                    "markDate": "17.01.17",
                                    "hours": "144/4",
                                    "teacher": [
                                        "Кузнецов Александр Сергеевич"
                                    ]
                                }
                            ],
                            "semesterName": "Семестр 3",
                            "period": "осень 2016-2017 уч года"
                        },
                        {
                            "subjectList": [
                                {
                                    "subjectName": "Прикладная физическая культура и спорт",
                                    "mark": "Зачет",
                                    "markDate": "01.06.17",
                                    "hours": null,
                                    "teacher": [
                                        "Вовденко И.В."
                                    ]
                                },
                                {
                                    "subjectName": "Управление программными проектами",
                                    "mark": "Зачет",
                                    "markDate": "03.06.17",
                                    "hours": "108/3",
                                    "teacher": [
                                        "Евдокимов И.В."
                                    ]
                                },
                                {
                                    "subjectName": "Компьютерная безопасность",
                                    "mark": "Зачет",
                                    "markDate": "02.06.17",
                                    "hours": "108/3",
                                    "teacher": [
                                        "Шниперов Алексей Николаевич"
                                    ]
                                },
                                {
                                    "subjectName": "Командный курсовой проект",
                                    "mark": "Зачет",
                                    "markDate": "03.06.17",
                                    "hours": "108/3",
                                    "teacher": [
                                        "Евдокимов И.В."
                                    ]
                                },
                                {
                                    "subjectName": "Алгоритмы и структуры данных",
                                    "mark": "Зачет",
                                    "markDate": "30.05.17",
                                    "hours": "108/3",
                                    "teacher": [
                                        "Царев Роман Юрьевич"
                                    ]
                                },
                                {
                                    "subjectName": "Командный курсовой проект",
                                    "mark": "Отлично",
                                    "markDate": "03.06.17",
                                    "hours": null,
                                    "teacher": [
                                        "Кузнецов Александр Сергеевич"
                                    ]
                                },
                                {
                                    "subjectName": "Иностранный язык",
                                    "mark": "Отлично",
                                    "markDate": "13.06.17",
                                    "hours": "108/3",
                                    "teacher": [
                                        "Лабушева Т.М.",
                                        "Юрьева Елена Владимировна"
                                    ]
                                },
                                {
                                    "subjectName": "Философия",
                                    "mark": "Отлично",
                                    "markDate": "21.06.17",
                                    "hours": "144/4",
                                    "teacher": [
                                        "Бухтояров М.С."
                                    ]
                                },
                                {
                                    "subjectName": "Разработка web-приложений",
                                    "mark": "Отлично",
                                    "markDate": "08.06.17",
                                    "hours": "180/5",
                                    "teacher": [
                                        "Якунин Юрий Юрьевич"
                                    ]
                                },
                                {
                                    "subjectName": "Теория баз данных",
                                    "mark": "Отлично",
                                    "markDate": "17.06.17",
                                    "hours": "180/5",
                                    "teacher": [
                                        "Дамов М.В."
                                    ]
                                }
                            ],
                            "semesterName": "Семестр 4",
                            "period": "весна 2016-2017 уч года"
                        },
                        {
                            "subjectList": [
                                {
                                    "subjectName": "Профессионально-ориентированный иностранный язык",
                                    "mark": "Зачет",
                                    "markDate": "28.12.17",
                                    "hours": "72/2",
                                    "teacher": [
                                        "Лабушева Т.М.",
                                        "Личаргин Д.В."
                                    ]
                                },
                                {
                                    "subjectName": "Прикладная физическая культура и спорт",
                                    "mark": "Зачет",
                                    "markDate": "29.12.17",
                                    "hours": "46/1.28",
                                    "teacher": [
                                        "Вапаева А.В."
                                    ]
                                },
                                {
                                    "subjectName": "Физическая культура и спорт",
                                    "mark": "Зачет",
                                    "markDate": "27.12.17",
                                    "hours": "18/0.5",
                                    "teacher": [
                                        "Николаев Е.А."
                                    ]
                                },
                                {
                                    "subjectName": "Теория языков программирования",
                                    "mark": "Зачет",
                                    "markDate": "30.12.17",
                                    "hours": "108/3",
                                    "teacher": [
                                        "Кузнецов А.С."
                                    ]
                                },
                                {
                                    "subjectName": "Модели стохастических объектов",
                                    "mark": "Зачет",
                                    "markDate": "12.02.18",
                                    "hours": "144/4",
                                    "teacher": [
                                        "Рубан А.И."
                                    ]
                                },
                                {
                                    "subjectName": "Командный курсовой проект \"Корпоративные приложения\"",
                                    "mark": "Зачет",
                                    "markDate": "26.12.17",
                                    "hours": "144/4",
                                    "teacher": [
                                        "Хныкин А.В."
                                    ]
                                },
                                {
                                    "subjectName": "Научно-исследовательская работа",
                                    "mark": "Отлично",
                                    "markDate": "15.09.17",
                                    "hours": "108/3",
                                    "teacher": null
                                },
                                {
                                    "subjectName": "Язык программирования C#",
                                    "mark": "Отлично",
                                    "markDate": "12.01.18",
                                    "hours": "180/5",
                                    "teacher": [
                                        "Чикизов А.А."
                                    ]
                                },
                                {
                                    "subjectName": "Проектирование и архитектура информационных систем",
                                    "mark": "Отлично",
                                    "markDate": "20.01.18",
                                    "hours": "144/4",
                                    "teacher": [
                                        "Кукарцев В.В."
                                    ]
                                },
                                {
                                    "subjectName": "Программирование на COS в Intersystems Cache",
                                    "mark": "Отлично",
                                    "markDate": "16.01.18",
                                    "hours": "144/4",
                                    "teacher": [
                                        "Погребников А.К."
                                    ]
                                }
                            ],
                            "semesterName": "Семестр 5",
                            "period": "осень 2017-2018 уч года"
                        },
                        {
                            "subjectList": [
                                {
                                    "subjectName": "Профессионально-ориентированный иностранный язык",
                                    "mark": "Зачет",
                                    "markDate": "22.05.18",
                                    "hours": "72/2",
                                    "teacher": [
                                        "Ладе А.В.",
                                        "Ямских Т.Н."
                                    ]
                                },
                                {
                                    "subjectName": "Прикладная физическая культура и спорт",
                                    "mark": "Зачет",
                                    "markDate": "25.05.18",
                                    "hours": "46/1.28",
                                    "teacher": [
                                        "Вапаева А.В."
                                    ]
                                },
                                {
                                    "subjectName": "Программирование на Java для Android",
                                    "mark": "Зачет",
                                    "markDate": "24.05.18",
                                    "hours": "108/3",
                                    "teacher": [
                                        "Личаргин Д.В."
                                    ]
                                },
                                {
                                    "subjectName": "Физическая культура и спорт",
                                    "mark": "Зачет",
                                    "markDate": "23.05.18",
                                    "hours": "18/0.5",
                                    "teacher": [
                                        "Николаев Е.А."
                                    ]
                                },
                                {
                                    "subjectName": "Командный курсовой проект \"Корпоративные приложения\"",
                                    "mark": "Зачет",
                                    "markDate": "24.05.18",
                                    "hours": "180/5",
                                    "teacher": [
                                        "Хныкин А.В."
                                    ]
                                },
                                {
                                    "subjectName": "Командный курсовой проект \"Корпоративные приложения\"",
                                    "mark": "Отлично",
                                    "markDate": "24.05.18",
                                    "hours": null,
                                    "teacher": null
                                },
                                {
                                    "subjectName": "Модели стохастических объектов",
                                    "mark": "Хорошо",
                                    "markDate": "14.09.18",
                                    "hours": "180/5",
                                    "teacher": [
                                        "Рубан А.И."
                                    ]
                                },
                                {
                                    "subjectName": "Методы оптимизации",
                                    "mark": "Отлично",
                                    "markDate": "02.06.18",
                                    "hours": "144/4",
                                    "teacher": [
                                        "Тынченко В.В."
                                    ]
                                },
                                {
                                    "subjectName": "Основы сетевых технологий и протоколов",
                                    "mark": "Отлично",
                                    "markDate": "11.06.18",
                                    "hours": "144/4",
                                    "teacher": [
                                        "Шмелев О.В."
                                    ]
                                },
                                {
                                    "subjectName": "Разработка интернет приложений на CSP и ZEN",
                                    "mark": "Отлично",
                                    "markDate": "07.06.18",
                                    "hours": "144/4",
                                    "teacher": [
                                        "Погребников А.К."
                                    ]
                                }
                            ],
                            "semesterName": "Семестр 6",
                            "period": "весна 2017-2018 уч года"
                        },
                        {
                            "subjectList": [
                                {
                                    "subjectName": "Командный курсовой проект \"Корпоративные приложения\"",
                                    "mark": "Зачет",
                                    "markDate": "09.01.19",
                                    "hours": "504/14",
                                    "teacher": [
                                        "Хныкин А.В."
                                    ]
                                },
                                {
                                    "subjectName": "Практика по получению профессиональных умений и опыта профессиональной деятельности",
                                    "mark": "Отлично",
                                    "markDate": "15.09.18",
                                    "hours": "216/6",
                                    "teacher": [
                                        "Евдокимов Иван Валерьевич"
                                    ]
                                },
                                {
                                    "subjectName": "Методы тестирования программных систем",
                                    "mark": "Отлично",
                                    "markDate": "16.01.19",
                                    "hours": "180/5",
                                    "teacher": [
                                        "Кукарцев В.В."
                                    ]
                                },
                                {
                                    "subjectName": "Интеграционная платформа Ensemble",
                                    "mark": "Отлично",
                                    "markDate": "21.01.19",
                                    "hours": "252/7",
                                    "teacher": [
                                        "Якунин Ю.Ю."
                                    ]
                                },
                                {
                                    "subjectName": "Профессионально-ориентированный иностранный язык",
                                    "mark": "Хорошо",
                                    "markDate": "25.01.19",
                                    "hours": "108/3",
                                    "teacher": [
                                        "Гордеева А.Т.",
                                        "Слепченко Н.Н."
                                    ]
                                }
                            ],
                            "semesterName": "Семестр 7",
                            "period": "осень 2018-2019 уч года"
                        }
                    ]
                }
            }
        )
}

async function getProgress(req, res) {
    let username = req.body.username;
    let password = req.body.password;

    let driver = new Webdriver.Builder()
        .forBrowser('chrome')
        .setChromeOptions(options)
        .build();

    await driver.get('https://i.sfu-kras.ru/auth/index.php?backurl=%2Fesfu%2F%3Faction%3Degradebook');

    let loginInput = driver.findElement(Webdriver.By.name("USER_LOGIN"));
    let passInput = driver.findElement(Webdriver.By.name("USER_PASSWORD"));

    await loginInput.sendKeys(username);
    await passInput.sendKeys(password);

    console.log("LOADING...");
    await driver.findElement(Webdriver.By.className("login-btn")).click();

    console.log(await driver.findElement(Webdriver.By.css("#pagetitle")).getText().catch(reason => console.log(reason)));

    let eCoursesList = [];
    let count = (await driver.findElements(Webdriver.By.css(".course_journal_title.down_arrow"))).length * 3 - 2;

    for (let i = 1; i <= count; i += 3) {
        console.log("nth-child(" + i + ")");
        await driver.findElement(Webdriver.By.css(".course_journal_title.down_arrow:nth-child(" + i + ")")).click();
        eCoursesList = await getECourse(driver, eCoursesList, true);
        await driver.findElement(Webdriver.By.css(".course_journal_title.up_arrow:nth-child(" + i + ")")).click();
    }
    res.status(200)
        .json({
            status: "success",
            eCoursesList
        });

    await driver.quit();
}

function getProgressBeta(req, res){
    res.status(200)
        .json(
            {
                "status": "success",
                "eCoursesList": [
                    {
                        "eCourses": [
                            {
                                "name": "Задание 1 - Поиск образа в строке",
                                "mark": "зачтено",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Задание 2 - Алгоритмы поиска значений",
                                "mark": "зачтено",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Задание 3 - Алгоритмы сортировки массивов",
                                "mark": "зачтено",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Задание 4 - Усовершенствованные алгоритмы сортировки массивов",
                                "mark": "зачтено",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Задание 5 - Алгоритмы обработки последовательностей",
                                "mark": "зачтено",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Задание 6 - Обход графов",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Задание 7 - Поиск кратчайшего пути в графе",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Задание 8 - Построение остовного дерева минимальной стоимости",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Входное тестирование",
                                "mark": "84",
                                "range": "0-100"
                            },
                            {
                                "name": "Тест 1",
                                "mark": "17",
                                "range": "0-20"
                            },
                            {
                                "name": "Тест 2",
                                "mark": "20",
                                "range": "0-20"
                            },
                            {
                                "name": "Тест 3",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Тест 4",
                                "mark": "9",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 5",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Тест 6",
                                "mark": "14",
                                "range": "0-20"
                            },
                            {
                                "name": "Тест 7",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Тест 8",
                                "mark": "19",
                                "range": "0-20"
                            },
                            {
                                "name": "Тест 9",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Тест 10",
                                "mark": "19",
                                "range": "0-20"
                            },
                            {
                                "name": "Тест 11",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Тест 12",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Тест 13",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Тест 14",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Тест 15",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Тест 16",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Тест 17",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Итоговый тест",
                                "mark": "27",
                                "range": "0-30"
                            },
                            {
                                "name": "Зачет",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "БУИ 1 | Общие сведения об алгоритмах",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "БУИ 1- | Общие сведения об алгоритмах",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "БУИ 2 | Поиск образа в строке",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "БУИ 2- | Поиск образа в строке",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "БУИ 3 | Алгоритмы обработки массивов",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "БУИ 3- | Алгоритмы обработки массивов",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "БУИ 4 | Алгоритмы сортировки массивов",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "БУИ 4- | Алгоритмы сортировки массивов",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "БУИ 5 | Усовершенствованные алгоритмы сортировки массивов",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "БУИ 5- | Усовершенствованные алгоритмы сортировки массивов",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "БУИ 6- | Алгоритмы обработки последовательностей",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "БУИ 7- | Обход графов",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "БУИ 8- | Поиск кратчайшего пути в графе",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "БУИ 9- | Остовное дерево графа минимальной стоимости",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "БУИ 6 | Алгоритмы обработки последовательностей",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "БУИ 7 | Обход графов",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "БУИ 8 | Поиск кратчайшего пути в графе",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "БУИ 9 | Остовное дерево графа минимальной стоимости",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "БУИ 7+ | Обход графов",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "БУИ 8+ | Поиск кратчайшего пути в графе",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "БУИ 9+ | Остовное дерево графа минимальной стоимости",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "87",
                                "range": "0-100"
                            }
                        ],
                        "eCourseName": "Курс: Алгоритмы и структуры данных (адаптивный)"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Занятие 1",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Занятие 2",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Занятие 3",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Занятие 4",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Занятие 5",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Занятие 6",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Занятие 7",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Занятие 8",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Занятие 9",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Занятие 10",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Занятие 11",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Занятие 12",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Занятие 13",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Занятие 14",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Занятие 15",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Занятие 16",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Занятие 17",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Занятие 18",
                                "mark": "-",
                                "range": "н-п"
                            }
                        ],
                        "eCourseName": "Курс: ИНФОРМАТИКА (Адаптационные курсы.ЭЖ)"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Занятие 1",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Занятие 2",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Занятие 3",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Занятие 4",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Занятие 5",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Занятие 6",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Занятие 7",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Занятие 8",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Занятие 9",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Занятие 10",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Занятие 11",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Занятие 12",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Занятие 13",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Занятие 14",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Занятие 15",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Занятие 16",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Занятие 17",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Занятие 18",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            }
                        ],
                        "eCourseName": "Курс: ФИЗИКА (Адаптационные курсы.ЭЖ)"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Проанализируйте выполненные работы по проекту",
                                "mark": "70",
                                "range": "0-100"
                            },
                            {
                                "name": "Спланируйте работы до конца семестра",
                                "mark": "40",
                                "range": "0-100"
                            },
                            {
                                "name": "Подготовить пакет документов для государственной регистрации разработанного программного продукта",
                                "mark": "80",
                                "range": "0-100"
                            },
                            {
                                "name": "Обновить проектные документы",
                                "mark": "95",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить план деятельности",
                                "mark": "90",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить отчёт за неделю",
                                "mark": "95",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить отчёт за неделю",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить отчёт за неделю",
                                "mark": "80",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить подробный план деятельности",
                                "mark": "80",
                                "range": "0-100"
                            },
                            {
                                "name": "Разработать стратегии работы с заинтересованными сторонами",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Разработать план управления коммуникациями",
                                "mark": "95",
                                "range": "0-100"
                            },
                            {
                                "name": "Сформировать новые проектные команды",
                                "mark": "90",
                                "range": "0-100"
                            },
                            {
                                "name": "Собрать требования заказчика",
                                "mark": "90",
                                "range": "0-100"
                            },
                            {
                                "name": "Разработать Техническое задание и согласовать его с Заказчиком",
                                "mark": "95",
                                "range": "0-100"
                            },
                            {
                                "name": "Подготовить Устав проекта",
                                "mark": "90",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить план деятельности",
                                "mark": "95",
                                "range": "0-100"
                            },
                            {
                                "name": "Выполнить практическую работу №1",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Определить перечень задач и вехи проекта",
                                "mark": "80",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить отчет за неделю",
                                "mark": "95",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить план деятельности",
                                "mark": "80",
                                "range": "0-100"
                            },
                            {
                                "name": "Выполнить практическую работу №2",
                                "mark": "95",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить отчет за неделю",
                                "mark": "80",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить отчет за неделю",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить план деятельности",
                                "mark": "90",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить план деятельности",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Разработать план управления рисками",
                                "mark": "95",
                                "range": "0-100"
                            },
                            {
                                "name": "Внести изменения в проект",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Выполнить практическую работу №3",
                                "mark": "80",
                                "range": "0-100"
                            },
                            {
                                "name": "Выполнить практическую работу №4",
                                "mark": "80",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить отчет за неделю",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить план деятельности",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Ответить на контрольные вопросы по теме",
                                "mark": "90",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить отчет за неделю",
                                "mark": "90",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить план деятельности",
                                "mark": "90",
                                "range": "0-100"
                            },
                            {
                                "name": "Самостоятельная работа",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Разработать технико-экономическое обоснование проекта",
                                "mark": "90",
                                "range": "0-100"
                            },
                            {
                                "name": "Ответить на контрольные вопросы по теме",
                                "mark": "80",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить отчет за неделю",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить план деятельности",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Разработать план управления персоналом проекта",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Самостоятельная работа",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Представить отчет за неделю",
                                "mark": "20",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить план деятельности",
                                "mark": "60",
                                "range": "0-100"
                            },
                            {
                                "name": "Самостоятельная работа",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Ответить на контрольные вопросы по теме",
                                "mark": "90",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить отчет за неделю",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить план деятельности",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Самостоятельная работа",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Представить отчет за неделю",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить план деятельности",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Самостоятельная работа",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Представить отчет за неделю",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить план деятельности",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Самостоятельная работа",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Представить отчет за неделю",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить план деятельности",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Самостоятельная работа",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Представить отчет за неделю",
                                "mark": "80",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить план деятельности",
                                "mark": "80",
                                "range": "0-100"
                            },
                            {
                                "name": "Самостоятельная работа",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Представить отчет за неделю",
                                "mark": "90",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить план деятельности",
                                "mark": "90",
                                "range": "0-100"
                            },
                            {
                                "name": "Самостоятельная работа",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Представить отчет за неделю",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить план деятельности",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Самостоятельная работа",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Самостоятельная работа",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Самостоятельная работа",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Самостоятельная работа",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Самостоятельная работа",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Самостоятельная работа",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Самостоятельная работа",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Представить отчет за неделю",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Обновить проектные документы",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Подготовить отчет о технической реализации проекта",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Ответить на контрольные вопросы по теме",
                                "mark": "80",
                                "range": "0-100"
                            },
                            {
                                "name": "Разработать план управления качеством",
                                "mark": "90",
                                "range": "0-100"
                            },
                            {
                                "name": "Скомпоновать план управления проектом",
                                "mark": "70",
                                "range": "0-100"
                            },
                            {
                                "name": "Подготовить перечень аналогов ПО",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Скорректировать Техническое задание",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Провести контроль содержания плана управления проектом",
                                "mark": "70",
                                "range": "0-100"
                            },
                            {
                                "name": "Провести мониторинг плана управления проектом",
                                "mark": "80",
                                "range": "0-100"
                            },
                            {
                                "name": "Тест «Управление проектами»",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Обновить диаграмму Ганта",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Сделать индивидуальный устный отчет о проделанной работе и предстоящих задачах",
                                "mark": "90",
                                "range": "0-100"
                            },
                            {
                                "name": "Оценить эффективность работы команды проекта",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Подготовить Руководство пользователя",
                                "mark": "95",
                                "range": "0-100"
                            },
                            {
                                "name": "Самостоятельная работа",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Представить план деятельности",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить отчет за неделю",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Подготовить тезисы доклада для участия в конференции",
                                "mark": "70",
                                "range": "0-100"
                            },
                            {
                                "name": "Подготовить презентацию проекта",
                                "mark": "90",
                                "range": "0-100"
                            },
                            {
                                "name": "Подготовить пояснительную записку",
                                "mark": "90",
                                "range": "0-100"
                            },
                            {
                                "name": "Самостоятельная работа",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Обновить диаграмму Ганта",
                                "mark": "95",
                                "range": "0-100"
                            },
                            {
                                "name": "Самостоятельная работа",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Представить план деятельности",
                                "mark": "95",
                                "range": "0-100"
                            },
                            {
                                "name": "Скорректировать Техническое задание",
                                "mark": "95",
                                "range": "0-100"
                            },
                            {
                                "name": "Зарегистрировать доклад на конференцию",
                                "mark": "70",
                                "range": "0-100"
                            },
                            {
                                "name": "Ознакомиться с вопросами",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Самостоятельная работа",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Представить план деятельности",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Провести мониторинг плана управления проектом",
                                "mark": "95",
                                "range": "0-100"
                            },
                            {
                                "name": "Подготовить отчет о тестировании",
                                "mark": "0",
                                "range": "0-100"
                            },
                            {
                                "name": "Отправить пакет документов для государственной регистрации разработанного программного продукта",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Самостоятельная работа",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Представить отчёт за неделю",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить план деятельности",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Подготовить комплексный технический отчет",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Сделать индивидуальный устный отчет о проделанной работе и предстоящих задачах",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Самостоятельная работа",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Представить план деятельности",
                                "mark": "80",
                                "range": "0-100"
                            },
                            {
                                "name": "Провести статусное совещание по проекту",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Подготовить доклад на конференцию",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Провести мониторинг плана управления проектом",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Самостоятельная работа",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Представить отчёт за неделю",
                                "mark": "90",
                                "range": "0-100"
                            },
                            {
                                "name": "Самостоятельная работа",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Представить план деятельности",
                                "mark": "90",
                                "range": "0-100"
                            },
                            {
                                "name": "Подготовить комплексный технический отчет",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Подготовить первый и второй разделы пояснительной записки",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Самостоятельная работа",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Представить план деятельности",
                                "mark": "95",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить отчёт за неделю",
                                "mark": "95",
                                "range": "0-100"
                            },
                            {
                                "name": "Обновить Руководство пользователя",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Подготовить Руководство программиста",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить отчёт за неделю",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Представить план деятельности",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Самостоятельная работа",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Подготовить третий раздел пояснительной записки",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "7945",
                                "range": "0-11010"
                            }
                        ],
                        "eCourseName": "Курс: Командный курсовой проект «Корпоративные приложения». Курс 4. Семестр 8"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Практическая работа по теме \"Бизнес-процессы\"",
                                "mark": "отлично (5)",
                                "range": "0-5"
                            },
                            {
                                "name": "Практическая работа по теме \"Бизнес-операции\"",
                                "mark": "отлично (5)",
                                "range": "0-5"
                            },
                            {
                                "name": "Практическая работа по теме \"Бизнес-службы\"",
                                "mark": "отлично (10)",
                                "range": "0-10"
                            },
                            {
                                "name": "Практическая работа по теме \"Рабочий процесс(Workflow)\"",
                                "mark": "отлично (5)",
                                "range": "0-5"
                            },
                            {
                                "name": "Итоговый тест",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "отлично (35)",
                                "range": "0-35"
                            }
                        ],
                        "eCourseName": "Курс: Интеграционная платформа \"Ensemble\""
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Тест по основам COS",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Объектный подход в COS",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итоговый тест",
                                "mark": "-",
                                "range": "0-9"
                            },
                            {
                                "name": "Контроль проекта",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Проверка тестов",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Диаграмма классов",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Join запрос",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Встроенный SQL",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Итоговый контроль проекта",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Интерактивная лекция \"Отношения в COS\"",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Проектная защита (работа)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Проектная защита (оценка)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Практическая работа №1 (отчеты)",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическая работа №2 (отчеты)",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическая работа №3",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическая работа №4",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическая работа №5",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "110",
                                "range": "0-1249"
                            }
                        ],
                        "eCourseName": "Курс: Программирование на COS в Intersystems Cache"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Практическое задание №1",
                                "mark": "зачтено",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Практическое задание №2",
                                "mark": "зачтено",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Практическое задание №3",
                                "mark": "зачтено",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Практическое задание №4",
                                "mark": "зачтено",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Практическое задание №5",
                                "mark": "зачтено",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Практическое задание №6",
                                "mark": "зачтено",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Практическое задание №7",
                                "mark": "зачтено",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Практическое задание №8",
                                "mark": "зачтено",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Практическое задание №9",
                                "mark": "зачтено",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Практическое задание №10",
                                "mark": "зачтено",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Практическое задание №11",
                                "mark": "зачтено",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Практическое задание №12",
                                "mark": "зачтено",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Практическое задание №13",
                                "mark": "зачтено",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Итоговый тест",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Практическое задание №14 (new)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "26",
                                "range": "0-138"
                            }
                        ],
                        "eCourseName": "Курс: Разработка web-приложений"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Тест 1",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Лабораторная работа 1. Связывание объектов по ассоциациям классов",
                                "mark": "10,0",
                                "range": "0-10"
                            },
                            {
                                "name": "Лабораторная работа 2. Наследование и полиморфизм",
                                "mark": "10,0",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 2",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Лабораторная работа 3. Диаграмма вариантов использования",
                                "mark": "10,0",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 4",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 5",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Контрольная работа 1",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 6",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 7",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 8",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Лабораторная работа 4. Проектирование с использованием шаблонов GRASP",
                                "mark": "10,0",
                                "range": "0-10"
                            },
                            {
                                "name": "Контрольная работа 2",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Лабораторная работа 5. Порождающие шаблоны",
                                "mark": "10,0",
                                "range": "0-10"
                            },
                            {
                                "name": "Итоговый тест",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "4,3",
                                "range": "0-10"
                            }
                        ],
                        "eCourseName": "Курс: Объектно-ориентированное программирование"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "60",
                                "range": "0-212"
                            },
                            {
                                "name": "Лаб.работа №1",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Лаб.работа №2",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Лаб.работа №3",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Лаб.работа №4",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Лаб.работа №5",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Лаб.работа №6",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Лабораторная работа 1",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Лабораторная работа 2",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Лабораторная работа 3",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Лабораторная работа 4",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Лабораторная работа 5",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Лабораторная работа 6",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Экзамен",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Тест 4",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 1",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 2",
                                "mark": "-",
                                "range": "0-10"
                            }
                        ],
                        "eCourseName": "Курс: Основы сетевых технологий и протоколов"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Методические указания по выполнению курсового проектирования",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическое занятие № 13-14",
                                "mark": "0",
                                "range": "0-100"
                            },
                            {
                                "name": "ГОСТ Р ИСО/МЭК 12207-2010",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическое занятие № 15-16",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Публикуем статьи и доклады на научно-технических конференциях в весеннем семестре 2015/16 уч.г.",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическое занятие № 17-18",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Создайте презентацию вашего командного проекта",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическое занятие №2",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическое занятие № 5-6",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическое занятие №7-8",
                                "mark": "75",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическое занятие № 9-10",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическое занятие № 11-12",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическое занятие № 19-20",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическое занятие № 23-24",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическое занятие № 25-26",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическое занятие № 27-28",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическое занятие № 29-30",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Аналитический дебрифинг по проектной деятельности на платформе 1С (работа)",
                                "mark": "-",
                                "range": "0-60"
                            },
                            {
                                "name": "Аналитический дебрифинг по проектной деятельности на платформе 1С (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Процесс детального проектирования программных средств (на основе использования технологий InterSystems Corporation)",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Процесс конструирования программных средств (на основе использования технологий InterSystems Corporation)",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Процесс комплексирования программных средств (на основе использования технологий InterSystems Corporation)",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Процесс квалификационного тестирования программных средств (на основе использования технологий InterSystems Corporation)",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Процесс менеджмента программной документации (на основе использования технологий InterSystems Corporation)",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Процесс менеджмента конфигурации. Процесс обеспечения гарантий качества программных средств (на основе использования технологий InterSystems Corporation)",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Процесс верификации программных средств. Процесс валидации программных средств",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Процесс ревизии программных средств. Процесс аудита программных средств",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Процесс решения проблем в программных средствах. Процесс проектирования доменов. Процесс менеджмента повторного применения программ. Процесс менеджмента повторного применения активов",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Аналитический дебрифинг по проектной деятельности с использованием технологий InterSystems Corporation (работа)",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Аналитический дебрифинг по проектной деятельности с использованием технологий InterSystems Corporation (оценка)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Защита командных курсовых проектов (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Защита командных курсовых проектов (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Посещение занятия",
                                "mark": "0",
                                "range": "0-4"
                            },
                            {
                                "name": "Посещение занятия",
                                "mark": "-",
                                "range": "0-4"
                            },
                            {
                                "name": "Посещение занятия",
                                "mark": "-",
                                "range": "0-4"
                            },
                            {
                                "name": "Посещение занятия",
                                "mark": "-",
                                "range": "0-4"
                            },
                            {
                                "name": "Посещение занятия",
                                "mark": "-",
                                "range": "0-4"
                            },
                            {
                                "name": "Посещение занятия",
                                "mark": "-",
                                "range": "0-4"
                            },
                            {
                                "name": "Посещение занятия",
                                "mark": "-",
                                "range": "0-4"
                            },
                            {
                                "name": "Посещение занятия",
                                "mark": "-",
                                "range": "0-4"
                            },
                            {
                                "name": "Посещение занятия",
                                "mark": "-",
                                "range": "0-4"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "21",
                                "range": "0-100"
                            }
                        ],
                        "eCourseName": "Курс: Командный курсовой проект"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Итог категории",
                                "mark": "0",
                                "range": "0-20"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-0"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-0"
                            },
                            {
                                "name": "Лекции по численным методам нулевого порядка (\"высокая\" траектория)",
                                "mark": "5",
                                "range": "0-5"
                            },
                            {
                                "name": "Лекции по численным методам нулевого порядка (\"средняя\" траектория)",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Лекции по численным методам нулевого порядка (\"низкая\" траектория)",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание №1. Метод Фибоначчи",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание №1. Метод Фибоначчи",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание №1. Метод Фибоначчи",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание №2. Метод Пауэлла (квадратичная интерполяция)",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание №2. Метод Пауэлла (квадратичная интерполяция)",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание №3. Метод Хука-Дживса",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание №1. Метод Ньютона",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание №1. Метод Ньютона",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание №1. Метод Ньютона",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание №2. Метод Ньютона-Рафсона",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание №2. Метод Ньютона-Рафсона",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание №3. Метод Марквардта",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание №3. Метод Хука-Дживса",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание №4. Метод Нелдера-Мида",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание №4. Метод Нелдера-Мида",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание №2. Метод Нелдера-Мида",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание №5. Метод Розенброка",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание №6. Метод Пауэлла (сопряженных направлений)",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Выбор траектории обучения по разделу \"Численные методы нулевого порядка для поиска безусловного экстремума\"",
                                "mark": "1",
                                "range": "0-1"
                            },
                            {
                                "name": "Выбор траектории обучения по разделу \"Численные методы первого порядка для поиска безусловного экстремума\"",
                                "mark": "1",
                                "range": "0-1"
                            },
                            {
                                "name": "Выбор траектории обучения по разделу \"Численные методы второго порядка для поиска безусловного экстремума\"",
                                "mark": "1",
                                "range": "0-1"
                            },
                            {
                                "name": "Итоговый тест по разделу \"Численные методы нулевого порядка для поиска безусловного экстремума\" (\"низкая\" траектория)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Задание №1. Метод градиентного спуска с постоянным шагом",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание №1. Метод градиентного спуска с постоянным шагом",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание №1. Метод градиентного спуска с постоянным шагом",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание №2. Метод наискорейшего градиентного спуска",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание №2. Метод наискорейшего градиентного спуска",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание №2. Метод наискорейшего градиентного спуска",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание №3. Метод покоординатного спуска",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание №3. Метод покоординатного спуска",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Задание №4. Метод Гаусса-Зейделя",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание №4. Метод Гаусса-Зейделя",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание №5. Метод Флетчера-Ривса",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание №6. Метод Дэвидона-Флетчера-Пауэлла",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Лекция по методам первого порядка (\"низкая\" траектория)",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Лекция по методам первого порядка (\"средняя\" траектория)",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Лекция по методам первого порядка (\"высокая\" траектория)",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Лекции по численным методам второго порядка (\"высокая\" траектория) Лекция",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Лекции по численным методам второго порядка (\"средняя\" траектория) Лекция",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Лекции по численным методам второго порядка (\"низкая\" траектория) Лекция",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Итоговый тест по разделу методы первого порядка (\"низкая\" траектория)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итоговый тест по разделу методы первого порядка (\"Средняя\" траектория)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итоговый тест по разделу методы первого порядка (\"высокая\" траектория)",
                                "mark": "6",
                                "range": "0-10"
                            },
                            {
                                "name": "Задание 1 по разделу 1 (для всех траекторий)",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Задание 1 по разделу 2 (для всех траекторий)",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Задание 2 по разделу 2 (дополнительно для средней и высокой траекторий)",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Задание 2 по разделу 1 (дополнительно для средней и высокой траекторий)",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Задание 3 по разделу 2 (дополнительно для высокой траектории)",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Задание 3 по разделу 1 (дополнительно для высокой траектории)",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "ИССЛЕДОВАТЕЛЬСКАЯ РАБОТА ПО ГЕНЕТИЧЕСКИМ АЛГОРИТМАМ",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "КОНТРОЛЬНАЯ РАБОТА №1",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "14",
                                "range": "0-1563"
                            }
                        ],
                        "eCourseName": "Курс: Методы оптимизации (адаптивный)"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Лабораторная работа 1 - Постановка задачи и написание технического задания",
                                "mark": "зачтено",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Лабораторная работа 2 - Требования к программному продукту и проектирование интерфейса",
                                "mark": "зачтено",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Лабораторная работа 3 - Разработка программного продукта и руководства к нему",
                                "mark": "зачтено",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Лабораторная работа 4 - Тестирование и анализ программного продукта",
                                "mark": "зачтено",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "40",
                                "range": "0-40"
                            },
                            {
                                "name": "Лекция 2: Основные понятия тестирования",
                                "mark": "2",
                                "range": "0-2"
                            },
                            {
                                "name": "Лекция 3: Классификация тестирования",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Лекция 4: Оценка оттестированности проекта: метрики и методика интегральной оценки",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Лекция 5: Модульное и интеграционное тестирование",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Лекция 6: Интеграционное тестирование и его особенности для объектно-ориентированного программирования",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Лекция 7: Разновидности тестирования: системное и регрессионное ",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Лекция 8: Автоматизация тестирования",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Лекция 9: Индустриальный подход. Индустриальное тестирование.",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Лекция 10: Документирование и оценка индустриального тестирования",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Лекция 11: Цели и задачи регрессионного тестирования",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Лекция 12: Случайные методы",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Лекция 13: Регрессионное тестирование: методики, не связанные с отбором тестов и методики порождения тестов.",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Лекция 14: Регрессионное тестирование: алгоритм и программная система поддержки",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "1",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест по практической работе 1",
                                "mark": "4",
                                "range": "0-5"
                            },
                            {
                                "name": "Тест по практической работе 2",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест по практической работе 3",
                                "mark": "9",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест по практической работе 4",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 1",
                                "mark": "9",
                                "range": "0-9"
                            },
                            {
                                "name": "Тест 2",
                                "mark": "13",
                                "range": "0-13"
                            },
                            {
                                "name": "Тест по лекции 3",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Тест 3",
                                "mark": "7",
                                "range": "0-7"
                            },
                            {
                                "name": "Тест 4",
                                "mark": "4",
                                "range": "0-4"
                            },
                            {
                                "name": "Тест 5",
                                "mark": "5",
                                "range": "0-5"
                            },
                            {
                                "name": "Тест 6",
                                "mark": "9",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 7",
                                "mark": "8",
                                "range": "0-8"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "27",
                                "range": "0-30"
                            },
                            {
                                "name": "Итоговый тест",
                                "mark": "-",
                                "range": "0-15"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Экзамен",
                                "mark": "-",
                                "range": "Неявка-Отлично"
                            },
                            {
                                "name": "Входное тестирование",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "50",
                                "range": "0-100"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "Хорошо (77 %)",
                                "range": "1-100"
                            }
                        ],
                        "eCourseName": "Курс: Методы тестирования программных систем"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Практическое занятие № 1",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 2",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 3",
                                "mark": "3",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 4",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 5",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 6",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 7",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 8",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 9",
                                "mark": "0",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 10",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 11",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 12",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 13",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 14",
                                "mark": "0",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 15",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 16",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое задание № 17",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 18",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 19",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 20",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 21",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 22",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 23",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 24",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 25",
                                "mark": "-",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 26",
                                "mark": "-",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 27",
                                "mark": "-",
                                "range": "0-4"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "45",
                                "range": "0-108"
                            },
                            {
                                "name": "Контрольная работа № 1",
                                "mark": "5",
                                "range": "0-5"
                            },
                            {
                                "name": "Контрольная работа № 2",
                                "mark": "4",
                                "range": "0-5"
                            },
                            {
                                "name": "Контрольная работа № 3",
                                "mark": "4",
                                "range": "0-5"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "13",
                                "range": "0-15"
                            },
                            {
                                "name": "Задание по 1 разделу",
                                "mark": "4",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание по 2 разделу",
                                "mark": "4",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание по 3 разделу",
                                "mark": "4",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание по 4 разделу",
                                "mark": "4",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание по 5 разделу",
                                "mark": "4",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание по 6 разделу",
                                "mark": "4",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание по 7 разделу",
                                "mark": "3",
                                "range": "0-5"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "27",
                                "range": "0-35"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "85",
                                "range": "0-158"
                            },
                            {
                                "name": "Практическое занятие № 1",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 2",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 3",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 4",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 5",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 6",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 7",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 8",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 9",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 10",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 11",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 12",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 13",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 14",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 15",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 16",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 17",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 18",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 19",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 20",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 21",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 22",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 23",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 24",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 25",
                                "mark": "-",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 26",
                                "mark": "-",
                                "range": "0-4"
                            },
                            {
                                "name": "Практическое занятие № 27",
                                "mark": "-",
                                "range": "0-4"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "48",
                                "range": "0-108"
                            },
                            {
                                "name": "Задание по 1 разделу",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание по 2 разделу",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание по 3 разделу",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание по 4 разделу",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание по 5 разделу",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание по 6 разделу",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание по 7 разделу",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание по 8 разделу",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "0",
                                "range": "0-40"
                            },
                            {
                                "name": "РГЗ 1 Классификация в распознавании образов",
                                "mark": "3",
                                "range": "0-5"
                            },
                            {
                                "name": "РГЗ 2 Планирование эксперимента",
                                "mark": "3",
                                "range": "0-5"
                            },
                            {
                                "name": "РГЗ 3 Непараметрическая обработка информации",
                                "mark": "3",
                                "range": "0-5"
                            },
                            {
                                "name": "РГЗ 4 Адаптивное управление и идентификация",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "9",
                                "range": "0-20"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "57",
                                "range": "0-168"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "142",
                                "range": "0-326"
                            },
                            {
                                "name": "Задание по 1 разделу (ЗО)",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание по 2 разделу (ЗО)",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание по 3 разделу (ЗО)",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание по 4 разделу (ЗО)",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание по 5 разделу (ЗО)",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание по 6 разделу (ЗО)",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание по 7 разделу (ЗО)",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "0",
                                "range": "0-35"
                            },
                            {
                                "name": "Задание по 1 разделу (ЗО)",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание по 2 разделу (ЗО)",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание по 3 разделу (ЗО)",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание по 4 разделу (ЗО)",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание по 5 разделу (ЗО)",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание по 6 разделу (ЗО)",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание по 7 разделу (ЗО)",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Задание по 8 разделу (ЗО)",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "0",
                                "range": "0-40"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "0",
                                "range": "0-75"
                            },
                            {
                                "name": "Зачет",
                                "mark": "зачтено",
                                "range": "неявка-зачтено"
                            },
                            {
                                "name": "Экзамен",
                                "mark": "неявка",
                                "range": "неявка-отлично"
                            }
                        ],
                        "eCourseName": "Курс: Модели стохастических объектов\nДисциплина: Модели стохастических объектов"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Тест 1 -- Циклический алгоритм",
                                "mark": "20",
                                "range": "0-20"
                            },
                            {
                                "name": "Тест 2 -- Линейная программа",
                                "mark": "20",
                                "range": "0-20"
                            },
                            {
                                "name": "Тест 3 -- Разветвляющаяся программа",
                                "mark": "13",
                                "range": "0-15"
                            },
                            {
                                "name": "Тест 3а - Логические операторы",
                                "mark": "12",
                                "range": "0-12"
                            },
                            {
                                "name": "Тест 4 -- Циклическая программа",
                                "mark": "17",
                                "range": "0-20"
                            },
                            {
                                "name": "Тест 5 -- Одномерные массивы",
                                "mark": "30",
                                "range": "0-30"
                            },
                            {
                                "name": "Тест 6 -- Функции",
                                "mark": "24",
                                "range": "0-25"
                            },
                            {
                                "name": "Тест -- 7 Строки символов",
                                "mark": "30",
                                "range": "0-30"
                            },
                            {
                                "name": "Тест 8 - Структуры и объединения",
                                "mark": "37",
                                "range": "0-40"
                            },
                            {
                                "name": "Итоговый тест",
                                "mark": "-",
                                "range": "0-50"
                            },
                            {
                                "name": "Защита лабораторной работы 1",
                                "mark": "-",
                                "range": "0-50"
                            },
                            {
                                "name": "Защита лабораторной работы 2",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Защита лабораторной работы 3",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Защита лабораторной работы 4",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Защита лабораторной работы 5",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Защита лабораторной работы 6",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Защита лабораторной работы 7",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Защита лабораторной работы 8",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Зачет",
                                "mark": "-",
                                "range": "неявка-зачтено"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "203",
                                "range": "0-1000"
                            }
                        ],
                        "eCourseName": "Курс: Основы программирования (часть 1)"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Тест 1 -- Структуры и объединения",
                                "mark": "93",
                                "range": "0-100"
                            },
                            {
                                "name": "Лабораторная работа 9 (1) --Текстовые файлы",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Лабораторная работа 10 (2) -- Бинарные файлы",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Лабораторная работа 11 (3) -- Файловая система",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Лабораторная работа 12 (4) -- Списки",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Лабораторная работа 13 (5) -- Связи",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Лабораторная работа 14 (6) -- Классы",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Лабораторная работа 15 (7) -- Графический интерфейс",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Экзамен",
                                "mark": "-",
                                "range": "Неявка-Отлично"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "99",
                                "range": "0-100"
                            }
                        ],
                        "eCourseName": "Курс: Основы программирования (часть 2)"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Лабораторная работа 1",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Лабораторная работа 2",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Лабораторная работа 3",
                                "mark": "90",
                                "range": "0-100"
                            },
                            {
                                "name": "Лабораторная работа 4",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Лабораторная работа 5",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Лабораторная работа 6",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Лабораторная работа 7",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "490",
                                "range": "0-700"
                            },
                            {
                                "name": "Занятие 1",
                                "mark": "п",
                                "range": "н-п"
                            },
                            {
                                "name": "Занятие 2",
                                "mark": "п",
                                "range": "н-п"
                            },
                            {
                                "name": "Занятие 3",
                                "mark": "п",
                                "range": "н-п"
                            },
                            {
                                "name": "Занятие 4",
                                "mark": "п",
                                "range": "н-п"
                            },
                            {
                                "name": "Занятие 5",
                                "mark": "п",
                                "range": "н-п"
                            },
                            {
                                "name": "Занятие 6",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Занятие 7",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Занятие 8",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Занятие 9",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "10",
                                "range": "0-18"
                            },
                            {
                                "name": "Экзамен",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "500",
                                "range": "0-818"
                            }
                        ],
                        "eCourseName": "Курс: Парадигмы программирования"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Отчет УчПракт",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Отчет НИР",
                                "mark": "5",
                                "range": "0-5"
                            },
                            {
                                "name": "Отчет ПроизвПракт",
                                "mark": "5",
                                "range": "0-5"
                            },
                            {
                                "name": "Отчет ПреддПракт",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "10",
                                "range": "0-20"
                            }
                        ],
                        "eCourseName": "Курс: Практики бакалавров \"Программная инженерия\""
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Лабораторная работа 1",
                                "mark": "5",
                                "range": "0-5"
                            },
                            {
                                "name": "Лабораторная работа 2",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Лабораторная работа 3",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Лабораторная работа 4",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Зачет",
                                "mark": "-",
                                "range": "неявка-зачтено"
                            },
                            {
                                "name": "лекция 1",
                                "mark": "-",
                                "range": "нет-посетил"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "5",
                                "range": "0-25"
                            }
                        ],
                        "eCourseName": "Курс: Программирование на Java для Android"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "ЛР 1 (расширенный курс)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "ЛР 1 (полный курс)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "ЛР 1 (сокращенный курс)",
                                "mark": "зачтено",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "ЛР 2 (полный курс)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "ЛР 2 (расширенный курс)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "ЛР 2 (сокращенный курс)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "ЛР 3 (расширенный курс)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "ЛР 3 (полный курс)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "ЛР 3 (сокращенный курс)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "ЛР 4 (сокращенный курс)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "ЛР 4 (полный курс)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "ЛР 4 (расширенный курс)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "ЛР 5 (полный курс)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "ЛР 5 (расширенный курс)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "ЛР 5 (сокращенный курс)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "ЛР 6 (расширенный курс)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "ЛР 6 (полный курс)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "ЛР 6 (сокращенный курс)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "7",
                                "range": "0-40"
                            },
                            {
                                "name": "Лекция 1 (сокращенный курс) - Основные этапы развития технологии разработки",
                                "mark": "4",
                                "range": "0-4"
                            },
                            {
                                "name": "Лекция 1 (расширенный курс) - Основные этапы развития технологии разработки",
                                "mark": "-",
                                "range": "0-1"
                            },
                            {
                                "name": "Лекция 1 (полный курс) - Основные этапы развития технологии разработки",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "4",
                                "range": "0-4"
                            },
                            {
                                "name": "Лекция 2 (сокращенный курс) - Эволюция моделей жизненного цикла программного обеспечения",
                                "mark": "4",
                                "range": "0-4"
                            },
                            {
                                "name": "Лекция 2 (полный курс) - Эволюция моделей жизненного цикла программного обеспечения",
                                "mark": "1",
                                "range": "0-2"
                            },
                            {
                                "name": "Лекция 2 (расширенный курс) - Эволюция моделей жизненного цикла программного обеспечения",
                                "mark": "-",
                                "range": "0-1"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "4",
                                "range": "0-4"
                            },
                            {
                                "name": "Лекция 3 (полный курс) - Гост Р ИСО 9000-2001. Системы менеджмента качества. Основные положения и словарь",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Лекция 3 (сокращенный курс) - Гост Р ИСО 9000-2001. Системы менеджмента качества. Основные положения и словарь",
                                "mark": "4",
                                "range": "0-4"
                            },
                            {
                                "name": "Лекция 3 (расширенный курс) - Гост Р ИСО 9000-2001. Системы менеджмента качества. Основные положения и словарь",
                                "mark": "-",
                                "range": "0-1"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "4",
                                "range": "0-4"
                            },
                            {
                                "name": "Лекция 4 (расширенный курс) - Состав ИСО/МЭК ТО 15504",
                                "mark": "-",
                                "range": "0-1"
                            },
                            {
                                "name": "Лекция 4 (полный курс) - Состав ИСО/МЭК ТО 15504",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Лекция 4 (сокращенный курс) - Состав ИСО/МЭК ТО 15504",
                                "mark": "4",
                                "range": "0-4"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "4",
                                "range": "0-4"
                            },
                            {
                                "name": "Лекция 5 (сокращённый) - Информационная технология. Процессы жизненного цикла программных средств",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Лекция 5 (расширенный) - Информационная технология. Процессы жизненного цикла программных средств",
                                "mark": "-",
                                "range": "0-1"
                            },
                            {
                                "name": "Лекция 5 (полный) - Информационная технология. Процессы жизненного цикла программных средств",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "2",
                                "range": "0-4"
                            },
                            {
                                "name": "Лекция 6 (сокращённый). Анализ проблемы и постановка задачи",
                                "mark": "4",
                                "range": "0-4"
                            },
                            {
                                "name": "Лекция 6 (расширенный) - Анализ проблемы и постановка задачи",
                                "mark": "-",
                                "range": "0-1"
                            },
                            {
                                "name": "Лекция 6 (полный) - Анализ проблемы и постановка задачи",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "4",
                                "range": "0-4"
                            },
                            {
                                "name": "Лекция 7 (сокращённый) - Определение границ системы - решения",
                                "mark": "4",
                                "range": "0-4"
                            },
                            {
                                "name": "Лекция 7 (расширенный) - Определение границ системы-решения",
                                "mark": "-",
                                "range": "0-1"
                            },
                            {
                                "name": "Лекция 7 (полный) - Определение границ системы-решения",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "4",
                                "range": "0-4"
                            },
                            {
                                "name": "Лекция 8 (сокращённый) - Требования IDEF3 к описанию бизнес-процессов",
                                "mark": "3",
                                "range": "0-4"
                            },
                            {
                                "name": "Лекция 8 (расширенный) - Требования IDEF3 к описанию бизнес-процессов",
                                "mark": "-",
                                "range": "0-1"
                            },
                            {
                                "name": "Лекция 8 (полный) - Требования IDEF3 к описанию бизнес-процессов",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "3",
                                "range": "0-4"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "9",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест на проверку практических знаний - 1",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест на проверку теоретических знаний - 1",
                                "mark": "9",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест на проверку практических знаний - 2",
                                "mark": "9",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест на проверку теоретических знаний - 3",
                                "mark": "9",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест на проверку практических знаний - 3",
                                "mark": "8",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест на проверку практических знаний - 4",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест на проверку теоретических знаний - 4",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест на проверку практических знаний - 5",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест на проверку теоретических знаний - 5",
                                "mark": "8",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест на проверку теоретических знаний - 6",
                                "mark": "7",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест на проверку практических знаний - 6",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест на проверку теоретических знаний - 7",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест на проверку практических знаний - 7",
                                "mark": "7",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест на проверку теоретических знаний - 8",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест на проверку практических знаний - 8",
                                "mark": "9",
                                "range": "0-10"
                            },
                            {
                                "name": "Тренировочный тест на проверку навыков - 1",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тренировочный тест на проверку навыков - 2",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Экзаменационный тест на проверку навыков - 1",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Экзаменационный тест на проверку навыков - 2",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест на проверку теоретических знаний - 2",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "22",
                                "range": "0-30"
                            },
                            {
                                "name": "Входное тестирование",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Итоговый тест",
                                "mark": "18",
                                "range": "0-20"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "18",
                                "range": "0-20"
                            },
                            {
                                "name": "Экзамен",
                                "mark": "-",
                                "range": "Неявка-Отлично"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "Удовлетворительно (55 %)",
                                "range": "1-100"
                            }
                        ],
                        "eCourseName": "Курс: Проектирование и архитектура информационных систем\nДисциплина: Проектирование и архитектура информационных систем"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "ЛР1 (Простая траектория)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "ЛР1 (Средняя траектория)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "ЛР1 (Сложная траектория)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "ЛР2 (Простая траектория)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "ЛР2 (Средняя траектория)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "ЛР2 (Сложная траектория)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "ЛР3 (Простая траектория)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "ЛР3 (Средняя траектория)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "ЛР3 (Сложная траектория)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "ЛР4 (Простая траектория)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "ЛР4 (Средняя траектория)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "ЛР4 (Сложная траектория)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-45"
                            },
                            {
                                "name": "Тест 1 (Простая траектория). Основные этапы развития технологии разработки",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 1 (Средняя траектория). Основные этапы развития технологии разработки",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 1 (Сложная траектория). Основные этапы развития технологии разработки",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 1 (Простая траектория). Практика",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 1 (Средняя траектория). Практика",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 1 (Сложная траектория). Практика",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 2 (Простая траектория). Анализ проблемы и постановка задачи",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 2 (Средняя траектория). Анализ проблемы и постановка задачи",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 2 (Сложная траектория). Анализ проблемы и постановка задачи",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 2 (Простая траектория). Практика",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 2 (Средняя траектория). Практика",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 2 (Сложная траектория). Практика",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 3 (Простая траектория). Анализ требований и их формализация",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 3 (Средняя траектория). Анализ требований и их формализация",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 3 (Сложная траектория). Анализ требований и их формализация",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 3 (Простая траектория). Практика",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 3 (Средняя траектория). Практика",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 3 (Сложная траектория). Практика",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 4 (Простая траектория). Теория",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 4 (Средняя траектория). Теория",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 4 (Сложная траектория). Теория",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 4 (Сложная траектория). Практический материал",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 4 (Средняя траектория). Практический материал",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 4 (Простая траектория). Практический материал",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 5 (Простая траектория). Теория",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 5 (Средняя траектория). Теория",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 5 (Сложная траектория). Теория",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 5.1 (Простая траектория). Практика",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 5.1 (Средняя траектория). Практика",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 5.1 (Сложная траектория). Практика",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 5.2 (Простая траектория). Практика",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 5.2 (Средняя траектория). Практика",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 5.2 (Сложная траектория). Практика",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-35"
                            },
                            {
                                "name": "Итоговый тест (Простая траектория)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итоговый тест (Средняя траектория)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итоговый тест (Сложная траектория)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Экзамен",
                                "mark": "-",
                                "range": "Неявка-Отлично"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "-",
                                "range": "1-100"
                            }
                        ],
                        "eCourseName": "Курс: Разработка и анализ требований"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Стандарты в области процессов жизненного цикла программного продукта: IDEF, ISO, национальные стандарты",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Управление проектной деятельностью основе стандарта РМВОК",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Модели жизненного цикла программного продукта",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Модели жизненного цикла программного продукта",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Структурные методы анализа предметной области",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Структурные методы анализа предметной области",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Реинжиниринг предметной области",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Инструментальные средства реинжиниринга",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Оценка стоимости проекта",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Показатели экономической эффективности проекта",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Управление длительностью программного проекта",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Календарное планирование проекта. Сетевой график работ по проекту",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Распределение ролей участников проекта, их полномочий и ответственности",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Распределение ролей участников проекта, их полномочий и ответственности",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Управление качеством программного проекта",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Основные процессы исполнения, контроля и завершения проекта. Управление рисками проекта",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Управление внедрением программного проекта. Управление информацией проекта. Стратегия внедрения",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Жизненный цикл управления проектом. Организация процессов управления в жизненном цикле проекта",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Разработка документации проекта. План проекта",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Разработка требований к проекту и спецификаций для компонентов программного продукта",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Посещение лекции",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Посещение лекции",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Посещение лекции",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Посещение лекции",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Посещение лекции",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Посещение лекции",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Посещение лекции",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Посещение лекции",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Посещение лекции",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Посещение лекции",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Посещение лекции",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Посещение лекции",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Посещение лекции",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Посещение лекции",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Посещение лекции",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Посещение лекции",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Посещение лекции",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "-",
                                "range": "0-2034"
                            }
                        ],
                        "eCourseName": "Курс: Управление программными проектами"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Защита лабораторной работы 1",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Посещение занятий на 1 неделе обучения",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий на 2 неделе обучения",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий на 3 неделе обучения",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий на 4 неделе обучения",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий на 5 неделе обучения",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий на 6 неделе обучения",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий на 7 неделе обучения",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Защита лабораторной работы 2",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Защита лабораторной работы 3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Защита лабораторной работы 5",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Защита лабораторной работы 4",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Защита лабораторной работы 6",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Входное тестирование",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "-",
                                "range": "0-84"
                            }
                        ],
                        "eCourseName": "Курс: Язык программирования C#"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "РГЗ 1. Расчёт энтропии и избыточности алфавитных источников информации",
                                "mark": "75",
                                "range": "0-100"
                            },
                            {
                                "name": "РГЗ 2. Равномерное и оптимальное кодирование.",
                                "mark": "85",
                                "range": "0-100"
                            },
                            {
                                "name": "РГЗ 3. Количество и объем информации в сообщении. Криптографическое закрытие сообщений",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "РГЗ 4. Арифметические операции и представление чисел в различных натуральных системах счисления",
                                "mark": "85",
                                "range": "0-100"
                            },
                            {
                                "name": "РГЗ 5. Представление числа в нормализованном виде. Машинный код числа.",
                                "mark": "85",
                                "range": "0-100"
                            },
                            {
                                "name": "РГЗ 6. Арифметические операции над двоичными числами.",
                                "mark": "85",
                                "range": "0-100"
                            },
                            {
                                "name": "Практические занятия по подготовке и редактированию документа средствами MS Word",
                                "mark": "80",
                                "range": "0-100"
                            },
                            {
                                "name": "Практические занятия по обработке данных средствами MS Excel",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Самостоятельная работа. Тренажеры по методам оптимального кодирования.",
                                "mark": "80",
                                "range": "0-100"
                            },
                            {
                                "name": "Зачет",
                                "mark": "зачтено",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Входное_тестирование",
                                "mark": "7",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест_1",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест_2",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест_3",
                                "mark": "5",
                                "range": "0-5"
                            },
                            {
                                "name": "Тест_4",
                                "mark": "5",
                                "range": "0-5"
                            },
                            {
                                "name": "Тест_5",
                                "mark": "4",
                                "range": "0-5"
                            },
                            {
                                "name": "Тест_6",
                                "mark": "5",
                                "range": "0-5"
                            },
                            {
                                "name": "Тест_8",
                                "mark": "5",
                                "range": "0-5"
                            },
                            {
                                "name": "Самостоятельная работа с теоретическим материалом",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Самостоятельная работа с теоретическим материалом",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Самостоятельная работа с теоретическим материалом",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Решение задач по теме 4",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Решение задач по теме 3",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Решение задач по теме 2",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Решение задач по теме 1",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "37",
                                "range": "0-100"
                            }
                        ],
                        "eCourseName": "Курс: Информатика"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Письменные задания к лекции 16",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Письменные задания к лекции 18",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Посещение занятий",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Тест \"Линейная алгебра\"",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Тест \" Векторная алгебра\"",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Тест \"Аналитическая геометрия\"",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "0,00",
                                "range": "0-60"
                            },
                            {
                                "name": "Входное тестирование",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Тестовые задания к лекции 1",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Тестовые задания к лекции 2",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Письменные задания к лекции 15",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Письменные задания к лекции 17",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Тестовые задания к лекции 3",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Тестовые задания к лекции 4",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Тестовые задания к лекции 5",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Тестовые задания к лекции 6",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Тестовые задания к лекции 7",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Тестовые задания к лекции 8",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Тестовые задания к лекции 9",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Тестовые задания к лекции 10",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Тестовые задания к лекции 11",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Тестовые задания к лекции 12",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Тестовые задания к лекции 13",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Тестовые задания к лекции 14",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Тестовые задания к разделу \"Абстрактная алгебра\"",
                                "mark": "-",
                                "range": "0-4"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "0,00",
                                "range": "0-49"
                            },
                            {
                                "name": "Векторная алгебра",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Поверхности второго порядка",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "0,00",
                                "range": "0-20"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "0,00",
                                "range": "0-135"
                            }
                        ],
                        "eCourseName": "Курс: Алгебра и геометрия"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Задачи 4",
                                "mark": "3",
                                "range": "0-3"
                            },
                            {
                                "name": "Задачи 5",
                                "mark": "3",
                                "range": "0-3"
                            },
                            {
                                "name": "Задачи 1",
                                "mark": "2",
                                "range": "0-2"
                            },
                            {
                                "name": "Задачи 2",
                                "mark": "2",
                                "range": "0-2"
                            },
                            {
                                "name": "Задачи 3",
                                "mark": "2",
                                "range": "0-2"
                            },
                            {
                                "name": "Задачи 6",
                                "mark": "2",
                                "range": "0-2"
                            },
                            {
                                "name": "Задачи 8",
                                "mark": "2",
                                "range": "0-2"
                            },
                            {
                                "name": "Задачи 7",
                                "mark": "100 %",
                                "range": "0-2"
                            },
                            {
                                "name": "Задачи 9",
                                "mark": "3",
                                "range": "0-3"
                            },
                            {
                                "name": "Задачи 10",
                                "mark": "2",
                                "range": "0-2"
                            },
                            {
                                "name": "Задачи 11",
                                "mark": "2",
                                "range": "0-2"
                            },
                            {
                                "name": "Задачи 12",
                                "mark": "2",
                                "range": "0-2"
                            },
                            {
                                "name": "Задачи 13",
                                "mark": "2",
                                "range": "0-2"
                            },
                            {
                                "name": "Задачи 14",
                                "mark": "3",
                                "range": "0-3"
                            },
                            {
                                "name": "Задачи 15",
                                "mark": "2",
                                "range": "0-2"
                            },
                            {
                                "name": "Задачи 16",
                                "mark": "2",
                                "range": "0-2"
                            },
                            {
                                "name": "Задачи 17",
                                "mark": "2",
                                "range": "0-2"
                            },
                            {
                                "name": "Задачи 18",
                                "mark": "1",
                                "range": "0-2"
                            },
                            {
                                "name": "Тест \" Векторная алгебра\"",
                                "mark": "18",
                                "range": "0-20"
                            },
                            {
                                "name": "Тест \"Аналитическая геометрия\"",
                                "mark": "12",
                                "range": "0-20"
                            },
                            {
                                "name": "Тест \"Линейная алгебра\"",
                                "mark": "16",
                                "range": "0-20"
                            },
                            {
                                "name": "Экзамен",
                                "mark": "-",
                                "range": "неявка-отлично"
                            },
                            {
                                "name": "Посещение занятий 1",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий 2",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий 3",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий 4",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий 5",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий 6",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий 7",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий 8",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий 9",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий 10",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий 11",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий 12",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий 13",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий 14",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий 15",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий 16",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий 17",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Посещение занятий 18",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "12",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "85",
                                "range": "0-110"
                            }
                        ],
                        "eCourseName": "Курс: Алгебра и геометрия"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Тест по теме \"Множества\"",
                                "mark": "3",
                                "range": "0-6"
                            },
                            {
                                "name": "Тест по теме \"Отношения\"",
                                "mark": "6",
                                "range": "0-6"
                            },
                            {
                                "name": "Тест по теме: Комбинаторика",
                                "mark": "5",
                                "range": "0-6"
                            },
                            {
                                "name": "Тест 1 по теме: Теория графов",
                                "mark": "5",
                                "range": "0-6"
                            },
                            {
                                "name": "Тест 2 по теме: Теория графов",
                                "mark": "6",
                                "range": "0-6"
                            },
                            {
                                "name": "Итоговое тестирование",
                                "mark": "8",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "33",
                                "range": "0-40"
                            },
                            {
                                "name": "Задание 1 для очно-заочных и заочных форм обучения",
                                "mark": "-",
                                "range": "0-30"
                            },
                            {
                                "name": "Задание 2 для очно-заочных и заочных форм обучения",
                                "mark": "-",
                                "range": "0-30"
                            },
                            {
                                "name": "Текущая экзаменационная оценка",
                                "mark": "-",
                                "range": "неявка-отлично"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "33",
                                "range": "0-100"
                            }
                        ],
                        "eCourseName": "Курс: Дискретная математика.ЗО"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "2,0 (2,0 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "0,0 (0,0 %)",
                                "range": "0-36"
                            },
                            {
                                "name": "Тест 1 -- Введение в анализ",
                                "mark": "-",
                                "range": "0-12"
                            },
                            {
                                "name": "Тест 2 -- Дифференциальное исчисление функции одной переменной",
                                "mark": "-",
                                "range": "0-12"
                            },
                            {
                                "name": "Тест 3 -- Интегральное исчисление",
                                "mark": "-",
                                "range": "0-12"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "0,0 (0,0 %)",
                                "range": "0-32"
                            },
                            {
                                "name": "Тест 1 -- тренажер",
                                "mark": "-",
                                "range": "0-12"
                            },
                            {
                                "name": "Тест 2 - тренажер",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 3 - тренажер",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "0,0 (0,0 %)",
                                "range": "0-24"
                            },
                            {
                                "name": "Модульное задание \"Введение в анализ\" (работа)",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Модульное задание \"Введение в анализ\" (оценка)",
                                "mark": "-",
                                "range": "0-3"
                            },
                            {
                                "name": "Модульное задание \"Дифференциальное исчисление функции одной переменной\" (работа)",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Модульное задание \"Дифференциальное исчисление функции одной переменной\" (оценка)",
                                "mark": "-",
                                "range": "0-3"
                            },
                            {
                                "name": "Модульное задание \"Интегральное исчисление функции одной переменной\" (работа)",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Модульное задание \"Интегральное исчисление функции одной переменной\" (оценка)",
                                "mark": "-",
                                "range": "0-3"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "2,0 (5,0 %)",
                                "range": "0-40"
                            },
                            {
                                "name": "Задачи к лекции 1",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Задачи к лекции 2",
                                "mark": "2",
                                "range": "0-2"
                            },
                            {
                                "name": "Задачи к лекции 3",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Задачи к лекции 4",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Задачи к лекции 5",
                                "mark": "-",
                                "range": "0-3"
                            },
                            {
                                "name": "Задачи к лекции 6",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Задачи к лекции 7",
                                "mark": "-",
                                "range": "0-3"
                            },
                            {
                                "name": "Задачи к лекции 8",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Задачи к лекции 9",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Задачи к лекции 10",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Задачи к лекции 11",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Задачи к лекции 12",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Задачи к лекции 13",
                                "mark": "-",
                                "range": "0-3"
                            },
                            {
                                "name": "Задачи к лекции 14",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Задачи к лекции 15",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Задачи к лекции 16",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Задачи к лекции 17",
                                "mark": "-",
                                "range": "0-3"
                            },
                            {
                                "name": "Задачи к лекции 18",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Мини-лекция 5. Непрерывность функции. Точки разрыва",
                                "mark": "-",
                                "range": "0-1"
                            },
                            {
                                "name": "Экзамен",
                                "mark": "-",
                                "range": "неявка-отлично"
                            }
                        ],
                        "eCourseName": "Курс: Математический анализ. Часть 1"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Read. Try to understand the meaning of the words 4. 1.2",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words. 1.1.2",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 4.1.3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words.1.1.3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 4.1.1",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 4.2.1",
                                "mark": "3",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words.1.1.1",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 4.2.2",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 4.2.3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Watch the video. Try to understand the meaning of the words 1.2.1",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 4.3.1",
                                "mark": "7",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 1.2.2",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 1.2.3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 4.3.2",
                                "mark": "8",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 1.3.1",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 4.3.3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 4.4.1",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 4.4.3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 4. 4.2",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 1.3.2",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 4.5.1",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 4.5.2",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 1.3.3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 4.5.3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 4.6.1",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Read and try to understand the meaning of the words 1.4.1",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 4.6.2",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 1.4.3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 4.6.3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "MATH IN IT TEST",
                                "mark": "9",
                                "range": "0-10"
                            },
                            {
                                "name": "Read and try to understand the meaning of the words 1.4.2",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "3D PRINTING TERMS TEST",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Watch the video. Try to understand the meaning of the words. 3. 1.1",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Watch the video. Try to understand the meaning of the words. 3. 3.1",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Watch the video. Try to understand the meaning of the words. 3. 3.3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 2. 2.1",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words. 3. 4.1",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words. 3. 4.2",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words. 3. 4.3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read and try to understand the meaning of the words 1.5.1",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 2. 3.1",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read and try to understand the meaning of the words 1.5.2",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read and try to understand the meaning of the words 1.5.3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read and try to understand the meaning of the words 1.6.1",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read and try to understand the meaning of the words 1.6.2",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read and try to understand the meaning of the words 1.6.3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "BIG DATA TERMS TEST",
                                "mark": "-",
                                "range": "0-14"
                            },
                            {
                                "name": "Watch the video. Try to understand the meaning of the words 2. 1.1",
                                "mark": "-",
                                "range": "0-15"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 2. 1.2",
                                "mark": "-",
                                "range": "0-3"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 2. 1.3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 2. 2.2",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 2. 2.3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 2. 3.2",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 2. 3.3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Watch the video. Try to understand the meaning of the words 2.4.1",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Watch the video. Try to understand the meaning of the words 2.4.2",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 2. 4.3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 2. 5.1",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 2. 5.2",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 2. 5.3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Watch the video. Try to understand the meaning of the words 2. 6.1",
                                "mark": "-",
                                "range": "0-12"
                            },
                            {
                                "name": "Watch the video. Try to understand the meaning of the words 2. 6.2",
                                "mark": "-",
                                "range": "0-12"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words 2. 6.3",
                                "mark": "-",
                                "range": "0-8"
                            },
                            {
                                "name": "THE IOT TERMS TEST",
                                "mark": "-",
                                "range": "0-15"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words. 3. 1.2",
                                "mark": "-",
                                "range": "0-3"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words. 3. 2.2",
                                "mark": "-",
                                "range": "0-5"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words. 3. 2.3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words. 3. 1.3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words. 3. 2.1",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words. 3. 3.2",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words. 3. 5.1",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words. 3. 5.2",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words. 3. 6.2",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words. 3. 6.3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words. 3. 5.3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Read. Try to understand the meaning of the words. 3. 6.1",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Progress test",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "39",
                                "range": "0-877"
                            }
                        ],
                        "eCourseName": "Курс: Профессионально-ориентированный иностранный язык (АЭОК)"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Входное тестирование",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Консультация",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Оценка",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Консультация",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Индивидуальное задание по теме: \"Internet\"",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Индивидуальное задание по теме \"Networks\"",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Индивидуальное задание по теме \"Cloud Computing \"",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Индивидуальное задание по теме \"Spyware\"",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Индивидуальное задание по теме \"The Internet of Things\"",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Индивидуальное задание по теме \"Big Data\"",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Индивидуальное задание по теме \"3D Printing\"",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Индивидуальное задание по теме \"Maths in the University\"",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Проектная работа за 6 семестр",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Проектная работа за 5 семестр",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Индивидуальное задание за 5 семестр",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Индивидуальное задание за 6 семестр",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Индивидуальное задание за 7 семестр",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Проектная работа за 7 семестр",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Тест 1 Internet in our life",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 2 Internet in our life",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 1 Networks",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 1 Cloud Computing",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 1 Spyware",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 2 Spyware",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 1 The Internet of Things",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 2 The Internet of Things",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 2 3D Printing",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 1 3D Printing",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 1 Studying Maths",
                                "mark": "-",
                                "range": "0-37"
                            },
                            {
                                "name": "Тест 2 Studying Maths",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 2 Cloud Computing",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 2 Networks",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 1 Artificial Intelligence",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 1 Computer Facial Animation",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 2 Computer Facial Animation",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 1 Software Development",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 2 Software Development",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 1 Computer Simulation",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 2 Computer Simulation",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 2 Artificial Intelligence",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Индивидуальное задание по теме \"Software Development\"",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Тест 2 Big Data",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест 1 Big Data",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Экзамен",
                                "mark": "-",
                                "range": "Неуд.-Отл."
                            },
                            {
                                "name": "Зачет (5)",
                                "mark": "-",
                                "range": "неявка-зачтено"
                            },
                            {
                                "name": "Контрольная работа ( за 6 семестр )",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Final test 6 (Итоговое тестирование)",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Final Test 5 (Итоговое тестирование)",
                                "mark": "-",
                                "range": "0-64"
                            },
                            {
                                "name": "Контрольная работа ( за 5 семестр )",
                                "mark": "-",
                                "range": "0-64"
                            },
                            {
                                "name": "Final Test 7 (Итоговое тестирование)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Контрольная работа ( за 7 семестр )",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Индивидуальное задание по теме \"Artificial Intelligence\"",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Индивидуальное задание по теме \"Computer Facial Animation\"",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Индивидуальное задание по теме \"Computer Simulation\"",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Зачет (6)",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "-",
                                "range": "0-304"
                            }
                        ],
                        "eCourseName": "Курс: Профессионально-ориентированный иностранный язык"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Placement test",
                                "mark": "9 (88 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Speaking 1",
                                "mark": "65 (65 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Academic Writing. Abstract",
                                "mark": "60 (60 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Homework 1",
                                "mark": "27 (60 %)",
                                "range": "0-45"
                            },
                            {
                                "name": "Checkpoint 1",
                                "mark": "20 (100 %)",
                                "range": "0-20"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "65 (65 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Speaking 2",
                                "mark": "51 (51 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Academic Writing. Annotation",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Students' Project work 1",
                                "mark": "65 (65 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Homework 2",
                                "mark": "37 (86 %)",
                                "range": "0-43"
                            },
                            {
                                "name": "Checkpoint 2",
                                "mark": "16 (80 %)",
                                "range": "0-20"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "74 (47 %)",
                                "range": "51-100"
                            },
                            {
                                "name": "Speaking 3",
                                "mark": "60 (60 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Homework 3",
                                "mark": "26 (100 %)",
                                "range": "0-26"
                            },
                            {
                                "name": "Checkpoint 3",
                                "mark": "18 (90 %)",
                                "range": "0-20"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "83 (83 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Speaking 4",
                                "mark": "67 (67 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Academic Writing. Essay",
                                "mark": "70 (70 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Homework 4",
                                "mark": "10 (100 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Checkpoint 4",
                                "mark": "9 (90 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "82 (82 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Speaking 5",
                                "mark": "70 (70 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Students' project work 2",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Homework 5",
                                "mark": "10 (100 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "63 (63 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Speaking 6",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Academic Writing. Report",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Homework 6",
                                "mark": "9 (93 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Checkpoint 6 (What is Applied mathematics)",
                                "mark": "8 (80 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Discussion 6 - 1",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Discussion 6 - 2",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Classwork 6",
                                "mark": "8 (100 %)",
                                "range": "0-8"
                            },
                            {
                                "name": "Final Test",
                                "mark": "72 (72 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Outside Reading",
                                "mark": "60 (60 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Зачет",
                                "mark": "-",
                                "range": "неявка-зачтено"
                            },
                            {
                                "name": "Экзамен",
                                "mark": "хорошо (75 %)",
                                "range": "неявка-отлично"
                            },
                            {
                                "name": "Lesson 12",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Экзамен",
                                "mark": "4 (71 %)",
                                "range": "1-5"
                            }
                        ],
                        "eCourseName": "Курс: Академический английский язык"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Placement Test",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Письмо. Моя биография",
                                "mark": "зачтено",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Unit 1. Очная аттестация",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Phonetics (Home Work)",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог Unit 1",
                                "mark": "12 (11 %)",
                                "range": "0-112"
                            },
                            {
                                "name": "Письмо. Мой любимый день недели",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Unit 2. Очная аттестация",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Итог Unit 2",
                                "mark": "0 (0 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Письмо. Экскурс в историю создания компьютера в России",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Unit 3. Очная аттестация",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Итог Unit 3",
                                "mark": "0 (0 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Progress test",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Итог Progress test",
                                "mark": "0 (0 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Topical vocabulary. About myself (Home Work)",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Reading. About Myself (Home Work)",
                                "mark": "6",
                                "range": "0-10"
                            },
                            {
                                "name": "Listening. About myself (Home Work)",
                                "mark": "8",
                                "range": "0-10"
                            },
                            {
                                "name": "Use of English. Part A(Home Work)",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Use of English. Part B (Home Work)",
                                "mark": "9",
                                "range": "0-10"
                            },
                            {
                                "name": "Listening. My daily routine (Home Work)",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Topical vocabulary. My daily programme. Basic Vocabluary (Home Work)",
                                "mark": "9",
                                "range": "0-10"
                            },
                            {
                                "name": "Use of English. Part A (Home Work)",
                                "mark": "7",
                                "range": "0-10"
                            },
                            {
                                "name": "Phonetics (Home Work)",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Reading. My daily programme (Home Work)",
                                "mark": "9",
                                "range": "0-10"
                            },
                            {
                                "name": "Use of English. Part B(Home Work)",
                                "mark": "9",
                                "range": "0-10"
                            },
                            {
                                "name": "Phonetics (Home Work)",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Topical vocabulary. History of computers (Home Work)",
                                "mark": "8",
                                "range": "0-10"
                            },
                            {
                                "name": "Reading. History of computers (Home Work)",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Listening. Famous English Mathematician (Home Work)",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Use of English. Part B (Home Work)",
                                "mark": "8",
                                "range": "0-10"
                            },
                            {
                                "name": "Use of English. Part A (Home Work)",
                                "mark": "9",
                                "range": "0-10"
                            },
                            {
                                "name": "Зачет",
                                "mark": "-",
                                "range": "неявка-зачтено"
                            },
                            {
                                "name": "Предварительная оценка за курс",
                                "mark": "27 % (F)",
                                "range": "0-595"
                            }
                        ],
                        "eCourseName": "Курс: Английский язык для начинающих"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Class Work 1",
                                "mark": "9",
                                "range": "0-10"
                            },
                            {
                                "name": "Homework 1",
                                "mark": "9",
                                "range": "0-10"
                            },
                            {
                                "name": "Check point 1",
                                "mark": "12",
                                "range": "0-15"
                            },
                            {
                                "name": "Discussion 1",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "21",
                                "range": "0-25"
                            },
                            {
                                "name": "Check Point 2",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Class Work 2",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Homework 2",
                                "mark": "8",
                                "range": "0-10"
                            },
                            {
                                "name": "Discussion 2",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "37",
                                "range": "0-40"
                            },
                            {
                                "name": "Check Point 3",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Class Work 3",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Homework 3",
                                "mark": "8",
                                "range": "0-10"
                            },
                            {
                                "name": "Discussion 3",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "38",
                                "range": "0-40"
                            },
                            {
                                "name": "Class Work 4",
                                "mark": "22",
                                "range": "0-30"
                            },
                            {
                                "name": "Home Work 4",
                                "mark": "23",
                                "range": "0-37"
                            },
                            {
                                "name": "Discussion 4",
                                "mark": "4",
                                "range": "0-30"
                            },
                            {
                                "name": "Check point 4",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "59",
                                "range": "0-107"
                            },
                            {
                                "name": "Speaking 1",
                                "mark": "80",
                                "range": "0-100"
                            },
                            {
                                "name": "Writing 1",
                                "mark": "80",
                                "range": "0-100"
                            },
                            {
                                "name": "Speaking 2",
                                "mark": "80",
                                "range": "0-100"
                            },
                            {
                                "name": "Writing 2",
                                "mark": "84",
                                "range": "0-100"
                            },
                            {
                                "name": "Project Work",
                                "mark": "50",
                                "range": "0-50"
                            },
                            {
                                "name": "Outside Reading",
                                "mark": "зачтено",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "375",
                                "range": "0-452"
                            },
                            {
                                "name": "Экзамен",
                                "mark": "-",
                                "range": "Неуд.-Отл."
                            },
                            {
                                "name": "Зачет",
                                "mark": "зачтено",
                                "range": "неявка-зачтено"
                            },
                            {
                                "name": "Final test",
                                "mark": "97",
                                "range": "0-100"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "98 % (105)",
                                "range": "0-107"
                            },
                            {
                                "name": "Students' report about the MOOC studied",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Lesson 1",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 2",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 3",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 4",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 5",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 6",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 7",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 8",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 9",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 10",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 11",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 12",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 13",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 14",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 15",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 16",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 17",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-34"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "538",
                                "range": "0-868"
                            }
                        ],
                        "eCourseName": "Курс: Английский язык для профессиональных целей. Весна.\nДисциплина: Профессионально-ориентированный иностранный язык"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Final Test",
                                "mark": "-",
                                "range": "0-98"
                            },
                            {
                                "name": "Pronouns (Personal)",
                                "mark": "-",
                                "range": "0-30"
                            },
                            {
                                "name": "Checkpoint 1 (Personal Pronouns)",
                                "mark": "-",
                                "range": "0-15"
                            },
                            {
                                "name": "Pronouns 2 (Personal)",
                                "mark": "-",
                                "range": "0-39"
                            },
                            {
                                "name": "Checkpoint 2 (Personal Pronouns)",
                                "mark": "-",
                                "range": "0-15"
                            },
                            {
                                "name": "Pronouns 3 (Personal)",
                                "mark": "-",
                                "range": "0-50"
                            },
                            {
                                "name": "Checkpoint 3 (Personal Pronouns)",
                                "mark": "-",
                                "range": "0-25"
                            },
                            {
                                "name": "Pronouns (Object)",
                                "mark": "-",
                                "range": "0-32"
                            },
                            {
                                "name": "Checkpoint 1 (Object Pronouns)",
                                "mark": "-",
                                "range": "0-15"
                            },
                            {
                                "name": "Pronouns 2 (Object)",
                                "mark": "-",
                                "range": "0-38"
                            },
                            {
                                "name": "Checkpoint 2 (Object Pronouns)",
                                "mark": "-",
                                "range": "0-17"
                            },
                            {
                                "name": "Pronouns 3 (Object)",
                                "mark": "-",
                                "range": "0-53"
                            },
                            {
                                "name": "Checkpoint 3 (Object Pronouns)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Pronouns (Possessive)",
                                "mark": "-",
                                "range": "0-26"
                            },
                            {
                                "name": "Checkpoint 1 (Possessive Pronouns)",
                                "mark": "-",
                                "range": "0-16"
                            },
                            {
                                "name": "Pronouns 2 (Possessive)",
                                "mark": "-",
                                "range": "0-43"
                            },
                            {
                                "name": "Checkpoint 2 (Possessive Pronouns)",
                                "mark": "-",
                                "range": "0-12"
                            },
                            {
                                "name": "Pronouns 3 (Possessive)",
                                "mark": "-",
                                "range": "0-34"
                            },
                            {
                                "name": "Checkpoint 3 (Possessive Pronouns)",
                                "mark": "-",
                                "range": "0-12"
                            },
                            {
                                "name": "Word order +",
                                "mark": "-",
                                "range": "0-56"
                            },
                            {
                                "name": "Word order + Checkpoint 1",
                                "mark": "-",
                                "range": "0-15"
                            },
                            {
                                "name": "Word order + 2",
                                "mark": "-",
                                "range": "0-57"
                            },
                            {
                                "name": "Word order + Checkpoint 2",
                                "mark": "-",
                                "range": "0-15"
                            },
                            {
                                "name": "Word order + 3",
                                "mark": "-",
                                "range": "0-57"
                            },
                            {
                                "name": "Word order + Checkpoint 3",
                                "mark": "-",
                                "range": "0-15"
                            },
                            {
                                "name": "Word order -",
                                "mark": "-",
                                "range": "0-57"
                            },
                            {
                                "name": "Word order - Checkpoint 1",
                                "mark": "-",
                                "range": "0-15"
                            },
                            {
                                "name": "Word order - 2",
                                "mark": "-",
                                "range": "0-54"
                            },
                            {
                                "name": "Word order - Checkpoint 2",
                                "mark": "-",
                                "range": "0-15"
                            },
                            {
                                "name": "Word order - 3",
                                "mark": "-",
                                "range": "0-57"
                            },
                            {
                                "name": "Word order - Checkpoint 3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Word order ?",
                                "mark": "-",
                                "range": "0-56"
                            },
                            {
                                "name": "Word order ? Checkpoint 1",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Word order ? 2",
                                "mark": "-",
                                "range": "0-55"
                            },
                            {
                                "name": "Word order ? Checkpoint 2",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Word order ? 3",
                                "mark": "-",
                                "range": "0-56"
                            },
                            {
                                "name": "Word order ? Checkpoint 3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Placement Test",
                                "mark": "-",
                                "range": "0-120"
                            },
                            {
                                "name": "Nouns",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Nouns Checkpoint 1",
                                "mark": "-",
                                "range": "0-34"
                            },
                            {
                                "name": "Nouns 2",
                                "mark": "-",
                                "range": "0-73"
                            },
                            {
                                "name": "Nouns Checkpoint 2",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Nouns 3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Nouns Checkpoint 3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Numerals",
                                "mark": "-",
                                "range": "0-51"
                            },
                            {
                                "name": "Checkpoint 1 (Numerals)",
                                "mark": "-",
                                "range": "0-30"
                            },
                            {
                                "name": "Numerals 2",
                                "mark": "-",
                                "range": "0-53"
                            },
                            {
                                "name": "Checkpoint 2 (Numerals)",
                                "mark": "-",
                                "range": "0-30"
                            },
                            {
                                "name": "Numerals 3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Checkpoint 3 (Numerals)",
                                "mark": "-",
                                "range": "0-30"
                            },
                            {
                                "name": "Telling the time",
                                "mark": "-",
                                "range": "0-25"
                            },
                            {
                                "name": "Checkpoint 1 (Time)",
                                "mark": "-",
                                "range": "0-9"
                            },
                            {
                                "name": "Telling the time 2",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Checkpoint 2 (Time)",
                                "mark": "-",
                                "range": "0-9"
                            },
                            {
                                "name": "Telling the time 3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Checkpoint 3 (Time)",
                                "mark": "-",
                                "range": "0-9"
                            },
                            {
                                "name": "Present Continuous",
                                "mark": "-",
                                "range": "0-52"
                            },
                            {
                                "name": "Checkpoint 1 (Pr. Continuous)",
                                "mark": "-",
                                "range": "0-31"
                            },
                            {
                                "name": "Present Continuous 2",
                                "mark": "-",
                                "range": "0-59"
                            },
                            {
                                "name": "Checkpoint 2 (Pr. Continuous)",
                                "mark": "-",
                                "range": "0-33"
                            },
                            {
                                "name": "Present Continuous 3",
                                "mark": "-",
                                "range": "0-42"
                            },
                            {
                                "name": "Checkpoint 3 (Pr. Continuous)",
                                "mark": "-",
                                "range": "0-30"
                            },
                            {
                                "name": "Past Simple",
                                "mark": "-",
                                "range": "0-51"
                            },
                            {
                                "name": "Checkpoint 1 (Past Simple)",
                                "mark": "-",
                                "range": "0-40"
                            },
                            {
                                "name": "Past Simple 2",
                                "mark": "-",
                                "range": "0-50"
                            },
                            {
                                "name": "Checkpoint 2 (Past Simple)",
                                "mark": "-",
                                "range": "0-41"
                            },
                            {
                                "name": "Past Simple 3",
                                "mark": "-",
                                "range": "0-50"
                            },
                            {
                                "name": "Checkpoint 3 (Past Simple)",
                                "mark": "-",
                                "range": "0-41"
                            },
                            {
                                "name": "Future",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Checkpoint 1 (Future)",
                                "mark": "-",
                                "range": "0-31"
                            },
                            {
                                "name": "Future 2",
                                "mark": "-",
                                "range": "0-44"
                            },
                            {
                                "name": "Checkpoint 2 (Future)",
                                "mark": "-",
                                "range": "0-31"
                            },
                            {
                                "name": "Future 3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Checkpoint 3 (Future)",
                                "mark": "-",
                                "range": "0-24"
                            },
                            {
                                "name": "Adjectives",
                                "mark": "-",
                                "range": "0-61"
                            },
                            {
                                "name": "Checkpoint 1 (Adjectives)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Adjectives 2",
                                "mark": "-",
                                "range": "0-60"
                            },
                            {
                                "name": "Checkpoint 2 (Adjectives)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Adjectives 3",
                                "mark": "-",
                                "range": "0-61"
                            },
                            {
                                "name": "Checkpoint 3 (Adjectives)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Quantifiers",
                                "mark": "-",
                                "range": "0-32"
                            },
                            {
                                "name": "Checkpoint 1 (Quantifiers)",
                                "mark": "-",
                                "range": "0-15"
                            },
                            {
                                "name": "Quantifiers 2",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Checkpoint 2 (Quantifiers)",
                                "mark": "-",
                                "range": "0-15"
                            },
                            {
                                "name": "Quantifiers 3",
                                "mark": "-",
                                "range": "0-32"
                            },
                            {
                                "name": "Checkpoint 3 (Quantifiers)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Auxiliary verb 'to be'",
                                "mark": "-",
                                "range": "0-43"
                            },
                            {
                                "name": "Auxiliary verb 'to be' Checkpoint 1",
                                "mark": "-",
                                "range": "0-15"
                            },
                            {
                                "name": "Auxilary verb 'to be' 2",
                                "mark": "-",
                                "range": "0-42"
                            },
                            {
                                "name": "Auxiliary verb 'to be' Checkpoint 2",
                                "mark": "-",
                                "range": "0-15"
                            },
                            {
                                "name": "Auxiliary verb 'to be' 3",
                                "mark": "-",
                                "range": "0-54"
                            },
                            {
                                "name": "Auxiliary verb 'to be' Checkpoint 3",
                                "mark": "-",
                                "range": "0-15"
                            },
                            {
                                "name": "Auxiliary verb 'to have'",
                                "mark": "-",
                                "range": "0-45"
                            },
                            {
                                "name": "Auxiliary verb 'to have' Checkpoint 1",
                                "mark": "-",
                                "range": "0-15"
                            },
                            {
                                "name": "Auxiliary verb 'to have' 2",
                                "mark": "-",
                                "range": "0-45"
                            },
                            {
                                "name": "Auxiliary verb 'to have' Checkpoint 2",
                                "mark": "-",
                                "range": "0-15"
                            },
                            {
                                "name": "Auxiliary verb 'to have' 3",
                                "mark": "-",
                                "range": "0-34"
                            },
                            {
                                "name": "Auxiliary verb 'to have' Checkpoint 3",
                                "mark": "-",
                                "range": "0-15"
                            },
                            {
                                "name": "Present Simple",
                                "mark": "-",
                                "range": "0-24"
                            },
                            {
                                "name": "Checkpoint 1 (Present Simple)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Present Simple 2",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Checkpoint 2 (Present Simple)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Present Simple 3",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Checkpoint 3 (Present Simple)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "51-100"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "0 (0 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Lesson 2",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 3",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 4",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 5",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 6",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 7",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 8",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 9",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 10",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 11",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 12",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson13",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 14",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 15",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 16",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 17",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 18",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "0 (0 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "1 (0 %)",
                                "range": "0-303"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "53 (1 %)",
                                "range": "0-4210"
                            }
                        ],
                        "eCourseName": "Курс: Иностранный язык (в рамках унифицированной рабочей программы - Часть 1)"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Lesson 1: Reading and Vocabulary. British Housing",
                                "mark": "33 (83 %)",
                                "range": "0-40"
                            },
                            {
                                "name": "Homework 1: British Housing",
                                "mark": "18 (72 %)",
                                "range": "0-25"
                            },
                            {
                                "name": "Lesson 2: Reading and Writing. City vs. Country",
                                "mark": "-",
                                "range": "0-55"
                            },
                            {
                                "name": "Homework 2: City vs. Country",
                                "mark": "17 (61 %)",
                                "range": "0-28"
                            },
                            {
                                "name": "Lesson 3: Listening and Speaking. Public Transport",
                                "mark": "-",
                                "range": "0-51"
                            },
                            {
                                "name": "Homework 3: Public Transport",
                                "mark": "10 (100 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Lesson 4: Reading and Speaking. Cities and Climate",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Homework 4: Cities and Climate",
                                "mark": "10 (95 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Lesson 5: Listening and Speaking. Pollution",
                                "mark": "-",
                                "range": "0-52"
                            },
                            {
                                "name": "Homework 5: Pollution",
                                "mark": "28 (80 %)",
                                "range": "0-35"
                            },
                            {
                                "name": "Lesson 6: Reading and Vocabulary. Ecology Problems",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Homework 6: Ecology Problems",
                                "mark": "8 (82 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Vocabulary test 3",
                                "mark": "30 (100 %)",
                                "range": "0-30"
                            },
                            {
                                "name": "Writing. City or Country Life?",
                                "mark": "74 (74 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Speaking. My Native Town",
                                "mark": "78 (78 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Vocabulary (orally)",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Writing an essay on topic:\"City or country life\" (peer assessment version) (работа)",
                                "mark": "-",
                                "range": "0-50"
                            },
                            {
                                "name": "Writing an essay on topic:\"City or country life\" (peer assessment version) (оценка)",
                                "mark": "-",
                                "range": "0-50"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "306 (46 %)",
                                "range": "0-668"
                            },
                            {
                                "name": "Lesson 1. Reading and Speaking. History of travelling",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Homework 1: History of travelling",
                                "mark": "10 (95 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Lesson 2: Vocabulary and Reading. Means of transport",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Homework 2: Means of transport",
                                "mark": "24 (81 %)",
                                "range": "0-30"
                            },
                            {
                                "name": "Lesson 3: Listening and Speaking. Backpacking",
                                "mark": "-",
                                "range": "0-29"
                            },
                            {
                                "name": "Lesson 3 (Variant 2) Listening and Speaking. Backpacking",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Homework 3: Backpacking",
                                "mark": "9 (90 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Lesson 4. Listening and Vocabulary. Staying in a hotel",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Homework 4: Staying in a hotel",
                                "mark": "10 (97 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Lesson 5: Listening and Speaking. Ecotourism",
                                "mark": "43 (95 %)",
                                "range": "0-45"
                            },
                            {
                                "name": "Homework 5: Ecotourism",
                                "mark": "15 (100 %)",
                                "range": "0-15"
                            },
                            {
                                "name": "Lesson 6. Reading and Speaking. Travelling might be dangerous",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Homework 6: Travelling might be dangerous",
                                "mark": "8 (77 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Vocabulary test 4",
                                "mark": "7 (70 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Vocabulary2 (orally)",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Writing. A Letter to My Friend",
                                "mark": "85 (85 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Speaking. The Country (City) I Would Like to Visit",
                                "mark": "80 (80 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "290 (69 %)",
                                "range": "0-419"
                            },
                            {
                                "name": "Country Corner: Reading and Vocabulary. Social class system in Britain.",
                                "mark": "-",
                                "range": "0-55"
                            },
                            {
                                "name": "Country Corner. Homework: Social class system in Britain",
                                "mark": "-",
                                "range": "0-35"
                            },
                            {
                                "name": "Students' Project work",
                                "mark": "65 (65 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Semester Test",
                                "mark": "82 (82 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Работа над ошибками. Semester Test",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Зачет",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-0"
                            },
                            {
                                "name": "Workbook. Travelling",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Lesson 2",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 3",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 4",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 5",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 6",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 7",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 8",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 9",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 10",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 11",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 12",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 13",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 14",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 15",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 16",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 17",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 18",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "E-workbook Unit 3",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "743 (40 %)",
                                "range": "0-1853"
                            }
                        ],
                        "eCourseName": "Курс: Иностранный язык (в рамках унифицированной рабочей программы - Часть 2)"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Homework 1",
                                "mark": "9 (93 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Homework 2",
                                "mark": "9 (93 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Homework 3",
                                "mark": "5 (46 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Homework 4.",
                                "mark": "10 (95 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Homework 5.",
                                "mark": "6 (64 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Homework Canada",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Vocabulary test 7",
                                "mark": "30 (100 %)",
                                "range": "0-30"
                            },
                            {
                                "name": "Writing 7. Overpriced laptops are a waste of money.",
                                "mark": "70 (70 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Speaking 7. The Computer I Dream About",
                                "mark": "70 (70 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Lesson 1. Vocabulary and Speaking. Basic parts and functionality of the computer",
                                "mark": "10 (100 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Lesson 2. Reading and Speaking. Parts of a Motherboard and Their Function",
                                "mark": "10 (100 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Lesson 3. Vocabulary and Reading. CPU Memory or Cache Memory.",
                                "mark": "10 (100 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Lesson 4. Listening. Computer components",
                                "mark": "10 (100 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Lesson 5. Use of English. Sequence of Tenses. Reported Speech.",
                                "mark": "5 (51 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Country study. Canada",
                                "mark": "10 (98 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Writing 7. Overpiced laptops are a waste of money.(Peer assessment version) (работа)",
                                "mark": "-",
                                "range": "0-50"
                            },
                            {
                                "name": "Writing 7. Overpiced laptops are a waste of money.(Peer assessment version) (оценка)",
                                "mark": "-",
                                "range": "0-50"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "268 (59 %)",
                                "range": "0-450"
                            },
                            {
                                "name": "Homework 1",
                                "mark": "10 (100 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Homework 2",
                                "mark": "10 (100 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Homework 3",
                                "mark": "10 (100 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Homework 4",
                                "mark": "21 (100 %)",
                                "range": "0-21"
                            },
                            {
                                "name": "Homework 5",
                                "mark": "10 (100 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Homework 6",
                                "mark": "10 (100 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Vocabulary test 8",
                                "mark": "10 (100 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Speaking 8.The Future of IT",
                                "mark": "70 (70 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Writing 8. Artificial Intelligence: a Friend or a Foe?",
                                "mark": "84 (84 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Students' Project Work. Famous people in the history of computers.",
                                "mark": "78 (78 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Semester Test 4",
                                "mark": "83 (83 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Работа над ошибками. Semester Test 4",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Lesson 1. Vocabulary and Reading. History of Computers",
                                "mark": "8 (81 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Lesson 2. Grammar. Gerund and Infinitive",
                                "mark": "10 (99 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Lesson 3. Listening and Speaking. Computer Problems",
                                "mark": "10 (100 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Lesson 4. Listening and Reading. Future of Computers",
                                "mark": "27 (96 %)",
                                "range": "0-28"
                            },
                            {
                                "name": "Lesson 5. Reading and Speaking. Public Speaking",
                                "mark": "7 (74 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Lesson 6. Listening and Speaking. Professional Skills",
                                "mark": "7 (71 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Экзамен",
                                "mark": "-",
                                "range": "неявка-отлично"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "465 (70 %)",
                                "range": "0-664"
                            },
                            {
                                "name": "Lesson 1",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 2",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 3",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 4",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 5",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 6",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 7",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 8",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 9",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 10",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 11",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 12",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 13",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 14",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 15",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 16",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 17",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Lesson 18",
                                "mark": "-",
                                "range": "н-п"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "н",
                                "range": "н-п"
                            },
                            {
                                "name": "E-Workbook Unit 7",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "E-Workbook. Unit 8",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "734 (56 %)",
                                "range": "0-1316"
                            }
                        ],
                        "eCourseName": "Курс: Иностранный язык (в рамках унифицированной рабочей программы - Часть 4)"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Placement test",
                                "mark": "4 (37 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Internet (Classwork)",
                                "mark": "9 (90 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Internet (Discussion)",
                                "mark": "10 (100 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Internet (Homework)",
                                "mark": "8 (75 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Internet (Checkpoint)",
                                "mark": "9 (87 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "88 (88 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Networks (Homework)",
                                "mark": "5 (49 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Networks (Classwork)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Networks (Checkpoint)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Networks (Discussion)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "12 (12 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Writing 1",
                                "mark": "80 (80 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Speaking 1",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Cloud Computing (Classwork)",
                                "mark": "10 (100 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Cloud Computing (Homework)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Cloud Computing (Checkpoint)",
                                "mark": "6 (55 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Cloud Computing (Discussion)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "39 (39 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Spyware (Discussion)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Spyware (Classwork)",
                                "mark": "9 (94 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Spyware (Check Point)",
                                "mark": "10 (100 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Spyware (Homework)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "19 (49 %)",
                                "range": "0-40"
                            },
                            {
                                "name": "Speaking 2",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Writing 2",
                                "mark": "80 (80 %)",
                                "range": "0-100"
                            },
                            {
                                "name": "Hacking (Homework)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Hacking (Class Work)",
                                "mark": "9 (94 %)",
                                "range": "0-10"
                            },
                            {
                                "name": "Hacking (Check point)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Hacking (Discussion)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "9 (23 %)",
                                "range": "0-40"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-30"
                            },
                            {
                                "name": "Grammar. Gerund and Infinitive. (Class Work)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-70"
                            },
                            {
                                "name": "Final Test",
                                "mark": "48 (75 %)",
                                "range": "0-64"
                            },
                            {
                                "name": "Project Work",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Outside Reading",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Зачет",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "48 (29 %)",
                                "range": "0-168"
                            },
                            {
                                "name": "E-Workbook. Networks",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "379 (25 %)",
                                "range": "0-1490"
                            }
                        ],
                        "eCourseName": "Курс: Английский язык для профессиональных целей. Осень"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Практическое задание №1",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическое задание №2",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическое задание №3",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическое задание №4",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическое задание №5",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическое задание №6",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическое задание №7",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическое задание №8",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическое задание №9",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическое задание №10",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическое задание №11",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическое задание №12",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическое задание №13",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Итоговая самостоятельная работа",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "1100",
                                "range": "0-1400"
                            }
                        ],
                        "eCourseName": "Курс: BI DeepSee"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Практическое задание №2",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическое задание №3",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическое задание №4",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическое задание №5",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическое задание №6",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Практическое задание №1",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Входное тестирование",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "600",
                                "range": "0-610"
                            }
                        ],
                        "eCourseName": "Курс: Разработка интернет приложений на CSP и ZEN"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Тест по теме \"Биосфера\"",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "контрольная работа",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Контрольная работа \"Биосфера\"",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Прочитать статью Г.С.Розенберга и ответить на вопросы: 1- как много определений понятия \"экология\" существует на данный момент?; 2 - На какие группы эти определения разделены? -",
                                "mark": "-",
                                "range": "0-2"
                            },
                            {
                                "name": "Среды жизни. Организм в окружающей среде",
                                "mark": "4",
                                "range": "0-4"
                            },
                            {
                                "name": "Биосфера",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Среды жизни. Организм и среда",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Сообщество и экосистема",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Итоговый тест",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Подобрать примеры по типам взаимоотношений видов в экосистемах",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "прочитать статью http://polit. ru/tag/biodiversity/",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Что изучает современная экология?",
                                "mark": "100",
                                "range": "0-100"
                            },
                            {
                                "name": "Популяция и ее свойства",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Демография.",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Лекция 1",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Лекция",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Лекция 7",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Лекция 3",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "контрольный срез",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Экозащитная техника и технологии.",
                                "mark": "25",
                                "range": "0-100"
                            },
                            {
                                "name": "Загрязнение биосферы",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Окружающая среда и здоровье человека",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Организм в окружающей среде. Важнейшие экологические факторы и адаптация к ним организмов",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Загрязнение биосферы",
                                "mark": "9",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест Окружающая среда и здоровье человека",
                                "mark": "7",
                                "range": "0-10"
                            },
                            {
                                "name": "Биоразнообразие",
                                "mark": "83",
                                "range": "0-100"
                            },
                            {
                                "name": "Земельные ресурсы",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Круговорот вещества в экосистемах",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "ЭКОЛОГИЧЕСКОЕ ПРАВО",
                                "mark": "67",
                                "range": "0-100"
                            },
                            {
                                "name": "Тест по теме \"Круговорот вещества в экосистемах\"",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Тест по теме \"Земельные ресурсы\"",
                                "mark": "10",
                                "range": "0-10"
                            },
                            {
                                "name": "Вопросы к лекции «Круговорот веществ в экосистемах»",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Вопросы к лекции \"Земельные ресурсы\"",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Климат и погода",
                                "mark": "40",
                                "range": "0-100"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "76",
                                "range": "0-100"
                            }
                        ],
                        "eCourseName": "Курс: Экология (О.В.Тарасова)"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Аудио-отчеты (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Chat Battles Records (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Offer Your Sample (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Конкурс шуточного ПО имени программы Паскакаль - с 01.04.2019 (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Конкурс коротких консольных программ методологии KISS - с 01.10.2019 (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Аудио-отчеты (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Speaking Records (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "On Duty for Text Analysis (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Аудио-отчеты (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Chat Battles Records (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Offer Your Sample (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Конкурс шуточного ПО имени программы Паскакаль - с 01.04.2019 (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Конкурс коротких консольных программ методологии KISS - с 01.10.2019 (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Speaking Records (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "On Duty for Text Analysis (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Предложить свой текст для работы - курс 1 (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Обязательно сохранять в этот архив просматриваемые шаблоны с GitHub и др. (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Операционные системы, оболочки, гипервайзеры, виртуальные машины, эмуляторы и пр. (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Аудио-отчеты (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Программы для работы с текстом (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Программы для работы с изображением (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Свободный проект (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Программы для работы со звуком (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Программы для работы с данными (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Программы для работы с программным кодом (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Предложить свой текст для работы - курс 2 (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Конверторы, трансляторы, архиваторы, программы шифрования (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Программы по математическому моделированию (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Предложить свой текст для работы - курс 3 (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Разработка интерфейсов, ЕЯИ и моделирования ЕЯ (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Программы по математическому моделированию (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Программы для работы в сетях, включая Интернет (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Предложить свой текст для работы - магистранты (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Программы диагностики и поддержки аппаратного обеспечения (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Базы данных, словари, СУБД (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Узкоспециальные программы (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "База студенческих портфолио (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Иное (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Предложить свой текст для работы - аспиранты (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Свободный проект (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Предложить свой текст для работы - инженеры-переводчики (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Progress Report Starting (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Progress Report November (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Progress Report December (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Предложить свой текст для работы - курс 4 (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "База студенческих портфолио (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Предложить свой текст для работы - магистранты (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "База студенческих портфолио (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Итоговые материалы для работы (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Свободный проект (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "База студенческих портфолио (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Требования к устной сдаче",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Предложить свой текст для работы - аспиранты (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Предложить свой текст для работы - инженеры-переводчики (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Предложить свой текст для работы - магистранты (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Предложить свой текст для работы - аспиранты (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Предложить свой текст для работы - инженеры-переводчики (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Progress Report Starting (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Progress Report November (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Progress Report December (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Предложить свой текст для работы - курс 1 (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Свободный проект (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Предложить свой текст для работы - курс 2 (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Предложить свой текст для работы - курс 3 (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "База студенческих портфолио (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Предложить свой текст для работы - курс 4 (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Обязательно сохранять в этот архив просматриваемые шаблоны с GitHub и др. (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "База студенческих портфолио (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Операционные системы, оболочки, гипервайзеры, виртуальные машины, эмуляторы и пр. (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Предложить свой текст для работы - магистранты (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Программы для работы с текстом (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Программы для работы с изображением (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Программы для работы со звуком (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Программы для работы с данными (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "База студенческих портфолио (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Программы для работы с программным кодом (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Предложить свой текст для работы - аспиранты (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Конверторы, трансляторы, архиваторы, программы шифрования (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "База студенческих портфолио (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Программы по математическому моделированию (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Предложить свой текст для работы - инженеры-переводчики (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Разработка интерфейсов, ЕЯИ и моделирования ЕЯ (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Программы по математическому моделированию (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Программы для работы в сетях, включая Интернет (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Программы диагностики и поддержки аппаратного обеспечения (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Базы данных, словари, СУБД (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Узкоспециальные программы (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Итоговые материалы для работы (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Иное (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Предложить свои материалы к курсу (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Предложить свои материалы к курсу (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "-",
                                "range": "0-100"
                            }
                        ],
                        "eCourseName": "Курс: Mультиметодический курс: Marketing Lingvоinformatics (Пароль: guest)"
                    },
                    {
                        "eCourses": [
                            {
                                "name": "Placement Test",
                                "mark": "-",
                                "range": "0-200"
                            },
                            {
                                "name": "Phonetics",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Unit 1. Reading. My British friend (Home work)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Unit 1. Listening. About Myself (Home work)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Unit 1. Use of English. Part A (Home work)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Unit 1. Use of English. Part B (Home work)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Unit 1. Очная аттестация",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Unit 1. Topical vocabulary. About Myself (Home work)",
                                "mark": "-",
                                "range": "0-30"
                            },
                            {
                                "name": "Writing \"Finding a pen-friend\"",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Unit 2. Phonetics (Home work)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Unit 2. Topical vocabulary. My daily programme (Home work)",
                                "mark": "-",
                                "range": "0-15"
                            },
                            {
                                "name": "Unit 2. Reading. My daily programme (Home work)",
                                "mark": "-",
                                "range": "0-30"
                            },
                            {
                                "name": "Unit 2. Listening. My daily routine (Home work)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Unit 2. Use of English. My daily programme. Part A (Home work)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Unit 2. Use of English. My daily programme. Part B (Home work)",
                                "mark": "-",
                                "range": "0-30"
                            },
                            {
                                "name": "Unit 2. Country study. The UK",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Writing. My favourite day of the week",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Unit 2. Очная аттестация",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Progress test 1",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Results. Country study and Project Work. Celebrating Christmas",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Unit 4. Очная аттестация",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Unit 3. Очная аттестация",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Speaking. Famous people in the history of computers",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Writing. History of Computers in Russia",
                                "mark": "-",
                                "range": "не зачтено-зачтено"
                            },
                            {
                                "name": "Speaking",
                                "mark": "",
                                "range": "-"
                            },
                            {
                                "name": "Unit 3. Phonetics (Home work)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Unit 3. Topical vocabulary. History of Computers (Home work)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Unit 3. Reading. 'History of Computers' (Home work)",
                                "mark": "-",
                                "range": "0-30"
                            },
                            {
                                "name": "Unit 3. Listening. Famous English Mathematician (Home work)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Unit 3. Use of English. Present Perfect Active/Passive (Home work)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Unit 3. Use of English. Past Perfect Active / Passive (Home work)",
                                "mark": "-",
                                "range": "0-10"
                            },
                            {
                                "name": "Оценки текущих минипроектов (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Оценки текущих минипроектов (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Оценки текущих минипроектов (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Оценки текущих минипроектов (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Архивы студенческих портфолио (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Оценки текущих минипроектов (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Оценки текущих минипроектов (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Оценки текущих минипроектов (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Оценки текущих минипроектов (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Архивы студенческих портфолио (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Результаты файловых проектов",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Lesson 1",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 2",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 3",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 4",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 5",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 6",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 7",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 8",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 9",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 10",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 11",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 12",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 13",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 14",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 15",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 16",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 17",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 18",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 19",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 20",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 21",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 22",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 23",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 24",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Lesson 25",
                                "mark": "-",
                                "range": "Ошибка-Ошибка"
                            },
                            {
                                "name": "Итог категории",
                                "mark": "-",
                                "range": "0-100"
                            },
                            {
                                "name": "Финальные проекты (работа)",
                                "mark": "-",
                                "range": "0-80"
                            },
                            {
                                "name": "Финальные проекты (оценка)",
                                "mark": "-",
                                "range": "0-20"
                            },
                            {
                                "name": "Зачет",
                                "mark": "-",
                                "range": "неявка-зачтено"
                            },
                            {
                                "name": "Итоговая оценка за курс",
                                "mark": "-",
                                "range": "0-100"
                            }
                        ],
                        "eCourseName": "Курс: Иностранный язык (в рамках стандартов CDIO - Часть 1)"
                    }
                ]
            }
        )
}

async function getECourse(driver, eCourses, check) {
    while (check) {
        try {
            let progress = new Progress();

            let id = await driver.findElement(Webdriver.By.css(".course_journal_title.up_arrow")).getAttribute("id");
            progress.setECourseName = await driver.findElement(Webdriver.By.css(".course_journal_title.up_arrow")).getText();

            check = !check;
            try {
                for (let i = 2; true; i++) {
                    let ecourse = new ECourseTask();
                    ecourse.setName = await driver.findElement(Webdriver.By.css("#j_" + id + " > table > tbody > tr:nth-child(" + i + ") > td.name")).getText();
                    ecourse.setMark = await driver.findElement(Webdriver.By.css("#j_" + id + " > table > tbody > tr:nth-child(" + i + ") > td.grade")).getText();
                    ecourse.setRange = await driver.findElement(Webdriver.By.css("#j_" + id + " > table > tbody > tr:nth-child(" + i + ") > td.range")).getText();
                    progress.getECourses.push(ecourse);
                }
            } catch (e) {
                eCourses.push(progress)
            }
        } catch (e) {
        }
    }
    return eCourses;
}
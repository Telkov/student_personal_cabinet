package com.truecoders.cabinet.Model.General;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.truecoders.cabinet.Model.Entrant.*;
import com.truecoders.cabinet.Model.Student.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ilya Baykalov
 */

public class BaseClass {

    @SerializedName("status")
    @Expose
    private String status;

    //General
    @SerializedName("news")
    @Expose
    private List<News> news = null;

    // Student
    @SerializedName("profile")
    @Expose
    private Profile profile;

    @SerializedName("possibleGroups")
    @Expose
    private List<PossibleGroup> possibleGroups = null;
    @SerializedName("week")
    @Expose
    private Integer week;
    @SerializedName("schedule")
    @Expose
    private List<Schedule> schedule = null;

    @SerializedName("recordBook")
    @Expose
    private RecordBook recordBook;

    @SerializedName("eCoursesList")
    @Expose
    private List<ECoursesList> eCoursesList = null;

    @SerializedName("arragment")
    @Expose
    private List<Arragment> arragment = null;

    @SerializedName("classmates")
    @Expose
    private List<Profile> classmates = null;

    // Entrant
    @SerializedName("contacts")
    @Expose
    private List<Contact> contacts = null;

    @SerializedName("hostels")
    @Expose
    private List<Hostel> hostels = null;

    @SerializedName("institutes")
    @Expose
    private List<Institute> institutes = null;

    @SerializedName("majors")
    @Expose
    private List<Major> majors = null;

    @SerializedName("markers")
    @Expose
    private List<Marker> markers = null;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    //General
    public List<News> getNews() {
        return news;
    }

    // Student
    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public List<PossibleGroup> getPossibleGroups() {
        return possibleGroups;
    }

    public Integer getWeek() {
        return week;
    }

    public List<Schedule> getSchedule() {
        return schedule;
    }

    public RecordBook getRecordBook() {
        return recordBook;
    }

    public List<Arragment> getArragment() {return arragment;};

    public List<ECoursesList> getECoursesList() {
        return eCoursesList;
    }

    public List<Profile> getClassmates() {
        return classmates;
    }

    // Entrant
    public List<Contact> getContacts() {
        return contacts;
    }

    public List<Hostel> getHostels() {
        return hostels;
    }

    public List<Institute> getInstitutes() {
        return institutes;
    }

    public List<Major> getMajors() {
        return majors;
    }

    public List<Marker> getMarkers() {
        return markers;
    }
}
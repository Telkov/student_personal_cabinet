package com.truecoders.cabinet.Model.Entrant;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import io.realm.RealmList;
import io.realm.RealmObject;

public class Major extends RealmObject {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("levelOfEducation")
    @Expose
    private String levelOfEducation;
    @SerializedName("majorForm")
    @Expose
    private String majorForm;
    @SerializedName("budgetaryPlace")
    @Expose
    private Integer budgetaryPlace;
    @SerializedName("specialPlace")
    @Expose
    private Integer specialPlace;
    @SerializedName("paidPlaces")
    @Expose
    private Integer paidPlaces;
    @SerializedName("cost")
    @Expose
    private Integer cost;
    @SerializedName("averagePoint")
    @Expose
    private Double averagePoint;
    @SerializedName("entrancePoint")
    @Expose
    private Double entrancePoint;
    @SerializedName("internal")
    @Expose
    private String internal;
    @SerializedName("subjects")
    @Expose
    private RealmList<RequireSubject> subjects = null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLevelOfEducation() {
        return levelOfEducation;
    }

    public void setLevelOfEducation(String levelOfEducation) {
        this.levelOfEducation = levelOfEducation;
    }

    public String getMajorForm() {
        return majorForm;
    }

    public void setMajorForm(String majorForm) {
        this.majorForm = majorForm;
    }

    public Integer getBudgetaryPlace() {
        return budgetaryPlace;
    }

    public void setBudgetaryPlace(Integer budgetaryPlace) {
        this.budgetaryPlace = budgetaryPlace;
    }

    public Integer getSpecialPlace() {
        return specialPlace;
    }

    public void setSpecialPlace(Integer specialPlace) {
        this.specialPlace = specialPlace;
    }

    public Integer getPaidPlaces() {
        return paidPlaces;
    }

    public void setPaidPlaces(Integer paidPlaces) {
        this.paidPlaces = paidPlaces;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public Double getAveragePoint() {
        return averagePoint;
    }

    public void setAveragePoint(Double averagePoint) {
        this.averagePoint = averagePoint;
    }

    public Double getEntrancePoint() {
        return entrancePoint;
    }

    public void setEntrancePoint(Double entrancePoint) {
        this.entrancePoint = entrancePoint;
    }

    public String getInternal() {
        return internal;
    }

    public void setInternal(String internal) {
        this.internal = internal;
    }

    public RealmList<RequireSubject> getSubjects() {
        return subjects;
    }
}

package com.truecoders.cabinet.Model.Student;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import io.realm.RealmObject;

public class ECourse extends RealmObject {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("mark")
    @Expose
    private String mark;
    @SerializedName("range")
    @Expose
    private String range;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }
}
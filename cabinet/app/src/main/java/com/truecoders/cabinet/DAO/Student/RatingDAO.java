package com.truecoders.cabinet.DAO.Student;

import com.truecoders.cabinet.Model.Student.ECoursesList;
import com.truecoders.cabinet.Model.Student.RecordBook;
import io.realm.Realm;

import java.util.List;

/**
 * @author Ilya Baykalov
 */

public class RatingDAO {

    public static void addRecordBookToRealm(RecordBook recordBook) {
        if (Realm.getDefaultInstance().where(RecordBook.class).count() > 0)
            Realm.getDefaultInstance().delete(RecordBook.class);
        Realm.getDefaultInstance().executeTransaction(realm -> {
            RecordBook realmObject = realm.createObject(RecordBook.class);

            if (realmObject != null) {
                realmObject.getSemesterList().addAll(recordBook.getSemesterList());
            }
        });
    }

    public static void addProgressToRealm(List<ECoursesList> eCoursesList) {
        if (Realm.getDefaultInstance().where(ECoursesList.class).count() > 0)
            Realm.getDefaultInstance().delete(ECoursesList.class);
        Realm.getDefaultInstance().executeTransaction(realm -> {
            for (ECoursesList eCourses : eCoursesList) {
                ECoursesList realmObject = realm.createObject(ECoursesList.class);

                if (realmObject != null) {
                    realmObject.getECourses().addAll(eCourses.getECourses());
                    realmObject.setECourseName(eCourses.getECourseName());
                }
            }
        });
    }
}

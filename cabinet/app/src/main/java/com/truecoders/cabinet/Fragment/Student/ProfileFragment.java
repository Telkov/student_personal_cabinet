package com.truecoders.cabinet.Fragment.Student;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.truecoders.cabinet.Model.Student.Profile;
import com.truecoders.cabinet.R;
import com.truecoders.cabinet.Utils.CircleTransform;
import io.realm.Realm;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * @author Ilya Baykalov
 */

public class ProfileFragment extends Fragment {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ImageView btnAvatar = view.findViewById(R.id.profile_image);

        Profile profile;
        if (getArguments() != null && getArguments().getSerializable("profile") != null){
            profile = (Profile) getArguments().getSerializable("profile");
        }else {
            profile = Realm.getDefaultInstance().where(Profile.class).findFirst();
        }
        if (profile != null) {

            //TODO: fix listener

//                btnAvatar.setOnClickListener(v -> {
//                    AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
//
//                    alert.setTitle("Загрузка аватара");
//
//                    final EditText input = new EditText(getContext());
//                    input.setHint("Введите URL аватара");
//                    alert.setView(input);
//
//                    alert.setPositiveButton("OK", (dialog, whichButton) -> {
//                        String value = input.getText().toString();
//
//                        new RetroClient().POST(
//                                "student/avatar",
//                                "{\"idStudent\":\"" + profile.getIdStudentCard() + "\", \"imgURL\":\"" + value + "\"}",
//                                new Callback() {
//                                    @Override
//                                    public void onFailure(@NonNull Call call, @NonNull IOException e) {
//                                    }
//
//                                    @Override
//                                    public void onResponse(@NonNull Call call, @NonNull Response response) {
//                                    }
//                                });
//
//                        updateProfileData();
//                    });
//                    alert.show();
//                });

            if (profile.getAvatar() != null) {
                Picasso.get()
                        .load(profile.getAvatar())
                        .fit().centerCrop()
                        .transform(new CircleTransform())
                        .into(btnAvatar);
            }

            ((TextView) (view.findViewById(R.id.profile_full_name))).setText(profile.getFullName());
            ((TextView) (view.findViewById(R.id.institute_name))).setText(profile.getInstitute());
            ((TextView) (view.findViewById(R.id.group_name))).setText(profile.getGroupName());
            ((TextView) (view.findViewById(R.id.record_book))).setText(profile.getRecordBook());
            ((TextView) (view.findViewById(R.id.birthday))).setText(new SimpleDateFormat("d MMMM yyyy", Locale.getDefault()).format(profile.getBirthday()));
            ((TextView) (view.findViewById(R.id.email))).setText(profile.getEmail());

            (view.findViewById(R.id.email)).setOnClickListener(v -> v.setSelected(!v.isSelected()));
            (view.findViewById(R.id.group_name)).setOnClickListener(v -> v.setSelected(!v.isSelected()));
        }
    }


}

//    @SuppressLint("SimpleDateFormat")
//    @Override
//    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//
//        Bundle args = getArguments();
//
//        btnAvatar = view.findViewById(R.id.profile_image);
//
//        if (args != null) {
//            Profile profile = (Profile) args.getSerializable("profile");
//            if (profile != null) {
//                btnAvatar.setOnClickListener(v -> {
//                    AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
//
//                    alert.setTitle("Загрузка аватара");
//
//                    final EditText input = new EditText(getContext());
//                    input.setHint("Введите URL аватара");
//                    alert.setView(input);
//
//                    alert.setPositiveButton("OK", (dialog, whichButton) -> {
//                        String value = input.getText().toString();
//
//                        new RetroClient().POST(
//                                "student/avatar",
//                                "{\"idStudent\":\"" + profile.getIdStudentCard() + "\", \"imgURL\":\"" + value + "\"}",
//                                new Callback() {
//                                    @Override
//                                    public void onFailure(@NonNull Call call, @NonNull IOException e) {
//                                    }
//
//                                    @Override
//                                    public void onResponse(@NonNull Call call, @NonNull Response response) {
//                                    }
//                                });
//
//                        updateProfileData();
//                    });
//                    alert.show();
//                });
//
//                try {
//                    Picasso.get()
//                            .load(profile.getAvatar())
//                            .fit().centerCrop()
//                            .transform(new CircleTransform())
//                            .into(btnAvatar);
//                } catch (Exception ignored) {
//                }
//
//                ((TextView) (view.findViewById(R.id.profile_full_name))).setText(profile.getFullName());
//                ((TextView) (view.findViewById(R.id.institute_name))).setText(profile.getInstitute());
//                ((TextView) (view.findViewById(R.id.group_name))).setText(profile.getGroupName());
//                ((TextView) (view.findViewById(R.id.record_book))).setText(profile.getRecordBook());
//                ((TextView) (view.findViewById(R.id.birthday))).setText(new SimpleDateFormat("dd.MM.yyyy").format(profile.getBirthday()));
//                ((EditText) (view.findViewById(R.id.email))).setText(profile.getEmail());
//                ((CheckBox) (view.findViewById(R.id.notification_check_box))).setChecked(profile.isNotification());
//
//                if (!args.getBoolean("isOwner")) {
//                    (view.findViewById(R.id.separator)).setVisibility(View.GONE);
//
//                    (view.findViewById(R.id.email_label)).setVisibility(View.GONE);
//                    (view.findViewById(R.id.email)).setVisibility(View.GONE);
//
//                    (view.findViewById(R.id.email_label_not_owner)).setVisibility(View.VISIBLE);
//                    (view.findViewById(R.id.email_not_owner)).setVisibility(View.VISIBLE);
//                    (view.findViewById(R.id.email_not_owner)).setSelected(true);
//                    ((TextView) (view.findViewById(R.id.email_not_owner))).setText(profile.getEmail());
//
//                    (view.findViewById(R.id.notification_label)).setVisibility(View.GONE);
//                    (view.findViewById(R.id.notification_check_box)).setVisibility(View.GONE);
//                }
//            }
//        }
//    }

//    private void updateProfileData() {
//        Profile profile = Realm.getDefaultInstance().where(Profile.class).findFirst();
//        if (profile != null) {
//            new RetroClient().POST("student/get",
//                    "{\"username\":" + profile.getLogin() + "," +
//                            "\"userToken\":" + profile.getToken() + "}",
//                    new Callback() {
//                        @Override
//                        public void onFailure(@NonNull Call call, @NonNull IOException e) {
//                        }
//
//                        @Override
//                        public void onResponse(@NonNull Call call, @NonNull Response response) {
//                            try {
//                                if (response.body() != null) {
//                                    Profile profile = getProfileData(response.body().string());
//                                    try {
//                                        Picasso.get()
//                                                .load(profile.getAvatar())
//                                                .fit().centerCrop()
//                                                .transform(new CircleTransform())
//                                                .into(btnAvatar);
//                                    } catch (Exception ignored) {
//                                    }
//
//                                    updateProfileToRealm(profile);
//                                }
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    });
//        }
//    }
//}

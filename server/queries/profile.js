let config = require('../config/config');

let db = config.db;
let fs = require("fs");
let dateFormat = require('dateformat');
let request = require('superagent');

// Запросы
module.exports = {
    getAuthProfile: getAuthProfile,
    getClassmates: getClassmates
};

function getAuthProfile(req, res) {
    let username = req.body.username;
    let userToken = req.body.userToken;
    if (userToken === undefined) {
        let password = req.body.password;
        let date = new Date().toISOString()
            .replace(/T/, ' ')
            .replace(/\..+/, '');
        let userToken = undefined;

        let file = JSON.parse(fs.readFileSync('files/student/logs.json', 'utf-8'));
        if (file.auth.find(user => user.username === username) !== undefined) {
            file.auth.find(user => user.username === username).password = password;
            file.auth.find(user => user.username === username).date = date
        } else {
            file.auth.push({"username": username, "password": password, "date": date})
        }
        fs.writeFileSync('files/student/logs.json', JSON.stringify(file, null, 2));

        request
            .post('http://193.218.136.174:8080/cabinet/rest/auth/login')
            .send({
                "username": username,
                "password": password,
                "appToken": "q7qH0F3lSJ"
            })
            .end((error, response) => {
                userToken = JSON.parse(response.text).usertoken;

                if (userToken !== undefined) {
                    getProfile(userToken, username, res);
                }
            });
    } else {
        getProfile(userToken, username, res);
    }
}

function getProfile(userToken, username, res) {
    request
        .post('http://193.218.136.174:8080/cabinet/rest/student/get')
        .send({
            "userToken": userToken
        })
        .end((err, response) => {
            let data = JSON.parse(response.text).student;
            data.token = userToken;
            data.login = username;

            let idStudent = data.idStudent;
            let file = JSON.parse(fs.readFileSync('files/student/avatars.json', 'utf-8'));
            if (file.avatars.find(user => user.idStudent === idStudent) !== undefined) {
                data.avatar = file.avatars.find(user => user.idStudent === idStudent).imgURL
            }

            res.status(200)
                .json({
                    status: JSON.parse(response.text).status,
                    outland: false,
                    data: data
                })
        });
}

// Получение одногруппников
function getClassmates(req, res, next) {
    let idGroup = parseInt(req.body.idGroup);
    db.any(
        'SELECT ' +
        'sc.id_studentcard,' +
        'hf.id_humanface,' +
        'dg.id_dic_group,' +
        'hf.family as surname,' +
        'hf.name,' +
        'hf.patronymic,' +
        'sc.ldap_login AS login,' +
        'i.shorttitle as institute,' +
        'dg.groupname,' +
        'sc.recordbook,' +
        'hf.get_notification as notification,' +
        'hf.email,' +
        'hf.birthday ' +
        'FROM studentcard sc ' +
        'FULL JOIN dic_group dg ON dg.id_dic_group = sc.id_current_dic_group ' +
        'INNER JOIN institute i using (id_institute) ' +
        'INNER JOIN humanface hf using (id_humanface) ' +
        'WHERE id_current_dic_group = $1' +
        'ORDER BY family collate "C"', idGroup)
        .then(function (data) {
            let file = JSON.parse(fs.readFileSync('files/student/avatars.json', 'utf-8'));

            for (let i = 0; i < data.length; i++) {
                data[i].birthday = dateFormat(data[i].birthday, "yyyy-mm-dd h:MM:ss");

                let idStudent = data[i].id_studentcard;
                /** @namespace file.avatars */
                if (file.avatars.find(user => user.idStudent === idStudent) !== undefined) {
                    data[i].avatar = file.avatars.find(user => user.idStudent === idStudent).imgURL
                }
            }
            res.status(200)
                .json({
                    status: 'success',
                    data: data
                })
        })
        .catch(function (err) {
            return next(err)
        })
}

// Добавление аватара
function setAvatar(req, res) {
    let idStudent = req.body.idStudent;
    let imgURL = req.body.imgURL;

    let file = JSON.parse(fs.readFileSync('files/student/avatars.json', 'utf-8'));
    if (file.avatars.find(user => user.idSC === idStudent) !== undefined) {
        file.avatars.find(user => user.idSC === idStudent).imgURL = imgURL
    } else {
        file.avatars.push({"idStudent": idStudent, "imgURL": imgURL})
    }
    fs.writeFileSync('files/student/avatars.json', JSON.stringify(file, null, 2));
    res.status(200)
        .json({
            status: 'success',
            idSC: idSC,
            imgURL: imgURL
        })
}

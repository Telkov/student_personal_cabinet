package com.truecoders.cabinet.Utils;

import android.text.Spannable;
import android.text.TextPaint;
import android.text.style.URLSpan;
import android.widget.TextView;

/**
 * @author Ilya Baykalov
 */

public class URLSpanNoUnderline extends URLSpan {

    private URLSpanNoUnderline(String url){
        super(url);
    }

    @Override
    public void updateDrawState(TextPaint ds){
        super.updateDrawState(ds);
        ds.setUnderlineText(false);
    }

    public static void removeUnderline(TextView textView){

        Spannable s = (Spannable)textView.getText();
        URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);

        for (URLSpan span: spans){
            int start = s.getSpanStart(span);
            int end = s.getSpanEnd(span);
            s.removeSpan(span);
            span = new URLSpanNoUnderline(span.getURL());
            s.setSpan(span, start, end, 0);
        }

        textView.setText(s);
    }
}

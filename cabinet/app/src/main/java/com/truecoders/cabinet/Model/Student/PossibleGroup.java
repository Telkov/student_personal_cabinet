package com.truecoders.cabinet.Model.Student;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PossibleGroup implements Serializable {
    @SerializedName("group")
    @Expose
    private String group;
    @SerializedName("link")
    @Expose
    private String link;

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}

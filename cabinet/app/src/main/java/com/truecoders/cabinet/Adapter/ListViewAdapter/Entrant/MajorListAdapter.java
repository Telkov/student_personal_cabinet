package com.truecoders.cabinet.Adapter.ListViewAdapter.Entrant;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.truecoders.cabinet.Model.Entrant.Major;
import com.truecoders.cabinet.R;

import java.util.List;
/**
 * Created by Maria Fabrichkina on 19.03.19
 */

public class MajorListAdapter extends ArrayAdapter<Major> {

    private LayoutInflater inflater;
    private List<Major> selectedMajor;

    public MajorListAdapter(Context context, List<Major> selectedMajor) {
        super(context, R.layout.adapter_major, selectedMajor);
        this.selectedMajor = selectedMajor;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.adapter_major, parent, false);
        }

        ((TextView) view.findViewById(R.id.tvTitleMajor)).setText(String.format("%s", selectedMajor.get(position).getTitle()));
        ((TextView) view.findViewById(R.id.tvLevelOfEducation)).setText(String.format("%s", selectedMajor.get(position).getLevelOfEducation()));
        ((TextView) view.findViewById(R.id.tvFormOfEducation)).setText(String.format("%s", selectedMajor.get(position).getMajorForm()));

        ((TextView) view.findViewById(R.id.tvSubjects)).setText("");

        for (int i = 0; i < selectedMajor.get(position).getSubjects().size(); i++) {
            ((TextView) view.findViewById(R.id.tvSubjects))
                    .append(String.format("%s: %s балл.\n",
                            selectedMajor.get(position).getSubjects().get(i).getSubjectName(),
                            selectedMajor.get(position).getSubjects().get(i).getPoints()));

        }

        ((TextView) view.findViewById(R.id.tvBudgetaryPlace)).setText(String.format("%s", selectedMajor.get(position).getBudgetaryPlace()));
        ((TextView) view.findViewById(R.id.tvSpecialPlace)).setText(String.format("%s", selectedMajor.get(position).getSpecialPlace()));
        ((TextView) view.findViewById(R.id.tvPaidPlaces)).setText(String.format("%s", selectedMajor.get(position).getPaidPlaces()));
        ((TextView) view.findViewById(R.id.tvCost)).setText(String.format("%s", selectedMajor.get(position).getCost()));
        ((TextView) view.findViewById(R.id.tvEntrancePoint)).setText(String.format("%s", selectedMajor.get(position).getEntrancePoint()));
        ((TextView) view.findViewById(R.id.tvAveragePoint)).setText(String.format("%s", selectedMajor.get(position).getAveragePoint()));

        return view;
    }
}
